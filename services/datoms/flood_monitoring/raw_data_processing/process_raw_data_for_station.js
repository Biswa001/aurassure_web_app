'use strict';

const moment = require('moment-timezone'),
	mysql = require('mysql'),
	kafka = require('node-rdkafka'),
	dbAccess = require('/home/ec2-user/lib/db_access/db_access.js'),
	kafkaAccess = require('/home/ec2-user/lib/kafka_access/kafka_access.js');

const kafkaProducer = new kafka.Producer({
	'metadata.broker.list': kafkaAccess.production_cluster.brokers,
	'dr_cb': true
});
kafkaProducer.on('delivery-report', (err, report) => {
	if(err) {
		console.log('Error in kafka producer -> '+ JSON.stringify(err));
		/*putToAppLog({
			type: 'error',
			message: 'Error in kafka producer -> '+ JSON.stringify(err)
		});*/
	}
});
kafkaProducer.setPollInterval(100);
const prepareKafkaProducer = () => {
	return new Promise((resolve, reject) => {
		kafkaProducer.connect();
		kafkaProducer.on('ready', () => {
			resolve();
		}).on('event.error', (err) => {
			console.log('Error in kafka producer -> '+ JSON.stringify(err));
			/*putToAppLog({
				type: 'error',
				message: 'Error in kafka producer -> '+ JSON.stringify(err)
			});*/
			reject();
		});
	});
};
//function to push to Device raw message in Kafka
const pushToRulesEngineEvaluationStreamInKafka = (message) => {
	try {
		kafkaProducer.produce(
			'rules-engine-stream',
			null,
			new Buffer(JSON.stringify(message)),
			null,
			Date.now()
		);
	} catch (err) {
		console.log('Error in kafka producer -> '+ JSON.stringify(err));
		/*putToAppLog({
			type: 'error',
			message: 'Error in kafka producer -> '+ JSON.stringify(err)
		});*/
	}
};

const data_db_connection_pool = mysql.createPool({
	connectionLimit : 10,
	host     		: dbAccess.data_db.host,
	user     		: dbAccess.data_db.user,
	password 		: dbAccess.data_db.password,
	database 		: dbAccess.data_db.database,
	multipleStatements: true
});

const is_valid_json = (json_string) => {
	try {
		JSON.parse(json_string);
	} catch (e) {
		return false;
	}
	return true;
};

const main = (async () => {
	try {
		//prepare Kafka Producer
		await prepareKafkaProducer();

		setInterval(() => {
			let current_time = moment.tz('Asia/Kolkata').unix();
			/*get all the flood monitoring stations*/
			data_db_connection_pool.query('SELECT fms_id, fms_param_list, fms_lst_data, fms_params_last_update_time FROM flood_monitoring_stations', [], (err, results) => {
				results.map((result) => {
					let station_raw_data = {},
						source_device_ids = [],
						station_param_details = (is_valid_json(result.fms_param_list) ? JSON.parse(result.fms_param_list) : []),
						station_params_last_value = (is_valid_json(result.fms_lst_data) ? JSON.parse(result.fms_lst_data) : []),
						station_params_last_update_time = (is_valid_json(result.fms_params_last_update_time) ? JSON.parse(result.fms_params_last_update_time) : {}),
						station_id = result.fms_id;
					station_param_details.map((param_detail) => {
						source_device_ids.push(param_detail.device_id);
					});
					let sql = 'SELECT idev_id, last_raw_data, last_data_receive_time FROM iot_devices WHERE idev_id IN ('+ source_device_ids.join(',') +')';
					let query = data_db_connection_pool.query(sql, [], (err, results) => {
						if(!err && results.length) {
							results.map((result) => {
								/*if data is received within 2 mins*/
								if(current_time - result.last_data_receive_time <= 120) {
									let device_raw_data = (is_valid_json(result.last_raw_data) ? JSON.parse(result.last_raw_data) : {});
									station_param_details.map((param_detail) => {
										if(device_raw_data[param_detail.key] !== undefined) {
											if(['dust_pm_2_5', 'dust_pm_10', 'temperature', 'humidity', 'street_water_level'].indexOf(param_detail.type) >= 0) {
												if(
													station_params_last_update_time[param_detail.key] === undefined
													 || (station_params_last_update_time[param_detail.key] != result.last_data_receive_time)
												) {
													if(param_detail.type === 'street_water_level' && param_detail.surface_height && param_detail.surface_height !== '') {
														let current_water_level = (param_detail.surface_height - device_raw_data[param_detail.key]).toFixed(2);
														if(current_water_level < 0) {
															current_water_level = 0;
														}
														/*
														* station is configured for the first time -> show current value
														* station surface height set for the first time -> show current value
														* diff. from last value (the exact last data point if available -> within 18 minutes) of already configured & surface height set sensor < 20 -> show current value
														* diff. from last value of already configured & surface height set sensor > 20 -> show last value
														*/
														if(
															station_params_last_value[param_detail.key] !== undefined //when the station is configured for the first time
															&& station_params_last_value[param_detail.key] < 300 //when the base level of the sensor is configured for the first time
															&& station_params_last_update_time[param_detail.key] !== undefined
															&& current_time - station_params_last_update_time[param_detail.key] <= 1080 //18 minutes
															&& Math.abs(station_params_last_value[param_detail.key] - current_water_level) > 20
														) {
															station_raw_data[param_detail.key] = station_params_last_value[param_detail.key];
														} else {
															station_raw_data[param_detail.key] = current_water_level;
														}
													} else {
														station_raw_data[param_detail.key] = device_raw_data[param_detail.key];
													}
													//update last data update time of the parameter
													station_params_last_update_time[param_detail.key] = result.last_data_receive_time;
													//adjust temp. by -3 degree
													if(param_detail.type === 'temperature' && station_raw_data[param_detail.key] > 3) {
														station_raw_data[param_detail.key] = (station_raw_data[param_detail.key] - 3).toFixed(2);
													}
												}
											} else if(param_detail.type === 'pump_status' && param_detail.upper_limit && param_detail.upper_limit !== '') {
												if(
													parseFloat(device_raw_data[param_detail.key]) > parseFloat(param_detail.upper_limit)
												) {
													station_raw_data[param_detail.key] = 'ON';
												} else {
													station_raw_data[param_detail.key] = 'OFF';
												}
											} else if(param_detail.type === 'sump_level' && param_detail.surface_height && param_detail.surface_height !== '') {
												station_raw_data[param_detail.key] = (param_detail.surface_height - device_raw_data[param_detail.key]).toFixed(2);
											} else if(param_detail.type === 'penstock_level' && param_detail.surface_height && param_detail.surface_height !== '') {
												station_raw_data[param_detail.key] = (param_detail.surface_height - device_raw_data[param_detail.key]).toFixed(2);
											} else if(param_detail.type === 'open_canal_water_level') {
												if(
													device_raw_data[param_detail.key] < 500
													|| station_params_last_value[param_detail.key] === undefined
												) {
													//sensor reading < 500 -> valid data
													if(param_detail.surface_height && param_detail.surface_height !== '') {
														station_raw_data[param_detail.key] = (param_detail.surface_height - device_raw_data[param_detail.key]).toFixed(2);
														let current_water_level = (param_detail.surface_height - device_raw_data[param_detail.key]).toFixed(2);
														if(current_water_level < 0) {
															current_water_level = 0;
														}
														/*
														* station is configured for the first time -> show current value
														* station surface height set for the first time -> show current value
														* diff. from last value (the exact last data point if available -> within 18 minutes) of already configured & surface height set sensor < 20 -> show current value
														* diff. from last value of already configured & surface height set sensor > 20 -> show last value
														*/
														if(
															station_params_last_value[param_detail.key] !== undefined //when the station is configured for the first time
															&& station_params_last_value[param_detail.key] < 500 //when the base level of the sensor is configured for the first time
															&& station_params_last_update_time[param_detail.key] !== undefined
															&& current_time - station_params_last_update_time[param_detail.key] <= 180 //3 minutes
															&& Math.abs(station_params_last_value[param_detail.key] - current_water_level) > 25
														) {
															station_raw_data[param_detail.key] = station_params_last_value[param_detail.key];
														} else {
															station_raw_data[param_detail.key] = current_water_level;
														}
													} else {
														station_raw_data[param_detail.key] = device_raw_data[param_detail.key];
													}
												} else {
													//sensor noise -> invalid data -> store last data
													station_raw_data[param_detail.key] = station_params_last_value[param_detail.key];
												}
											} else {
												station_raw_data[param_detail.key] = device_raw_data[param_detail.key];
											}
										}
									});
								}
							});
						}

						/*update raw data of stations if data received from at least one of the devices*/
						if(Object.keys(station_raw_data).length !== 0) {
							data_db_connection_pool.query('INSERT INTO flood_monitoring_stations_raw_data(fms_id, fms_rd_time, fms_rd_data) VALUES (?,?,?); UPDATE flood_monitoring_stations SET fms_lst_dat_time=?, fms_lst_dat_srv_time=?, fms_params_last_update_time=?, fms_lst_data=? WHERE fms_id=?', [station_id, current_time, JSON.stringify(station_raw_data), current_time, current_time, JSON.stringify(station_params_last_update_time), JSON.stringify(station_raw_data), station_id], (err) => {
								if(err) {
									console.log('error -> ', err);
								}
							});

							//push data point to Rules Engine Stream for further processing
							pushToRulesEngineEvaluationStreamInKafka({
								app_id: 19,
								station_id: station_id,
								time: current_time,
								data: station_raw_data
							});
						}
					});
				});
			});
		}, 60000);
	} catch(err) {
		console.log('Error caught inside catch of main function -> ', err);
		/*putToAppLog({
			type: 'error',
			message: 'Error caught inside catch of main function -> ', err
		});*/
	}
})();
