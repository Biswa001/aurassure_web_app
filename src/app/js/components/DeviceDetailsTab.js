import React from 'react';
import { Layout, Row, Col, Button, Icon, Table, Input, Tabs, Drawer, Form, Select, TreeSelect, Card, Tooltip, Menu, Dropdown, Modal, Popconfirm, notification } from 'antd';
// import './css/style.less';
import Head from './imports/Head';
import Side from './imports/Side';
import Loading from './imports/Loading';
// import RolesPage from './imports/RolesPage';
import UserGroupsPage from './imports/UserGroupsPage';
// import StationPage from './imports/StationPage';
import DeviceCalibrationPage from './imports/DeviceCalibrationPage';
import DeviceDetailsPage from './imports/DeviceDetailsPage';
import DeviceConfigurationPage from './imports/DeviceConfigurationPage';
import { Link } from 'react-router-dom';
import io from 'socket.io-client';

// import queryString from 'query-string';

const queryString = require('query-string');

const TabPane = Tabs.TabPane;

const { Option } = Select;

const confirm = Modal.confirm;

const { Content } = Layout;
/**
 * Main component for the Users page.
 */
export default class DeviceDetailsTab extends React.Component {
	/**
	 * This is the Constructor for Dashboard page used to set the default task while page loads.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		/**
		 * This stores the parsed data from the query string.
		 * @type {String}
		 */
		this.parsed_search = queryString.parse(this.props.location.search);
		console.log('parsed', this.parsed);
		/**
		* This sets the initial state for the page.
		* @type {Object}
		* @property {string} tab_toggle used to store the value to toggle the tabs.
		*/
		this.state = {
			tab_toggle: props.location.pathname && (props.location.pathname.search('/details/') > -1) ? 'details' : (props.location.pathname && (props.location.pathname.search('/configuration/') > -1) ? 'config' : (props.location.pathname && (props.location.pathname.search('/calibration/') > -1) ? 'calib' : 'details')),
			device_id: props.match.params.device_id,
		};
	}
	/**
	 * This function changes the toggles between the tabs.
	 */
	changeGroup(key, update = false) {
		console.log('in changeGroup',key);
		console.log('in changeGroup',update);
		let search = this.state.table_search;
		this.setState({
			tab_toggle: key,
			table_search: update ? '' : search
		}, () => {
			if (this.state.tab_toggle == 'details') {
				this.props.history.push('/devices/' + this.state.device_id + '/details/');
			} else if (this.state.tab_toggle == 'config') {
				this.props.history.push('/devices/' + this.state.device_id + '/configuration/');
			} else if (this.state.tab_toggle == 'calib') {
				this.props.history.push('/devices/' + this.state.device_id + '/calibration/');
			}
		});
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	componentDidMount() {
		// document.title = 'Device Details - Chennai Smart City';

		/**
		 * Socket is used to sync the data between client and server in real time.
		 */
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
			query: {
				s_token: this.readCookie('PHPSESSID')
				// s_token: 'qd3gdp5gnqgi6svqj63c3ro1p6'
			}
		});
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket');
			console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			this.socket.emit('get_device_details', JSON.stringify({
				device_id: this.state.device_id
			}));
		});

		this.socket.on('update_device_details', (payload) => {
			console.log('update_device_details', JSON.parse(payload));
			if (this.state.device_id == JSON.parse(payload).device_id) {
				let device_details = JSON.parse(payload);
				this.setState({
					device_id: device_details.device_id,
					location_name: device_details.location_name,
					device_qr_code: device_details.device_qr_code,
					status: device_details.status
				});
			}
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			// console.log('update_location_details_in_dashboard', JSON.parse(payload));
			if (JSON.parse(payload).device_id == this.state.device_id) {
				this.setState(JSON.parse(payload));
			}
		});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {
		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	/**
	 * This renders entire class with navigation bar.
	 * @return {ReactElement} markup
	 */
	render () {
		if(this.state.data_loading) {
			return <div className="panel-body details-list text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
		} else {
			return (
				<div id="device" className="mar-top-70">
					<Side active_link="device" />
					<Head/>
					<Layout className="mar-top-72">
						<Content className="contains">
							<div className="configure-device">
								<div className="head top-60">
									<span className={'dot1' + (this.state.status ? ' ' + this.state.status : '')}></span>
									<span>{(this.state.location_name && this.state.location_name != '' ? this.state.location_name + ' - ' + (this.state.device_qr_code && this.state.device_qr_code != '' ? this.state.device_qr_code : 'No QR Code Configured') : 'Device')}</span>
								</div>
								<span className="back-btn">
									<Link to={'/device/'}>
										<Button icon="arrow-left">Back</Button>
									</Link>
								</span>
								<div>
									<div role="tablist" className="station-tabs-bar">
										<div className="station-tabs-nav-scroll">
											<div className="station-tabs-nav station-tabs-nav-animated">
												<div>
													<div role="tab" aria-disabled="false" aria-selected="true" className={'station-tabs station-tabs-tab' + (this.props.location.pathname && (this.props.location.pathname.search('/details/') > -1) ? ' station-tabs-tab-active' : '')} onClick={() => this.changeGroup('details')}>Details</div>
													<div role="tab" aria-disabled="false" aria-selected="false" className={'station-tabs station-tabs-tab' + (this.props.location.pathname && (this.props.location.pathname.search('/configuration/') > -1) ? ' station-tabs-tab-active' : '')} onClick={() => this.changeGroup('config')}>Configuration</div>
													<div role="tab" aria-disabled="false" aria-selected="false" className={'station-tabs station-tabs-tab' + (this.props.location.pathname && (this.props.location.pathname.search('/calibration/') > -1) ? ' station-tabs-tab-active' : '')} onClick={() => this.changeGroup('calib')}>Calibration</div>
												</div>
													<div className="blank-border"></div>
											</div>
										</div>
									</div>
									<div className="station-view">
										{(() => {
											if (this.props.location.pathname && (this.props.location.pathname.search('/details/') > -1)) {
												console.log('render 2');
												return <DeviceDetailsPage device_id={this.state.device_id} {...this.props}/>;
											} else if (this.props.location.pathname && (this.props.location.pathname.search('/configuration/') > -1)) {
												console.log('render 3');
												return <DeviceConfigurationPage device_id={this.state.device_id}  {...this.props}/>;
											} else if (this.props.location.pathname && (this.props.location.pathname.search('/calibration/') > -1)) {
												console.log('render 4');
												return <DeviceCalibrationPage device_id={this.state.device_id}  {...this.props}/>;
											}/* else {
												console.log('render 4');
												return <Loading iniline={true} />;
											}*/
										})()}
									</div>
								</div>
							</div>
						</Content>
					</Layout>
				</div>
			);
		}
	}
}