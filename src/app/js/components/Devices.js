import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Switch, Modal, Drawer, Card } from 'antd';
import Head from './imports/Head';
import Side from './imports/Side';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import _ from 'lodash';
// import Link from 'react-router/Link';
import io from 'socket.io-client';
import moment from 'moment-timezone';
import { Link } from 'react-router-dom';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const SHOW_CHILD = TreeSelect.SHOW_CHILD;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];
const confirm = Modal.confirm;

const config = {
	chart: {
		renderTo: 'container',
		type: 'pie',
		plotBackgroundColor: null,
		plotBorderWidth: null,
		plotShadow: false,
		height: 360,
		width: window.innerWidth <= 425 ? window.innerWidth - 110 : 295
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	},
	 plotOptions: {
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
					enabled: false
			},
			showInLegend: true
		}
	},
		series: [{
				name: 'Online',
				colorByPoint: true,
				data: [{
						name: '>90%',
						y: 61.41,
						sliced: true,
						selected: true
				}, {
						name: '90-60%',
						y: 11.84
				}, {
						name: '<60%',
						y: 10.85
				}, {
						name: '0%',
						y: 4.67
				}]
		}]
};

function showDelete() {
	confirm({
		title: 'Do you want to delete ?',
		content: '',
		onOk() {},
		onCancel() {},
	});
}

const action = (
	<Menu>
		<Menu.Item key="action-1">Edit</Menu.Item>
		<Menu.Item key="action-2" onClick={showDelete}>Delete</Menu.Item>
	</Menu>
);

const AddForm = Form.create()(
	class extends React.Component {

		render() {
			const { visible, onCancel, onCreate } = this.props;
			
			return (
				<div id="create_form">
					<Drawer
						title="New Calibration"
						width={1000}
						placement="right"
						visible={visible}
						onClose={onCancel}
						maskClosable={false}
						style={{
							height: 'calc(100% - 55px)',
							overflow: 'auto',
							paddingBottom: 53,
						}}
					>
						<Form layout="vertical" hideRequiredMark>
							<Row gutter={16}>
								<Col span={6} className="wid-100">
									<Form.Item label="Calibration Type">
										<Select showSearch placeholder="Please select calibration type">
											<Option value="no2">NO2</Option>
											<Option value="co2">CO2</Option>
											<Option value="co">CO</Option>
											<Option value="pm2.5">PM2.5</Option>
											<Option value="pm10">PM10</Option>
											<Option value="noise">Noise</Option>
										</Select>
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={16}>
								<Col span={4} className="wid-100">
									<Form.Item label="WEzero">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="AUXzero">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Sensitivity">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Offset">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Multiplier">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
							</Row>
						</Form>
						<div
							style={{
								position: 'absolute',
								bottom: 0,
								width: '100%',
								borderTop: '1px solid #e8e8e8',
								padding: '10px 16px',
								textAlign: 'right',
								left: 0,
								background: '#fff',
								borderRadius: '0 0 4px 4px',
							}}
						>
							<Button
								style={{
									marginRight: 8,
								}}
								onClick={onCancel}
							>
								Cancel
							</Button>
							<Button onClick={onCancel} type="primary">Submit</Button>
						</div>
					</Drawer>
				</div>
			);
		}
	}
);

const data = [
	{
		station: 'Station-1',
		city: 'City-1',
		active: '13:34, 28 Sep'
	}, {
		station: 'Station-2',
		city: 'City-2',
		active: '13:34, 28 Sep'
	}, {
		station: 'Station-3',
		city: 'City-3',
		active: '13:34, 28 Sep'
	}, {
		station: 'Station-4',
		city: 'City-4',
		active: '13:34, 28 Sep'
	}, {
		station: 'Station-5',
		city: 'City-5',
		active: '13:34, 28 Sep'
	}, {
		station: 'Station-6',
		city: 'City-6',
		active: '13:34, 28 Sep'
	}, {
		station: 'Station-7',
		city: 'City-7',
		active: '13:34, 28 Sep'
	}, {
		station: 'Station-8',
		city: 'City-8',
		active: '13:34, 28 Sep'
	}
];

const userFilter = [];

class Devices extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: [],
			table_data: [],
			table_search: this.parsed_search && Object.values(this.parsed_search).length && this.parsed_search.search ? this.parsed_search.search : '',
			tableVisible: true,
			drawAddVisible: false,
			device_details: null,
			status: null,
			id: null,
			adding_status: null,
			city_id: 1, //'all',
			city_id_exclude: ['all', 'unconfigured', 'no_data_received'],
			search_value: '',
			modal_view: false,
			modal_sync_view: false,
			sync_index: null,
			sorted: false,
			sort_keys: null,
			date_time_online: false,
			filter_keys: {
				city_id: '0',
				connection: '0',
				sync: '0',
				health: '0',
				data_sending: '0',
				unconfigured: '0',//false
				device_archive_status: 'active',
				no_data_received: '0',//false
				publicly_accessible: '0'
			},
			health_status: {
				0: 'Any',
				1: 'OK',
				2: 'Power Failure',
				3: 'Code Restart',
				4: 'Modem Restart',
				5: 'Other Error',
				6: 'Battery Voltage Down',
				7: 'Network Signal Low',
				8: 'Debug Data Not Sent'
			},
			modem_types: {
				1: 'GPRS',
				2: 'WIFI',
				3: 'Ethernet'
			}
		};
		this.device_name = '';
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	setTableData() {
		let table_data = [];
		if (this.state.device_details && this.state.device_details.length) {
			this.state.device_details.map((device, index) => {
				table_data.push({
					key: index+1,
					id: device.id,
					qr: device.qr_code,
					station: device.name,
					city: this.state.device_cities[device.city_id],
					active: device.device_last_data_receive_timestamp,
					sync: device.device_sync_status,
					connect: device.device_modem_type,
					health: this.state.health_status[device.device_error_status],
					date: moment.unix(device.last_data_time).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
					timestamp: device.last_data_time,
					percent: device.online_percent,
					device_config: device
				});
			});
		}
		this.setState({
			table_data: table_data
		});
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Device Configuration - Aurassure';
		/**
		 * Socket is used to sync the data between client and server in real time.
		 */
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
			query: {
				s_token: this.readCookie('PHPSESSID')
				// s_token: 'qd3gdp5gnqgi6svqj63c3ro1p6'
			}
		});
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket');
			console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			this.socket.emit('get_list_of_devices', JSON.stringify({
				city_id: 1
			}));
			console.log('dashboard successfully connected');
		});

		this.socket.on('update_list_of_devices', (payload) => {
			let data = JSON.parse(payload);
			let child_data = [];
			if (data.cities && Object.keys(data.cities).length) {
				Object.keys(data.cities).map((ct_id) => {
					child_data.push({
						'title': data.cities[ct_id],
						'value': '0-1' + '-' + ct_id,
						'key': '0-1' + '-' + ct_id,
					});
				});
			}
			userFilter.push({
				title: 'City',
				value: '0-1',
				key: '0-1',
				children: child_data
			});
			this.setState({
				device_details: data.devices,
				device_cities: data.cities
			}, () => this.getFilteredStations(this.state.search_value));
			console.log('update_list_of_devices', this.state.device_details);
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			let data_status = JSON.parse(payload);
			if (this.state.device_details) {
				let status_change = this.state.device_details;
				status_change.map((stat,index) => {
					if (stat && data_status.device_id == stat.id) {
						Object.keys(data_status).map((key) => {
							if (key != 'device_id') {
								status_change[index][key] = data_status[key];
							}
						});
					}
				});
				this.setState({device_details: status_change}, () => this.updateFilter());
			}
			console.log('update_location_details_in_dashboard', this.state.device_details);
			// console.log('Dashboard successfully connected to socket.');
			// console.log('payload', data_status);
		});

		this.socket.on('new_station_added_successfully', (payload) => {
			let add_new_device = JSON.parse(payload);
			let device_details = this.state.device_details;
			device_details.push(add_new_device);
			this.setState({
				device_details: device_details
			}, () => this.getFilteredStations(this.state.search_value));
			// console.log('new_station_added_successfully', add_new_device);
			showPopup('success', 'New device added successfully!');
		});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {
		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	toggleShowOnlineDevices() {
		this.setState({date_time_online: !this.state.date_time_online}, () => this.updateFilter());
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * THis function sets the filtered list for tables according to the searched text.
	 * @param {String}
	 */
	setSearch(text) {
		let table_data = [],
			data_list = this.state.device_list.slice(0);
		let query = this.props.history.location.pathname;
		if (this.state.device_details && this.state.device_details.length && text != '') {
			this.state.device_details.map((st, index) => {
				if (text != '' && (st.name.toLowerCase().includes(decodeURI(encodeURI(text.toLowerCase()))))) {
					table_data.push({
						key: index+1,
						id: st.id,
						qr: st.qr_code,
						station: st.name,
						city: this.state.device_cities[st.city_id],
						active: st.device_last_data_receive_timestamp,
						sync: st.device_sync_status,
						connect: st.device_modem_type,
						health: this.state.health_status[st.device_error_status],
						date: moment.unix(st.last_data_time).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
						timestamp: st.last_data_time,
						percent: st.online_percent,
						device_config: st
					});
				}
				// console.log('stttttttttttt', st);
			});
		} else if (text == '') {
			this.state.device_details.map((st, index) => {
				table_data.push({
					key: index+1,
					id: st.id,
					qr: st.qr_code,
					station: st.name,
					city: this.state.device_cities[st.city_id],
					active: st.device_last_data_receive_timestamp,
					sync: st.device_sync_status,
					connect: st.device_modem_type,
					health: this.state.health_status[st.device_error_status],
					date: moment.unix(st.last_data_time).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
					timestamp: st.last_data_time,
					percent: st.online_percent,
					device_config: st
				});
			});
		}
		this.setState({
			table_data: table_data,
			table_search: decodeURI(encodeURI(text))
		}, () => {
			// this.props.history.push('');
			// this.props.history.push(query + (this.state.table_search != '' ? '?search=' + this.state.table_search : ''));
			// this.queryCreate();
		});
	}

	constructTable(search_value) {
		let table_data = [];
		if (this.state.filtered_stations && this.state.filtered_stations.length) {
			this.state.filtered_stations.map((st, index) => {
				table_data.push({
					key: index+1,
					id: st.id,
					qr: st.qr_code,
					station: st.name,
					city: this.state.device_cities[st.city_id],
					active: st.device_last_data_receive_timestamp,
					sync: st.device_sync_status,
					connect: st.device_modem_type,
					health: this.state.health_status[st.device_error_status],
					date: moment.unix(st.last_data_time).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
					timestamp: st.last_data_time,
					percent: st.online_percent,
					device_config: st
				});
			});
		}
		this.setState({
			table_data: table_data,
			table_search: search_value
		});
	}

	updateFilter(options) {
		if (options) {
			this.setState({filter_keys: options}, () => this.getFilteredStations(this.state.search_value));
		} else {
			this.getFilteredStations(this.state.search_value);
		}
		// console.log('options', options);
	}

	getFilteredStations(search_value) {
		// console.log('getFilteredStations');
		if (this.state.device_details && this.state.device_details.length) {
			let device_list = [], current_time = moment().unix(), filtered_stations_list = [];
			this.state.device_details.map((config, index) => {
				device_list.push(config);
			});
			search_value = search_value.toLowerCase();
			// console.log('Device List', device_list);
			// console.log(this.state);
			this.state.device_details.map((config, index) => {
				// Connection Status
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.connection === '0') ||
						(
							(config.status === this.state.filter_keys.connection) ||
							((config.status === 'data_off') && (this.state.filter_keys.connection === 'online'))
						)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Only online devices
				if (this.state.date_time_online) {
					let less_fifteen_offline = current_time -  config.device_last_online_timestamp,
					fifteen_minute = 900;
					if (_.some(device_list, config) && (less_fifteen_offline >= fifteen_minute)) {
						device_list.splice(device_list.indexOf(config), 1);
					}
					console.log('date_time_online_device_list', device_list);
				}
				// Text Search
				let data_string = config.station_id + ' ' +
								config.name + ' ' +
								this.state.device_cities[config.city_id] + ' ' +
								config.device_firmware_version;
				// console.log('Search', data_string.toLowerCase());
				if (
					_.some(device_list, config) &&
					!(
						(search_value == '') ||
						(data_string.toLowerCase().search(search_value) > -1)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
			});
			
			filtered_stations_list = device_list.filter(Boolean);
			this.setState({
				filtered_stations: filtered_stations_list,
				search_value: search_value
			}, () => {this.constructTable(search_value)});
			// console.log('Filtered Devices:', device_list.filter(Boolean));
			// return device_list.filter(Boolean);
		}
	}

	searchBoxFilter(value) {
		this.getFilteredStations(value.trim());
	}

	onChangeFilter(value) {
		this.setState({ value });
	}

	showConfigDevice() {
		this.setState({
			tableVisible: false,
		});
	};

	showTable() {
		this.setState({
			tableVisible: true,
		})
	};

	showAddDrawer() {
		this.setState({ drawAddVisible: true });
	}

	handleCancel() {
		this.setState({ drawAddVisible: false });
	}

	onChange(value) {
		console.log('onChange ', value);
		this.setState({ value });
	}

	filterData() {

	}

	treeSelect (val, label, extra) {
		console.log('treeSelect', val);
		console.log('treeSelect', label);
		console.log('treeSelect', extra); 
		let table_data = [];
		if (val && val.length) {
			val.map((data_key) => {
				this.state.filtered_stations.map((stn, index) => {
					if (stn.city_id == data_key.split("-")[2]) {
						table_data.push({
							key: index+1,
							id: stn.id,
							qr: stn.qr_code,
							station: stn.name,
							city: this.state.device_cities[stn.city_id],
							active: stn.device_last_data_receive_timestamp,
							sync: stn.device_sync_status,
							connect: stn.device_modem_type,
							health: this.state.health_status[stn.device_error_status],
							date: moment.unix(stn.last_data_time).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
							timestamp: stn.last_data_time,
							percent: stn.online_percent,
							device_config: stn
						});
					}
				});
			});
		} else {
			this.state.filtered_stations.map((stn, index) => {
				table_data.push({
					key: index+1,
					id: stn.id,
					qr: stn.qr_code,
					station: stn.name,
					city: this.state.device_cities[stn.city_id],
					active: stn.device_last_data_receive_timestamp,
					sync: stn.device_sync_status,
					connect: stn.device_modem_type,
					health: this.state.health_status[stn.device_error_status],
					date: moment.unix(stn.last_data_time).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
					timestamp: stn.last_data_time,
					percent: stn.online_percent,
					device_config: stn
				});
			});
		}
		this.setState({
			value: val,
			table_data: table_data
		});
	}

	render() {
		const { startValue, endValue, endOpen } = this.state;
		const userProps = {
			treeData: userFilter,
			value: this.state.value,
			treeCheckable: true,
			maxTagCount: 3,
			showCheckedStrategy: SHOW_CHILD,
			searchPlaceholder: 'Please select filter',
		};

		const columns = [
			{
				title: 'Station Name',
				width: 150,
				key: 'station',
				dataIndex: 'station',
				sorter: (a, b) => a.station.length - b.station.length,
			}, {
				title: 'City',
				width: 120,
				dataIndex: 'city',
				key: 'city',
				sorter: (a, b) => a.city.length - b.city.length,
			}, {
				title: 'Active',
				width: 80,
				dataIndex: 'active',
				key: 'active',
				sorter: (a, b) => a.active.length - b.active.length,
				render: (timestamp) => (
					<Tooltip title="Last data received">
						<span className="dot"></span>
						<span className="date-time">{moment.unix(timestamp).tz('Asia/Kolkata').format('HH:mm, DD MMM')}</span>
					</Tooltip>
				),
			}, {
				title: 'Sync',
				width: 50,
				align: 'center',
				key: 'sync',
				render: (device) => {
					if (device.sync == 'in_sync') {
						return <Tooltip title="In Sync">
							{/*<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px" className="sync-img"><path fill="none" stroke="green" stroke-width="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31-132.6 78-7.8-3.8-16.4-5.7-25.5-5.7-32.8 0-59.8 24.4-64 56C47 152.3 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"></path><path fill="green" d="M374.7 144.5L353.2 123 231 245.2l-38.5-38.5-21 21 50.6 51 .8-.2 9.2 9.3"></path></svg>*/}
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="green" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31-132.6 78-7.8-3.8-16.4-5.7-25.5-5.7-32.8 0-59.8 24.4-64 56C47 152.3 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="green" d="M374.7 144.5L353.2 123 231 245.2l-38.5-38.5-21 21 50.6 51 .8-.2 9.2 9.3"/></svg>
						</Tooltip>;
					} else if (device.sync == 'not_in_sync') {
						return <Tooltip title="Device not in sync">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="red" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="red" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm83.6 52.2h21v-21h-21zM363.8 124H301v62.7l23-23c11.4 10.4 18.7 26 18.7 43.8 0 27.2-17.8 50-41.8 59.5v22c35.2-9.4 62.3-41.8 62.3-80.4 0-23-9.4-44-25-58.5zM259 218h21v-62.7h-21z"/></svg>
						</Tooltip>;
					} else if (device.sync == 'syncing') {
						return <Tooltip title="Syncing...">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="#FF830D" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="#FF830D" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm188-83.5h-62.7v62.7l23-23c11.5 10.4 18.8 26 18.8 43.8 0 27.2-17.8 50-41.8 59.5v22c35.5-9.4 62.6-41.8 62.6-80.4 0-23-9.4-44-25-58.5z"/></svg>
						</Tooltip>;
					} else if (device.sync == 'waiting_for_device') {
						return <Tooltip title="Waiting for device to connect...">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="#800080" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="#800080" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm188-83.5h-62.7v62.7l23-23c11.5 10.4 18.8 26 18.8 43.8 0 27.2-17.8 50-41.8 59.5v22c35.5-9.4 62.6-41.8 62.6-80.4 0-23-9.4-44-25-58.5z"/></svg>
						</Tooltip>;
					}
				},
			}, {
				title: 'Connectivity',
				width: 70,
				align: 'center',
				key: 'connect',
				render: (device) => {
					// console.log('devicesssssssssssssss', device);
					if (device.device_config) {
						if (device.device_config.device_modem_type == 1) {
							return <Tooltip title={(device.device_config.device_modem_type == device.device_config.device_modem_type_priority) ? 'Connected via GPRS' : 'Connected via ' + this.state.modem_types[device.device_config.device_modem_type] + ', but priority set to ' + this.state.modem_types[device.device_config.device_modem_type_priority]}>
								{/*<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="170 0 170 500" height="20" width="20"><path fill="green" d="M64 402H9c-3 0-5 1-6 3-2 1-3 3-3 6v55c0 3 1 5 3 7l6 2h55l7-2 2-7v-55l-2-6c-2-2-4-3-7-3zM283 292h-55l-6 3-3 6v165c0 3 1 5 3 7l6 2h55l7-2 2-7V301l-2-6c-2-2-4-3-7-3zM174 365h-55c-3 0-5 1-7 3s-2 4-2 7v91c0 3 0 5 2 7l7 2h55l6-2c2-2 3-4 3-7v-91c0-3-1-5-3-7l-6-3zM393 183h-55l-7 2-2 7v274l2 7 7 2h55l6-2c2-2 3-4 3-7V192c0-3-1-5-3-7l-6-2zM509 39c-2-2-4-2-7-2h-54c-3 0-5 0-7 2s-2 4-2 7v420c0 3 0 5 2 7l7 2h54l7-2c2-2 3-4 3-7V46c0-3-1-5-3-7z"></path></svg>*/}
								<div className={'device-modem-type' + ((device.device_config.device_modem_type == device.device_config.device_modem_type_priority) ? ' match' : ' no-match')}>
									<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="170 0 170 500" height="20" width="20"><path d="M64 402H9c-3 0-5 1-6 3-2 1-3 3-3 6v55c0 3 1 5 3 7l6 2h55l7-2 2-7v-55l-2-6c-2-2-4-3-7-3zM283 292h-55l-6 3-3 6v165c0 3 1 5 3 7l6 2h55l7-2 2-7V301l-2-6c-2-2-4-3-7-3zM174 365h-55c-3 0-5 1-7 3s-2 4-2 7v91c0 3 0 5 2 7l7 2h55l6-2c2-2 3-4 3-7v-91c0-3-1-5-3-7l-6-3zM393 183h-55l-7 2-2 7v274l2 7 7 2h55l6-2c2-2 3-4 3-7V192c0-3-1-5-3-7l-6-2zM509 39c-2-2-4-2-7-2h-54c-3 0-5 0-7 2s-2 4-2 7v420c0 3 0 5 2 7l7 2h54l7-2c2-2 3-4 3-7V46c0-3-1-5-3-7z"/></svg>
								</div>
							</Tooltip>;
						} else if (device.device_config.device_modem_type == 2) {
							return <Tooltip title={(device.device_config.device_modem_type == device.device_config.device_modem_type_priority) ? 'Connected via WIFI' : 'Connected via ' + this.state.modem_types[device.device_config.device_modem_type] + ', but priority set to ' + this.state.modem_types[device.device_config.device_modem_type_priority]}>
								<div className={'device-modem-type' + ((device.device_config.device_modem_type == device.device_config.device_modem_type_priority) ? ' match' : ' no-match')}>
									<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="135 0 170 500" height="20" width="20"><path d="M455 161l-1-1a318 318 0 0 0-454 3l1 4 34 33a5 5 0 0 0 7 0 262 262 0 0 1 372 0 5 5 0 0 0 7 0l33-33a5 5 0 0 0 1-6z"/><path d="M228 154c-62 0-121 24-165 68v7l33 33a5 5 0 0 0 8 0 175 175 0 0 1 248 0 5 5 0 0 0 7 0l33-33a5 5 0 0 0 0-7c-44-44-102-68-164-68z"/><path d="M228 241c-39 0-75 15-103 43a5 5 0 0 0 0 7l33 33a5 5 0 0 0 7 0 88 88 0 0 1 125 0 5 5 0 0 0 7 0l33-33c2-2 2-5 0-7-27-28-63-43-102-43zM228 334c-14 0-26 5-36 15-2 2-2 5 0 7l32 32a5 5 0 0 0 7 0l33-32c2-2 2-5 0-7a50 50 0 0 0-36-15z"/></svg>
								</div>
							</Tooltip>;
						} else if (device.device_config.device_modem_type == 3) {
							return <Tooltip title={(device.device_config.device_modem_type == device.device_config.device_modem_type_priority) ? 'Connected via Ethernet' : 'Connected via ' + this.state.modem_types[device.device_config.device_modem_type] + ', but priority set to ' + this.state.modem_types[device.device_config.device_modem_type_priority]}>
								<div className={'device-modem-type' + ((device.device_config.device_modem_type == device.device_config.device_modem_type_priority) ? ' match' : ' no-match')}>
									<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20" width="20"><path d="M501 245H267v-53h64c6 0 10-5 10-11V53c0-6-4-10-10-10H181c-6 0-10 4-10 10v128c0 6 4 11 10 11h64v53H11a11 11 0 1 0 0 22h106v53H53c-6 0-10 5-10 11v128c0 6 4 10 10 10h150c6 0 10-4 10-10V331c0-6-4-11-10-11h-64v-53h234v53h-64c-6 0-10 5-10 11v128c0 6 4 10 10 10h150c6 0 10-4 10-10V331c0-6-4-11-10-11h-64v-53h106a11 11 0 1 0 0-22z"/></svg>
								</div>
							</Tooltip>;
						} else {
							return <Tooltip title={'Connection type not set'}>
								<div className={'device-modem-type' + ((device.device_config.device_modem_type == device.device_config.device_modem_type_priority) ? ' match' : ' no-match')}>
									<svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="30 -45 450 600"><path d="M256 0C115.39 0 0 115.39 0 256s115.39 256 256 256 256-115.39 256-256S396.61 0 256 0zM97.108 368.954C73.48 335.805 61 296.737 61 256c0-107.52 87.48-195 195-195 40.737 0 79.805 12.48 112.954 36.108a15.132 15.132 0 0 1 1.886 1.625L98.733 370.841a15.215 15.215 0 0 1-1.625-1.887zM256 451c-40.737 0-79.805-12.48-112.954-36.108a15.132 15.132 0 0 1-1.886-1.625l272.105-272.105a14.92 14.92 0 0 1 1.626 1.885C438.52 176.195 451 215.263 451 256c0 107.52-87.48 195-195 195z" /></svg>
								</div>
							</Tooltip>;
						}
					}
					
				},
			}, {
				title: 'Health',
				width: 50,
				align: 'center',
				key: 'health',
				render: (config) => {
					// console.log('configsssssssssssssss', config);
					if (config.device_config) {
						if(moment().tz('Asia/Kolkata').unix() - config.device_config.device_last_data_receive_timestamp <= 900) {
							if (config.device_config.device_error_status == 1) {
								return <Tooltip title={this.state.health_status[config.device_config.device_error_status]}>
									{/*<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448.8 448.8" width="16"><path fill="green" d="M142.8 324l-107-107.2L0 252.4l142.8 143 306-306-35.7-36"></path></svg>*/}
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448.8 448.8" width="16"><path fill="#18BC9C" d="M142.8 324l-107-107.2L0 252.4l142.8 143 306-306-35.7-36"/></svg>
								</Tooltip>
							} else {
								return <Tooltip title={this.state.health_status[config.device_config.device_error_status]}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 286.1 286.1" width="16"><path fill="#F25A59" d="M143 0C64 0 0 64 0 143s64 143 143 143 143-64 143-143S222 0 143 0zm0 259.2c-64.2 0-116.2-52-116.2-116.2S78.8 26.8 143 26.8s116.2 52 116.2 116.2-52 116.2-116.2 116.2zm0-196.5c-10.2 0-18 5.4-18 14V156c0 8.6 7.8 14 18 14 10 0 18-5.7 18-14V76.6c0-8.4-8-14-18-14zm0 125c-9.8 0-17.8 8-17.8 18 0 9.7 8 17.7 17.8 17.7s18-8 18-17.8c0-10-8-18-18-18z"/></svg>
								</Tooltip>
							}
						} else {
							return <Tooltip title={this.state.health_status[config.device_config.device_error_status]}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 286.1 286.1" width="16"><path fill="#F25A59" d="M143 0C64 0 0 64 0 143s64 143 143 143 143-64 143-143S222 0 143 0zm0 259.2c-64.2 0-116.2-52-116.2-116.2S78.8 26.8 143 26.8s116.2 52 116.2 116.2-52 116.2-116.2 116.2zm0-196.5c-10.2 0-18 5.4-18 14V156c0 8.6 7.8 14 18 14 10 0 18-5.7 18-14V76.6c0-8.4-8-14-18-14zm0 125c-9.8 0-17.8 8-17.8 18 0 9.7 8 17.7 17.8 17.7s18-8 18-17.8c0-10-8-18-18-18z"/></svg>
								</Tooltip>
						}
					}
				},
			}, {
				title: 'Analytics',
				width: 60,
				align: 'center',
				key: 'analytics',
				render: (config) => (
					<Tooltip title="View Analytics">
						<Link to={'/stations/' + (config.device_config ? config.device_config.id : 0) + '/analytics/'}>
							<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 486.742 486.742" className="analytics-image"><path fill="#ddd" d="M33 362.37v78.9c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-138.8l-44.3 44.3c-9.4 9.3-21.4 14.7-34.3 15.6zM142 301.47v139.8c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-82.3c-13.9-.3-26.9-5.8-36.7-15.6l-41.9-41.9zM251 350.27v91c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-167.9l-69.9 69.9c-2.7 2.7-5.6 5-8.7 7zM432.7 170.17l-72.7 72.7v198.4c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2l-1.3-1.3z"></path><path fill="#ddd" d="M482.6 41.37c-2.9-3.1-7.3-4.7-12.9-4.7h-1.6c-28.4 1.3-56.7 2.7-85.1 4-3.8.2-9 .4-13.1 4.5-1.3 1.3-2.3 2.8-3.1 4.6-4.2 9.1 1.7 15 4.5 17.8l7.1 7.2c4.9 5 9.9 10 14.9 14.9l-171.6 171.7-77.1-77.1c-4.6-4.6-10.8-7.2-17.4-7.2-6.6 0-12.7 2.6-17.3 7.2L7.2 286.87c-9.6 9.6-9.6 25.1 0 34.7l4.6 4.6c4.6 4.6 10.8 7.2 17.4 7.2s12.7-2.6 17.3-7.2l80.7-80.7 77.1 77.1c4.6 4.6 10.8 7.2 17.4 7.2 6.6 0 12.7-2.6 17.4-7.2l193.6-193.6 21.9 21.8c2.6 2.6 6.2 6.2 11.7 6.2 2.3 0 4.6-.6 7-1.9 1.6-.9 3-1.9 4.2-3.1 4.3-4.3 5.1-9.8 5.3-14.1.8-18.4 1.7-36.8 2.6-55.3l1.3-27.7c.3-5.8-1-10.3-4.1-13.5z"></path></svg>
						</Link>
					</Tooltip>
				),
			}, {
				title: 'Configure',
				width: 60,
				align: 'center',
				key: 'configure',
				render: (config) => (
					<Tooltip title="Configure Device">
						<Link to={'/devices/' + (config.device_config ? config.device_config.id : 0) + '/details/'}>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 451.8 451.8" width="22" height="20" className="configure-image" onClick={() => this.showConfigDevice()}><path fill="none" stroke="#DDD" stroke-width="56.5" d="M127.6 30.6L323 224 129.5 423" stroke-linecap="round" stroke-linejoin="round"></path></svg>
						</Link>
					</Tooltip>
				),
			}
		];

		return (
			<div id="device" className="mar-top-70">
				<Side active_link="device" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="head">Device Details</div>
						{(() => {
							if (this.state.tableVisible && this.state.device_details && this.state.device_details.length) {
								return <div>
									{/*<Row type="flex" justify="space-around" className="device-details">
										<Col span={7} className="width-100">
											<div className="pie-text">Total Number of Devices -
												<span className="online-device"> 66</span>
												<span> </span>
												(<span className="success">54 </span>/<span className="danger"> 12</span>)
											</div>	
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</Col>

										<Col span={8} className="activity-details">
											<Card title="Recent Activities" className="back-grey">
												<div className="activity-container">
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-1</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-2</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-3</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-4</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-5</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-6</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-7</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-8</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-9</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-10</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-11</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
												</div>
											</Card>
										</Col>
									</Row>*/}
									<div className="table-filter"><TreeSelect treeDefaultExpandAll onChange={(value, label, extra) => this.treeSelect(value, label, extra)} {...userProps} className="filter-icon" /></div>
									<div className="table-search">
										<Input placeholder="Search By Station Name" prefix={<Icon type="search" />} value={decodeURI(this.state.table_search)}
										onChange={(e) => this.searchBoxFilter(e.target.value)}/>
									</div>
									<Row>
										{(() => {
											// console.log('this.state.table_data', this.state.table_data);
											if (this.state.table_data && this.state.table_data.length) {
												return <div className="show-container">
													Showing <span className="show-txt">{this.state.filtered_stations.length}</span> out of <span className="show-txt">{this.state.device_details.length}</span>
												</div>;
											}
										})()}
									</Row>
									<Row>
										<Table columns={columns} locale={{emptyText: 'No Devices Found!' }} pagination={false} dataSource={this.state.table_data} scroll={{ y: 540 }} />
									</Row>
								</div>;
							} else if (this.state.device_details && this.state.device_details.length == 0) {
								return <div className="loading">
									<h3 className="table title-text">
										<center>No devices to show</center>
									</h3>
								</div>;
							} else {
								return <div className="loading">
									<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
								</div>;
							}
						})()}
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Devices;