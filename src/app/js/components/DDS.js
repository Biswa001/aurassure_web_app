import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Switch, Modal, Drawer, Card, Carousel, Upload, List, Avatar, Divider, DatePicker, TimePicker, message } from 'antd';
import Head from './imports/Head';
import Side from './imports/Side';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import _ from 'lodash';
// import Link from 'react-router/Link';
import io from 'socket.io-client';
import moment from 'moment-timezone';
import { Link } from 'react-router-dom';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const SHOW_CHILD = TreeSelect.SHOW_CHILD;
const { Content } = Layout;
const { Option } = Select;

const columns = [{
	title: 'Date',
	dataIndex: 'date',
	width: 120
}, {
	title: 'Title',
	dataIndex: 'title'
}, {
	title: 'Action',
	dataIndex: 'action',
	width: 60
}];

const pStyle = {
	fontSize: 16,
	color: 'rgba(0,0,0,0.85)',
	lineHeight: '24px',
	display: 'block',
	marginBottom: 16,
};

const uploadImageProps = {
	name: 'file',
	accept: "image/*",
	action: '//jsonplaceholder.typicode.com/posts/',
	headers: {
		authorization: 'authorization-text',
	},
	onChange(info) {
		if (info.file.status !== 'uploading') {
			console.log(info.file, info.fileList);
		}
		if (info.file.status === 'done') {
			message.success(info.file.name + ' image uploaded successfully');
		} else if (info.file.status === 'error') {
			message.error(info.file.name + ' image upload failed.');
		}
	},
};

const uploadVideoProps = {
	name: 'file',
	accept: "video/*",
	action: '//jsonplaceholder.typicode.com/posts/',
	headers: {
		authorization: 'authorization-text',
	},
	onChange(info) {
		if (info.file.status !== 'uploading') {
			console.log(info.file, info.fileList);
		}
		if (info.file.status === 'done') {
			message.success(info.file.name + ' video uploaded successfully');
		} else if (info.file.status === 'error') {
			message.error(info.file.name + ' video upload failed.');
		}
	},
};

const DescriptionItem = ({ title, content }) => (
	<div
		style={{
		fontSize: 14,
		lineHeight: '22px',
		marginBottom: 7,
		color: 'rgba(0,0,0,0.65)',
		}}
	>
		<p
			style={{
			marginRight: 8,
			display: 'inline-block',
			color: 'rgba(0,0,0,0.85)',
			}}
		>
			{title}:
		</p>
			{content}
	</div>
);

const history_data = [];
const upcomming_data = [];
const userFilter = [];

class PreviewDrawer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false
		};
	}

	showDrawer() {
		this.setState({
			visible: true,
		});
	};

	onClose() {
		this.setState({
			visible: false,
		});
	};

	render() {
		return (
			<div>
				<Drawer
					width={'calc(100vW - 200px)'}
					placement="right"
					closable={false}
					onClose={() => this.props.onClose()}
					visible={true}
				>
					<p style={{ marginBottom: 24 }}>User Profile</p>
					<p style={pStyle}>Personal</p>
					<Row>
						<Col span={12}>
							<DescriptionItem title="Full Name" content="Lily" />{' '}
						</Col>
						<Col span={12}>
							<DescriptionItem title="Account" content="AntDesign@example.com" />
						</Col>
					</Row>
					<Row>
						<Col span={12}>
							<DescriptionItem title="City" content="HangZhou" />
						</Col>
						<Col span={12}>
							<DescriptionItem title="Country" content="China🇨🇳" />
						</Col>
					</Row>
					<Row>
						<Col span={12}>
							<DescriptionItem title="Birthday" content="February 2,1900" />
						</Col>
						<Col span={12}>
							<DescriptionItem title="Website" content="-" />
						</Col>
					</Row>
					<Row>
						<Col span={24}>
							<DescriptionItem
								title="Message"
								content="Make things as simple as possible but no simpler."
							/>
						</Col>
					</Row>
					<Divider />
					<p style={pStyle}>Company</p>
					<Row>
						<Col span={12}>
							<DescriptionItem title="Position" content="Programmer" />
						</Col>
						<Col span={12}>
							<DescriptionItem title="Responsibilities" content="Coding" />
						</Col>
					</Row>
					<Row>
						<Col span={12}>
							<DescriptionItem title="Department" content="AFX" />
						</Col>
						<Col span={12}>
							<DescriptionItem title="Supervisor" content={<a>Lin</a>} />
						</Col>
					</Row>
					<Row>
						<Col span={24}>
							<DescriptionItem
								title="Skills"
								content="C / C + +, data structures, software engineering, operating systems, computer networks, databases, compiler theory, computer architecture, Microcomputer Principle and Interface Technology, Computer English, Java, ASP, etc."
							/>
						</Col>
					</Row>
					<Divider />
					<p style={pStyle}>Contacts</p>
					<Row>
						<Col span={12}>
							<DescriptionItem title="Email" content="AntDesign@example.com" />
						</Col>
						<Col span={12}>
							<DescriptionItem title="Phone Number" content="+86 181 0000 0000" />
						</Col>
					</Row>
					<Row>
						<Col span={24}>
							<DescriptionItem
								title="Github"
								content={(
									<a href="http://github.com/ant-design/ant-design/">
										github.com/ant-design/ant-design/
									</a>
								)}
							/>
						</Col>
					</Row>
				</Drawer>
			</div>
		);
	}
}

class PicturesWall extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			previewVisible: false,
			previewImage: false,
			fileList: [{
				uid: '1',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+1',
			},{
				uid: '2',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+2',
			},{
				uid: '3',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+3',
			},{
				uid: '4',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+4',
			},{
				uid: '5',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+5',
			},{
				uid: '6',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+6',
			},{
				uid: '7',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+7',
			},{
				uid: '8',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+8',
			},{
				uid: '9',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+9',
			},{
				uid: '10',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+10',
			},{
				uid: '10',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+11',
			},{
				uid: '10',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/1004x986.png?text=DDS+Dummy+Image+12',
			}],
		};
	}

	handleCancel() {
		this.setState({ previewVisible: false });
	}

	handlePreview(file) {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: false,
		});
	}

	handleChange() {
		({ fileList }) => this.setState({ fileList })
	}

	render() {
		const { previewVisible, previewImage, fileList } = this.state;
		const uploadButton = (
			<div>
				<Icon type="plus" />
				<div className="ant-upload-text">Upload</div>
			</div>
		);
		return (
			<div className="clearfix image-view-list">
				<Upload
					action="//jsonplaceholder.typicode.com/posts/"
					listType="picture-card"
					fileList={fileList}
					onPreview={(e) => {this.handlePreview(e)}}
					// onChange={(e) => {this.handleChange(e)}}
				>
					{fileList.length >= 3 ? null : null}
				</Upload>
				{/*<Modal visible={previewVisible} footer={null} onCancel={(e) => {this.handleCancel()}}>
					<img alt="example" style={{ width: '100%' }} src={previewImage} />
				</Modal>*/}
			</div>
		);
	}
}

class ConfigPictureWall extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			previewVisible: false,
			previewImage: false,
			fileList: [{
				uid: '1',
				name: 'check.png',
				status: 'done',
				url: 'https://via.placeholder.com/160x96.png?text=DDS+Dummy+Image+1',
			}],
		};
	}

	handleCancel() {
		this.setState({ previewVisible: false });
	}

	handlePreview(file) {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true,
		});
	}

	handleChange() {
		({ fileList }) => this.setState({ fileList })
	}

	render() {
		const { previewVisible, previewImage, fileList } = this.state;
		const uploadButton = (
			<div>
				<Icon type="plus" />
				<div className="ant-upload-text">Upload</div>
			</div>
		);
		return (
			<div className="clearfix image-view-list">
				<Upload
					action="//jsonplaceholder.typicode.com/posts/"
					listType="picture-card"
					fileList={fileList}
					onPreview={(e) => {this.handlePreview(e)}}
					// onChange={(e) => {this.handleChange(e)}}
				>
					{fileList.length >= 3 ? null : null}
				</Upload>
				<Modal visible={previewVisible} footer={null} onCancel={(e) => {this.handleCancel()}}>
					<img alt="example" style={{ width: '100%' }} src={previewImage} />
				</Modal>
			</div>
		);
	}
}

class DDS extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			active_tab: 'text',
			active_data_tab: 'upcomming',
			modal_view: null,
			config_drawer: false,
			confirm_modal: false
		};
		this.constructData();
	}

	setTableData() {
		let table_data = [];
		if (this.state.device_details && this.state.device_details.length) {
			this.state.device_details.map((device, index) => {
				table_data.push({
					key: index+1,
					id: device.id,
					qr: device.qr_code,
					station: device.name,
					city: this.state.device_cities[device.city_id],
					active: device.device_last_data_receive_timestamp,
					sync: device.device_sync_status,
					connect: device.device_modem_type,
					health: this.state.health_status[device.device_error_status],
					date: moment.unix(device.last_data_time).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
					timestamp: device.last_data_time,
					percent: device.online_percent,
					device_config: device
				});
			});
		}
		this.setState({
			table_data: table_data
		});
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'DDS System - Aurassure';
	}

	toggleShowOnlineDevices() {
		this.setState({date_time_online: !this.state.date_time_online}, () => this.updateFilter());
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * THis function sets the filtered list for tables according to the searched text.
	 * @param {String}
	 */

	onChangeFilter(value) {
		this.setState({ value });
	}

	showAddDrawer() {
		this.setState({ drawAddVisible: true });
	}

	handleCancel() {
		this.setState({ 
			drawAddVisible: false,
			modal_view: null
		});
	}

	onChange(value) {
		console.log('onChange ', value);
		this.setState({ value });
	}

	changeTab(data) {
		this.setState({
			active_tab: data
		});
	}

	changeDataTab(data) {
		this.setState({active_data_tab: data});
	}

	constructData() {
		for (let i = 0; i < 120; i++) {
			history_data.push({
				key: i,
				date: moment.unix(1547029331).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
				title: 'Edward King - ' + i,
				action: <Icon className="action-icon" type="right" onClick={() => this.viewModal('history')}/>
			});
		}
		for (let i = 0; i < 3; i++) {
			upcomming_data.push({
				key: i,
				date: moment.unix(1547029331).tz("Asia/Kolkata").format('HH:mm, DD MMM'),
				title: 'Edward King - ' + i,
				action: <Icon className="action-icon" type="right" onClick={() => this.viewModal('upcomming')} />
			});
		}
	}

	viewModal(data) {
		this.setState({
			modal_view: data
		});
	}

	openConfigurationDrawer() {
		this.setState({
			config_drawer: true
		});
	}

	closeConfigurationDrawer() {
		this.setState({
			config_drawer: false
		});
	}

	openConfirmationPopup() {
		this.setState({
			confirm_modal: true
		});
	}

	closeConfirmationPopup() {
		this.setState({
			confirm_modal: false
		});
	}


	render() {
		const { startValue, endValue, endOpen } = this.state;
		return (
			<div id="dds" className="mar-top-70">
				<Side active_link="dds" />
				<Head active_link="dds"/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="head">DDS Data</div>
						{/*(() => {
							if (this.state.tableVisible && this.state.device_details && this.state.device_details.length) {
								return <div>
									{<Row type="flex" justify="space-around" className="device-details">
										<Col span={7} className="width-100">
											<div className="pie-text">Total Number of Devices -
												<span className="online-device"> 66</span>
												<span> </span>
												(<span className="success">54 </span>/<span className="danger"> 12</span>)
											</div>	
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</Col>

										<Col span={8} className="activity-details">
											<Card title="Recent Activities" className="back-grey">
												<div className="activity-container">
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-1</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-2</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-3</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-4</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-5</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-6</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-7</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-8</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-9</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-10</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-11</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
												</div>
											</Card>
										</Col>
									</Row>}
									<div className="table-filter"><TreeSelect treeDefaultExpandAll onChange={(value, label, extra) => this.treeSelect(value, label, extra)} {...userProps} className="filter-icon" /></div>
									<div className="table-search">
										<Input placeholder="Search By Station Name" prefix={<Icon type="search" />} value={decodeURI(this.state.table_search)}
										onChange={(e) => this.searchBoxFilter(e.target.value)}/>
									</div>
									<Row>
										{(() => {
											// console.log('this.state.table_data', this.state.table_data);
											if (this.state.table_data && this.state.table_data.length) {
												return <div className="show-container">
													Showing <span className="show-txt">{this.state.filtered_stations.length}</span> out of <span className="show-txt">{this.state.device_details.length}</span>
												</div>;
											}
										})()}
									</Row>
									<Row>
										<Table columns={columns} locale={{emptyText: 'No Devices Found!' }} pagination={false} dataSource={this.state.table_data} scroll={{ y: 540 }} />
									</Row>
								</div>;
							} else if (this.state.device_details && this.state.device_details.length == 0) {
								return <div className="loading">
									<h3 className="table title-text">
										<center>No devices to show</center>
									</h3>
								</div>;
							} else {
								return <div className="loading">
									<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
								</div>;
							}
						})()*/}
						<div className="dds-form">
							<div className="dds-slider">
								<Carousel autoplay effect="fade" dots={false}>
									<div><img className="slider-single-image" src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+1" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+2" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+3" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+4" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+5" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+6" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+7" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+8" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+9" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+10" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+11" alt="check.png" /></div>
									<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+12" alt="check.png" /></div>
								</Carousel>
								<PicturesWall />
							</div>
							<div className="dds-data-view">
								<div className="dds-data-tabs">
									<div role="tablist" className="station-tabs-bar">
										<div className="station-tabs-nav-scroll">
											<div className="station-tabs-nav station-tabs-nav-animated">
												<div>
													<div role="tab" className={'station-tabs station-tabs-tab' + (this.state.active_data_tab && (this.state.active_data_tab == "upcomming") ? ' station-tabs-tab-active' : '')} onClick={() => this.changeDataTab('upcomming')}>Upcomming</div>
													<div role="tab" className={'station-tabs station-tabs-tab' + (this.state.active_data_tab && (this.state.active_data_tab == "history") ? ' station-tabs-tab-active' : '')} onClick={() => this.changeDataTab('history')}>History</div>
													<div className="dds-config-setting">
														<Icon type="setting" /*theme="filled"*/ onClick={() => this.openConfigurationDrawer()}/>
													</div>
												</div>
													<div className="blank-border"></div>
											</div>
										</div>
									</div>
								</div>
								{(() => {
									if (this.state.active_data_tab == 'upcomming') {
										return <Table size="middle" locale= {{emptyText: 'Nothing planned yet'}}  columns={columns} dataSource={upcomming_data} pagination={false} scroll={{ y: 430 }} />;
									} else if (this.state.active_data_tab == 'history') {
										return <Table size="middle" locale= {{emptyText: 'Nothing played yet'}}  columns={columns} dataSource={history_data} pagination={false} scroll={{ y: 430 }} />;
									}
								})()}
							</div>
							{(() => {
								if (this.state.config_drawer) {
									return <div className="dds-input-form">
										<Drawer
											width={'calc(100vW - 200px)'}
											placement="right"
											closable={true}
											onClose={() => this.closeConfigurationDrawer()}
											visible={true}
										>
											{/*DDS Tab for Config form*/}
											<div className="config-tab">
												<div role="tablist" className="station-tabs-bar">
													<div className="station-tabs-nav-scroll">
														<div className="station-tabs-nav station-tabs-nav-animated">
															<div>
																<div role="tab" aria-disabled="false" aria-selected="true" className={'station-tabs station-tabs-tab' + (this.state.active_tab && (this.state.active_tab == "text") ? ' station-tabs-tab-active' : '')} onClick={() => this.changeTab('text')}>Text</div>
																<div role="tab" aria-disabled="false" aria-selected="false" className={'station-tabs station-tabs-tab' + (this.state.active_tab && (this.state.active_tab == "image") ? ' station-tabs-tab-active' : '')} onClick={() => this.changeTab('image')}>Image</div>
																<div role="tab" aria-disabled="false" aria-selected="false" className={'station-tabs station-tabs-tab' + (this.state.active_tab && (this.state.active_tab == "video") ? ' station-tabs-tab-active' : '')} onClick={() => this.changeTab('video')}>Video</div>
															</div>
																<div className="blank-border"></div>
														</div>
													</div>
												</div>
											</div>
											<div className="complete-form">
												{(() => {
													if (this.state.active_tab == 'text') {
														return <div className="text-config-form">
															<Row gutter={50}>
																<Col span={12} className="wid-100">
																	<Form.Item label="Header Text">
																		<Input className="input" placeholder="Header Text" />
																	</Form.Item>
																</Col>
																<Col span={12} className="wid-100">
																	<Form.Item label="Header Font Color">
																		<Select defaultValue="#FFF" style={{ width: 180 }} /* onChange={handleChange}*/>
																			<Option value="#FFF">White</Option>
																			<Option value="yellow">Yellow</Option>
																			<Option value="green">Green</Option>
																			<Option value="red">Red</Option>
																		</Select>
																	</Form.Item>
																</Col>
															</Row>
															<Row gutter={50}>
																<Col span={12} className="wid-100">
																	<Form.Item label="Body Text">
																		<Input className="input" placeholder="Body Text" />
																	</Form.Item>
																</Col>
																<Col span={12} className="wid-100">
																	<Form.Item label="Body Font Color">
																		<Select defaultValue="#FFF" style={{ width: 180 }} /* onChange={handleChange}*/>
																			<Option value="#FFF">White</Option>
																			<Option value="yellow">Yellow</Option>
																			<Option value="green">Green</Option>
																			<Option value="red">Red</Option>
																		</Select>
																	</Form.Item>
																</Col>
															</Row>
															<Row gutter={50}>
																<Col span={12} className="wid-100">
																	<Form.Item label="Duration in Seconds">
																		<Input className="input" placeholder="Duration in seconds" />
																	</Form.Item>
																</Col>
																<Col span={12} className="wid-100">
																	<Checkbox /*onChange={onChange}*/>Add Time to Image Header</Checkbox>
																</Col>
															</Row>
															<Row gutter={25}>
																<Col span={16} className="wid-100">
																	{/*<Form.Item>
																		<Button type="primary">Save</Button>
																	</Form.Item>*/}
																</Col>
																<Col span={4} className="wid-100">
																	<Form.Item>
																		<Button type="primary">Save</Button>
																	</Form.Item>
																</Col>
																<Col span={4} className="wid-100">
																	<Form.Item>
																		<Button>Reset</Button>
																	</Form.Item>
																</Col>
															</Row>
															<div className="config-form-picture-wall">
																<ConfigPictureWall />
																<div className="add-to-que">
																	<Button size={'large'}>Add To Main Que</Button>
																</div>
															</div>
														</div>;
													} else if (this.state.active_tab == 'image') {
														return <div className="image-config-form">
															<Row gutter={50}>
																<Col span={12} className="wid-100">
																	<Upload {...uploadImageProps}>
																		<Button>
																			<Icon type="upload" /> Select Image to Upload
																		</Button>
																	</Upload>
																</Col>
															</Row>
															<Row gutter={50}>
																<Col span={12} className="wid-100">
																	<Form.Item label="Duration in Seconds">
																		<Input className="input" placeholder="Duration in seconds" />
																	</Form.Item>
																</Col>
															</Row>
															<div className="margin-upload">
																<Row gutter={25}>
																	<Col span={16} className="wid-100">
																		{/*<Form.Item>
																			<Button type="primary">Save</Button>
																		</Form.Item>*/}
																	</Col>
																	<Col span={4} className="wid-100">
																		<Form.Item>
																			<Button type="primary">Save</Button>
																		</Form.Item>
																	</Col>
																	<Col span={4} className="wid-100">
																		<Form.Item>
																			<Button>Reset</Button>
																		</Form.Item>
																	</Col>
																</Row>
															</div>
															<div className="config-form-picture-wall">
																<ConfigPictureWall />
																<div className="add-to-que">
																	<Button size={'large'}>Add To Main Que</Button>
																</div>
															</div>
														</div>;
													} else if (this.state.active_tab == 'video') {
														return <div className="video-config-form">
															<Upload {...uploadVideoProps}>
																<Button>
																	<Icon type="upload" /> Select Video to Upload
																</Button>
															</Upload>
															<div className="margin-upload">
																<Row gutter={25}>
																	<Col span={16} className="wid-100">
																		{/*<Form.Item>
																			<Button type="primary">Save</Button>
																		</Form.Item>*/}
																	</Col>
																	<Col span={4} className="wid-100">
																		<Form.Item>
																			<Button type="primary">Save</Button>
																		</Form.Item>
																	</Col>
																	<Col span={4} className="wid-100">
																		<Form.Item>
																			<Button>Reset</Button>
																		</Form.Item>
																	</Col>
																</Row>
																<div className="config-form-picture-wall">
																	<ConfigPictureWall />
																	<div className="add-to-que">
																		<Button size={'large'}>Add To Main Que</Button>
																	</div>
																</div>
															</div>
														</div>;
													}
												})()}
												<div className="final-config-form">
													<div className="final-config-que">
														<PicturesWall />
													</div>
													<div className="form-with-time">
														<Row gutter={50}>
															<Col span={12} className="wid-100">
																<Form.Item label="From Time">
																	<DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
																</Form.Item>
															</Col>
															<Col span={12} className="wid-100">
																<Form.Item label="Upto Time">
																	<DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
																</Form.Item>
															</Col>
														</Row>
														<Row gutter={50}>
															<Col span={12} className="wid-100">
																<Form.Item label="Repeat Interval">
																	<Select defaultValue="never" style={{ width: 180 }} /* onChange={handleChange}*/>
																		<Option value="never">Never</Option>
																		<Option value="hourly">Hourly</Option>
																		<Option value="daily">Daily</Option>
																		<Option value="weekly">Weekly</Option>
																		<Option value="monthly">Monthly</Option>
																		<Option value="annully">Annually</Option>
																	</Select>
																</Form.Item>
															</Col>
														</Row>
														<Row gutter={25}>
															<Col span={12} className="wid-100">
																{/*<Form.Item>
																	<Button type="primary">Save</Button>
																</Form.Item>*/}
															</Col>
															<Col span={6} className="wid-100">
																<Form.Item>
																	<Button type="primary" onClick={() => this.openConfirmationPopup('text')}>Preview</Button>
																</Form.Item>
															</Col>
															<Col span={4} className="wid-100">
																<Form.Item>
																	<Button>Cancel</Button>
																</Form.Item>
															</Col>
														</Row>
													</div>
												</div>
											</div>
											{(() => {
												if (this.state.confirm_modal) {
													return <Modal
														title="Final Confirmation"
														visible={true}
														onOk={() => this.closeConfirmationPopup()}
														onCancel={() => this.closeConfirmationPopup()}
														footer={[
															<Button key="back" onClick={() => this.closeConfirmationPopup()}>Cancel</Button>,
															<Button key="submit" type="primary" onClick={() => this.closeConfirmationPopup()}>Submit</Button>
														]}
													>
														<div className="modal-display-confirm">
															<PicturesWall />
															<Carousel autoplay effect="fade" dots={true}>
																<div><img className="slider-single-image" src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+1" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+2" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+3" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+4" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+5" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+6" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+7" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+8" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+9" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+10" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+11" alt="check.png" /></div>
																<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+12" alt="check.png" /></div>
															</Carousel>
														</div>
													</Modal>;
												}
											})()}
										</Drawer>
									</div>;
								}
							})()}
							{(() => {
								if (this.state.modal_view !== null) {
									return <div className="preview-drawer">
										<Drawer
											width={'calc(100vW - 200px)'}
											placement="right"
											closable={true}
											onClose={() => this.handleCancel()}
											visible={true}
										>
											<p style={{ marginBottom: 24, fontWeight: 700 }}>Screen Title  (Craeted at: 23 Dec 2018 & Executed at: 02 Jan 2019)</p>
											<div className="dds-preview-slider">
												<PicturesWall />
												<Carousel autoplay effect="fade" dots={true}>
													<div><img className="slider-single-image" src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+1" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+2" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+3" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+4" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+5" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+6" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+7" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+8" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+9" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+10" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+11" alt="check.png" /></div>
													<div><img className="slider-single-image"src="https://via.placeholder.com/640x400.png?text=DDS+Dummy+Image+12" alt="check.png" /></div>
												</Carousel>
											</div>
											{(() => {
												if (this.state.modal_view == 'upcomming') {
													return <div className="btn-to-edit">
														<Button type="primary" >Edit</Button>
													</div>;
												}
											})()}
										</Drawer>
									</div>;
								}
							})()}
						</div>
					</Content>
				</Layout>
			</div>
		);
	}
}

export default DDS;