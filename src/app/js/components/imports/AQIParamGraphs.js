import React from 'react';
import ReactHighcharts from 'react-highcharts';
import { Layout, Row, Col, Button, Select, Divider, Icon, Tabs, Input, Anchor } from 'antd';
import moment from 'moment-timezone';
import _ from 'lodash';

export default class AQIParamGraphs extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			view_mode: (window.innerWidth >= 1700) ? 'desktop' : 'mobile'
		};
	}

	componentDidMount() {}

	componentWillUnmount() {}

	sentenceCase(str) {
		if ((str===null) || (str==='')){
			return false;
		} else {
			str = str.toString();
		}
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}

	shouldComponentUpdate(nextProps) {
		if (nextProps.station_data && this.props.station_data && !_.isEqual(this.props.station_data, nextProps.station_data)) {
			return true;
		} else {
			return false;
		}
	}

	render() {
		return <div>
			{(() => {
				this.individual_param_aqi_graph_config = [];
				let should_plot_individual_param_aqi = [];
				let current_time = moment().unix();
				let previous_time = current_time - 3600;
				let categories = [];
				if (this.props.station_data.hourly_param_aqis) {
					Object.keys(this.props.station_data.hourly_param_aqis).map(function(key, index) {
						let bar_chart = [];
						bar_chart[index] = [];
						should_plot_individual_param_aqi[index] = false;
						this.props.station_data.hourly_param_aqis[key].map((value, i) => {
							let color = this.props.color_code.color_1;
							if(value >= 401) {
								color = this.props.color_code.color_6;
							} else if(value >= 301) {
								color = this.props.color_code.color_5;
							} else if(value >= 201) {
								color = this.props.color_code.color_4;
							} else if(value >= 101) {
								color = this.props.color_code.color_3;
							} else if(value >= 51) {
								color = this.props.color_code.color_2;
							}
							bar_chart[index].push({
								y: parseInt(value),
								color: color
							});
							categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));
							if(value) {
								should_plot_individual_param_aqi[index] = true;
							}
						});
						categories.reverse();
						bar_chart.map((data, i) => {
							bar_chart[i].reverse();
						});

						/**
						 * This is a Object to plot Individual parameter aqi graph data.
						 * @type {Object}
						 * @property {Object} chart This sets chart properties.
						 * @property {Object} plotOptions This customize the graph plot settings/options.
						 * @property {Object} title This sets the title for the graph.
						 * @property {Object} subtitle This sets the subtitle for the graph.
						 * @property {Object} xAxis This defines what to show in xAxis.
						 * @property {Object} yAxis This defines what to show in yAxis.
						 * @property {Object} legend This hides the printing options from graph.
						 * @property {Object} tooltip This sets what to show in tooltip.
						 * @property {Array} series This contains data for ploting graph.
						 */
						this.individual_param_aqi_graph_config[index] = {
							chart: {
								type: 'column',
								height: 80,
								width: ((this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.wdir) || (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.wspeed) || (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.press) ? 230 : (this.state.view_mode == 'desktop' ? 230 : 150))
							},
							plotOptions: {
								series: {
									pointPadding: 0,
									groupPadding: 0
								}				
							},
							title: {
								text: ''
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								title: {
									text: 'Past 24 hr Trend',
									labels: {
										style: {
											useHTML: true,
											paddingTop: '10px'
										}
									 }
								},
								type: '',
								lineWidth: 0,
								minorGridLineWidth: 0,
								lineColor: 'transparent',      
								labels: {
									enabled: false
								},
								minorTickLength: 0,
								tickLength: 0,
								categories: categories
							},
							yAxis: {
								title: {
									text: ''
								},
								lineWidth: 0,
								minorGridLineWidth: 0,
								lineColor: 'transparent',      
								labels: {
									enabled: false
								},
								minorTickLength: 0,
								tickLength: 0,
								gridLineColor: 'transparent'

							},
							legend: {
								enabled: false
							},
							tooltip: {
								pointFormat: '<span style="color:{point.color}">Index <b>{point.y}</b>'
							},
							series: [{
								data: bar_chart[index]
							}]
						};
					}, this);
				}

				let hourly_aqis = [];
				this.props.station_data.hourly_aqis.map((hourly_aqi) => {
					if(hourly_aqi) {
						hourly_aqis.push(parseInt(hourly_aqi));
					}
				});
				if (hourly_aqis.length) {
					let graphs = Object.keys(this.props.station_data.hourly_param_aqis).map((key, index) => {
						if(should_plot_individual_param_aqi[index]) {
							let current_param_index;
							for(let i=0; i<24; i++) {
								if(this.props.station_data.hourly_param_aqis[key][i]) {
									current_param_index = this.props.station_data.hourly_param_aqis[key][i];
									break;
								}
							}
							let color_class = 'aqi-1';
							if(current_param_index >= 401) {
								color_class = 'aqi-6';
							} else if(current_param_index >= 301) {
								color_class = 'aqi-5';
							} else if(current_param_index >= 201) {
								color_class = 'aqi-4';
							} else if(current_param_index >= 101) {
								color_class = 'aqi-3';
							} else if(current_param_index >= 51) {
								color_class = 'aqi-2';
							}
							console.log('all_data', this.props.station_data.params);
							if ((this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.wdir) || (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.wspeed) || (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.press)) {
								return (
									<Row className="aqi-pollutant-graphs" key={index}>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
											{/*dangerouslySetInnerHTML={{__html: _.find(this.props.station_data.params, { 'key': key }).name}}*/}
												<span className="pollutant-name" dangerouslySetInnerHTML={{__html: _.find(this.props.station_data.params, { 'key': key }).name}} />
												<span className="graph-aqi"><span className={'graph-aqi-value ' + color_class}>{current_param_index}</span>AQI</span>
											</div>
											<Col span={19}>
												<ReactHighcharts ref="chart" config={this.individual_param_aqi_graph_config[index]} />
											</Col>
											<Col span={5}>
												<span className="value mar-left">
													<div className="graph-value">{Math.min.apply(null, this.props.station_data.hourly_param_aqis[key].filter((value) => value ? true : false))}</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">{Math.max.apply(null, this.props.station_data.hourly_param_aqis[key].filter((value) => value ? true : false))}</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
									</Row>
								);
							} else {
								return <Row className="aqi-pollutant-graphs">
									<Row className="hr24-graph" align="bottom">
										<div className="graph-name">
											<span className="pollutant-name" dangerouslySetInnerHTML={{__html: _.find(this.props.station_data.params, { 'key': key }).name}}></span>
											<span className="graph-aqi"><span className={'graph-aqi-value ' + color_class}>{current_param_index}</span>AQI</span>
										</div>
										<Col span={16}>
											<ReactHighcharts config={this.individual_param_aqi_graph_config[index]} ref="chart"></ReactHighcharts>
										</Col>
										<Col span={8}>
											<span className="value">
												<div className="graph-value">{Math.min.apply(null, this.props.station_data.hourly_param_aqis[key].filter((value) => value ? true : false))}</div>
												<div>Min</div>
											</span>
											<span className="value">
												<div className="graph-value">{Math.max.apply(null, this.props.station_data.hourly_param_aqis[key].filter((value) => value ? true : false))}</div>
												<div>Max</div>
											</span>
										</Col>
									</Row>
								</Row>;
							}
						}
					}, this);

					return (<div>
						{graphs}
					</div>);
				} else {
					return(<div className="flex no-data-text">
						<center className="text-danger table">No data to show</center>
					</div>);
				}
			})()}
		</div>;
	}
}