import React from 'react';
import ReactHighcharts from 'react-highcharts';
import { Layout, Row, Col, Button, Select, Divider, Icon, Tabs, Input, Anchor } from 'antd';
import moment from 'moment-timezone';

export default class AQIGraph extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	componentDidMount() {}

	componentWillUnmount() {}

	render() {
		return <div>
			{(() => {
				if (this.props.aura_device_data) {
					let graph_data = [];
					let current_time = moment().unix();
					let previous_time = current_time - 3600;
					let categories = [];
					if (this.props.aura_device_data) {
						// console.log('color_code_props', window.color_code);
						this.props.aura_device_data.hourly_aqis.map((hourly_aqi, i) => {
							let color = window.color_code.color_1;
							if(hourly_aqi.aqi >= 401) {
								color = window.color_code.color_6;
							} else if(hourly_aqi.aqi >= 301) {
								color = window.color_code.color_5;
							} else if(hourly_aqi.aqi >= 201) {
								color = window.color_code.color_4;
							} else if(hourly_aqi.aqi >= 101) {
								color = window.color_code.color_3;
							} else if(hourly_aqi.aqi >= 51) {
								color = window.color_code.color_2;
							}
							graph_data.push({
								y: parseInt(hourly_aqi.aqi),
								param: ((hourly_aqi.param == "" || hourly_aqi.param == null) ? '-' : hourly_aqi.param.toUpperCase()),
								color: color
							});

							categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));
						});
						categories.reverse();
					}
					graph_data.reverse();

					this.config = {
						chart: {
							type: 'column',
							height: 80,
							//width: 330
						},
						plotOptions: {
							series: {
								pointPadding: 0,
								groupPadding: 0
							}/*,
							column: {
								pointWidth: 5
							}*/			
						},
						title: {
							text: ''
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							title: {
								enabled: true,
								text: '',
								style: {
									fontWeight: 'normal'
								}
							},
							type: '',
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							categories: categories
						},
						yAxis: {
							title: {
								text: ''
							},
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							gridLineColor: 'transparent'

						},
						legend: {
							enabled: false
						},
						tooltip: {
							pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
						},
						series: [{
							data: graph_data
						}]
					};
				}
				let hourly_aqis = [];
				this.props.aura_device_data.hourly_aqis.map((hourly_aqi) => {
					if(hourly_aqi.aqi) {
						hourly_aqis.push(parseInt(hourly_aqi.aqi));
					}
				});
				console.log('hour', hourly_aqis);

				if(hourly_aqis && hourly_aqis.length) {
					return <Row className="hr24-graph">
						<Col span={17}>
							<ReactHighcharts ref="chart" config={this.config} />
						</Col>
						<Col span={7}>
							<span className="value">
								<div className="graph-value">{Math.min.apply(null, hourly_aqis)}</div>
								<div>Min</div>
							</span>
							<span className="value">
								<div className="graph-value">{Math.max.apply(null, hourly_aqis)}</div>
								<div>Max</div>
							</span>
						</Col>
					</Row>;
				} else {
					console.log('entered into 2st condition');
					return (<div>No data To Show</div>);
				}
			})()}
		</div>;
	}
}