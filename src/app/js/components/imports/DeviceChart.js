import React from 'react';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
	/**
	 * Chart component of Devices page.
	 */
export default class DeviceChart extends React.Component {
	/**
	 * This is the Constructor for Dashboard page used to set the default task while page loads.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		/**
		* This sets the initial state for the page.
		* @type {Object}
		* @param {Array} device_list Used to store the device list.
		*/
		this.state = {
			device_list: [],
		};
		
		/**
		 * Chart configuration
		 * @type {Object}
		 */
		this.config = null;
	}

	/**
	 * Predefined function of ReactJS.
	 * Called after the component mounts.
	 * @return {void}
	 */
	componentDidMount() {
		let device_90 = [],
			device_60_90 = [],
			device_60 = [],
			device_0 = [];
		if (this.props.device_list && this.props.device_list.length) {
			this.props.device_list.map((device) => {
				if (device.online_percent > 90) {
					device_90.push(device);
				} else if (device.online_percent >= 60 && device.online_percent <= 90) {
					device_60_90.push(device);
				} else if (device.online_percent < 60 && device.online_percent > 0) {
					device_60.push(device);
				} else {
					device_0.push(device);
				}
			});
			let device_90_percent,
				device_60_percent,
				device_60_90_percent,
				device_0_percent;

			device_90_percent = parseInt(((device_90.length / this.props.device_list.length) * 100).toFixed(2));
			device_60_percent = parseInt(((device_60.length / this.props.device_list.length) * 100).toFixed(2));
			device_60_90_percent = parseInt(((device_60_90.length / this.props.device_list.length) * 100).toFixed(2));
			device_0_percent = parseInt(((device_0.length / this.props.device_list.length) * 100).toFixed(2));

			this.setState({
				device_list: this.props.device_list,
				device_90: device_90,
				device_60_90: device_60_90,
				device_60: device_60,
				device_0: device_0,
				device_90_percent: device_90_percent,
				device_60_percent: device_60_percent,
				device_60_90_percent: device_60_90_percent,
				device_0_percent: device_0_percent,

			});
		}
	}

	/**
	 * Predefined function of ReactJS.
	 * Called every time after the component updates
	 * @return {void}
	 */
	componentDidUpdate() {
		let device_90 = [],
			device_60_90 = [],
			device_60 = [],
			device_0 = [];
		if (this.props.device_list && this.props.device_list.length) {
			this.props.device_list.map((device) => {
				if (device.online_percent > 90) {
					device_90.push(device);
				} else if (device.online_percent >= 60 && device.online_percent <= 90) {
					device_60_90.push(device);
				} else if (device.online_percent < 60 && device.online_percent > 0) {
					device_60.push(device);
				} else {
					device_0.push(device);
				}
			});
			let device_90_percent,
				device_60_percent,
				device_60_90_percent,
				device_0_percent;

			device_90_percent = parseInt(((device_90.length / this.props.device_list.length) * 100).toFixed(2));
			device_60_percent = parseInt(((device_60.length / this.props.device_list.length) * 100).toFixed(2));
			device_60_90_percent = parseInt(((device_60_90.length / this.props.device_list.length) * 100).toFixed(2));
			device_0_percent = parseInt(((device_0.length / this.props.device_list.length) * 100).toFixed(2));
			let display_data = [{
					name: '>90%',
					y: device_90_percent,
					/*sliced: true,
					selected: true*/
				}, {
					name: '90-60%',
					y: device_60_90_percent
				}, {
					name: '<60%',
					y: device_60_percent
				}, {
					name: '0%',
					y: device_0_percent
				}];
			this.setData(display_data);
		}
	}

	/**
	 * This function sends the slected segment to the parent component.
	 * @param  {Object}
	 * @return {void}
	 */
	callBack(e) {
		console.log('pie e', e.name);
		this.props.setSelected(e.name);
	}

	/**
	 * This functions pushes the latest data to the pie chart.
	 * @param {Object}
	 */
	setData(value){
		if (this.child) {
			let chart = this.child.getChart();
			console.log(value);
			chart.series[0].update({
				data: value
			});
		}
	}

	/**
	 * Predefined function that decides whether to update the component or not.
	 * @param  {Object}
	 * @return {Boolean}
	 */
	shouldComponentUpdate(nextProps) {
		if ((nextProps.device_list && this.state.device_list && !_.isEqual(this.state.device_list, nextProps.device_list))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Predefined function of ReactJS to render the component.
	 * @return {Object}
	 */
	render() {
		let config = {
			chart: {
				renderTo: 'container',
				type: 'pie',
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				height: 285,
				width: 285
			},
			title: {
				text: ''
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: false
					},
					showInLegend: true
				},
			},
			series: [{
					name: 'Devices',
					colorByPoint: true,
					data: [{
							name: '>90%',
							y: this.state.device_90_percent,
							sliced: (this.props.selected_percent == '>90%' ? true : false),
							selected: (this.props.selected_percent == '>90%' ? true : false)
					}, {
							name: '90-60%',
							y: this.state.device_60_90_percent,
							sliced: (this.props.selected_percent == '90-60%' ? true : false),
							selected: (this.props.selected_percent == '90-60%' ? true : false)
					}, {
							name: '<60%',
							y: this.state.device_60_percent,
							sliced: (this.props.selected_percent == '<60%' ? true : false),
							selected: (this.props.selected_percent == '<60%' ? true : false)
					}, {
							name: '0%',
							y: this.state.device_0_percent,
							sliced: (this.props.selected_percent == '0%' ? true : false),
							selected: (this.props.selected_percent == '0%' ? true : false)
					}],
					point: {
					cursor: 'pointer',
						events: {
							click: (e) => {
								this.callBack(e.point);
								console.log('pie chart',e);
							}
						}
					}
			}]
		};
		return <div>
			{(() => {
				if (this.state.device_list && this.state.device_list.length) {
					return <ReactHighcharts neverReflow={true} config={config} ref={(child) => { this.child = child; }} />;
				}
			})()}
		</div>;
	}
}