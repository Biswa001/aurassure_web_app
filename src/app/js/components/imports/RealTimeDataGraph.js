import React from 'react';
import ReactHighcharts from 'react-highcharts';
import { Layout, Row, Col, Button, Select, Divider, Icon, Tabs, Input, Anchor, notification } from 'antd';
import moment from 'moment-timezone';
import WindParameterGadget from './WindParameterGadget';

const TabPane = Tabs.TabPane;
const { Content, Footer } = Layout;

function callback(key) {
	console.log(key);
}

export default class RealTimeDataGraph extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			current_param: props.current_param
		};
	}

	componentDidMount() {
		this.data_update_interval_handle = setInterval(() => this.update_station_data(), 15000);
	}

	componentWillUnmount() {
		if(this.data_update_interval_handle) {
			clearInterval(this.data_update_interval_handle);
		}
	}

	update_station_data() {
		var that = this;
		var parameters = [];
		if (that.props.station_data.params && that.props.station_data.params.length) {
			that.props.station_data.params.map((param) => {
				parameters.push(param.key);
			});
		}

		fetch('https://api.aurassure.com/smart_city_platform/get_updated_data_of_station_for_parameter.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: this.props.station_id,
				parameters: parameters,
				last_data_update_time: this.props.station_data.time_stamps[this.props.station_data.time_stamps.length - 1],
				last_hourly_data_update_time: this.props.station_data.last_data_update_time
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Refresh Data:',json);
			if (json.status === 'success') {
				if (that.props.station_data && json.time_stamps && json.time_stamps != null && json.param_values && json.param_values != null) {
					var data = that.props.station_data;
					// console.log('direction',that.refs.direction.getChart());
					// if (json.param_values.wspeed) {
					// 	let speed = that.refs.speed.getChart();
					// 	var point = speed.series[0].points[0];
					// 	let newVal;
					// 	if (json.param_values.wspeed && json.param_values.wspeed.length) {
					// 		newVal = parseFloat(json.param_values.wspeed[json.param_values.wspeed.length - 1]);
					// 	}
					// 	point.update(newVal);
					// }
					// if (json.param_values.wdir) {
					// 	let direction = that.refs.direction.getChart();
					// 	let point = direction.series[0].points[0];
					// 	let newVal;
					// 	if (json.param_values.wdir && json.param_values.wdir.length) {
					// 		newVal = parseFloat(json.param_values.wdir[json.param_values.wdir.length - 1]);
					// 	}
					// 	console.log(newVal);
					// 	point.update(newVal);
					// 	let point2 = direction.series[1].points[0];
					// 	point2.update(newVal);
					// }
					// if (json.param_values.press) {
					// 	let pressure = that.refs.pressure.getChart();
					// 	var point = pressure.series[0].points[0];
					// 	let newVal;
					// 	if (json.param_values.press && json.param_values.press.length) {
					// 		newVal = (parseFloat(json.param_values.press[json.param_values.press.length - 1]) * 0.750062);
					// 	}
					// 	point.update(newVal);
					// }
					if (that.refs.real_time_chart) {
						let real_time_chart = that.refs.real_time_chart.getChart();
						json.time_stamps.map((time_stamp, data_point_index) => {
							data.time_stamps.push(time_stamp);

							for (let param in json.param_values) {
								data.param_values[param].push(json.param_values[param][data_point_index]);
								if (time_stamp - data.time_stamps[0] > 24*3600) {
									data.param_values[param].shift();
								}
								if(param === that.props.current_param) {
									if (time_stamp - data.time_stamps[0] > 24*3600) {
										real_time_chart.series[0].removePoint(0);
									}
									console.log('addPoint', [
										time_stamp * 1000,
										parseFloat(json.param_values[param][data_point_index])
									]);
									if (that.props.current_param == 'rain') {
										real_time_chart.setTitle({
											text: 'Current value: ' + that.props.station_data.param_values[that.props.current_param][that.props.station_data.param_values[that.props.current_param].length - 1] + ' ' + that.current_param_unit + ' / ' + (that.props.station_data.param_values[that.props.current_param][that.props.station_data.param_values[that.props.current_param].length - 1] * 25.4) + ' mm'
										});
									} else {
										real_time_chart.setTitle({
											text: 'Current value: ' + that.props.station_data.param_values[that.props.current_param][that.props.station_data.param_values[that.props.current_param].length - 1] + ' ' + that.current_param_unit
										});
									}
									real_time_chart.series[0].addPoint([
										time_stamp * 1000,
										parseFloat(json.param_values[param][data_point_index])
									], true);
									that.props.station_data.time_stamps.push(time_stamp);
									{/*if (that.props.station_data.param_values.temperature) {
										document.getElementById('current_temp').innerHTML = Math.round(parseFloat(that.props.station_data.param_values.temperature[(that.props.station_data.param_values.temperature.length - 1)])) + ' °C';
									}
									if (that.props.station_data.param_values.humidity) {
										document.getElementById('current_humid').innerHTML = Math.round(parseFloat(that.props.station_data.param_values.humidity[(that.props.station_data.param_values.humidity.length - 1)])) + ' %';
									}
									if (that.props.station_data.param_values.wspeed) {
										document.getElementById('wind_speed_data').innerHTML = Math.round(parseFloat(that.props.station_data.param_values.wspeed[(that.props.station_data.param_values.wspeed.length - 1)])) + ' m / s';
									}
									if (that.props.station_data.param_values.wdir) {
										document.getElementById('wind_direction_data').innerHTML = Math.round(parseFloat(that.props.station_data.param_values.wdir[(that.props.station_data.param_values.wdir.length - 1)])) + ' °';
									}
									if (that.props.station_data.param_values.press) {
										document.getElementById('pressure_data').innerHTML = Math.round(parseFloat(that.props.station_data.param_values.press[(that.props.station_data.param_values.press.length - 1)])) + ' mBar';
									}*/}
								}
							}
							if (time_stamp - data.time_stamps[0] > 24*3600) {
								let removed_time_stamp = data.time_stamps.shift();
							}
						});
					}
				}

				if (json.update_hourly_avg_data === true) {
					if (that.props.station_data) {
						data.aqi = json.aqi;
						data.aqi_range = json.aqi_range;
						data.aqi_status = json.aqi_status;
						data.connection_status = json.connection_status;
						var i;

						data.hourly_aqis = data.hourly_aqis.slice(0, 24 - json.hourly_aqis.length);
						for (i = json.hourly_aqis.length - 1; i >= 0; i--) {
							data.hourly_aqis.unshift(json.hourly_aqis[i]);
						}

						for (let param in json.hourly_param_aqis) {
							data.hourly_param_aqis[param] = data.hourly_param_aqis[param].slice(0, 24 - json.hourly_param_aqis[param].length);
							for (i = json.hourly_param_aqis[param].length - 1; i >= 0 ; i--) {
								data.hourly_param_aqis[param].unshift(json.hourly_param_aqis[param][i]);
							}
						}

						for (let param in json.hourly_param_concs) {
							data.hourly_param_concs[param] = data.hourly_param_concs[param].slice(0, 24 - json.hourly_param_concs[param].length);
							for (i = json.hourly_param_concs[param].length - 1; i >= 0; i--) {
								data.hourly_param_concs[param].unshift(json.hourly_param_concs[param][i]);
							}
						}

						data.suggestions = json.suggestions;
						data.humid = json.humid;
						data.last_data_update_time = json.last_data_update_time;
						data.temp = json.temp;
					}

					that.setState({
						station_data: data
					});	
				}			
			} else {
				// showPopup('danger',json.message);
				that.openNotification('error', json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			// showPopup('danger','Unable to load data!');
			that.openNotification('error', 'Unable to load data!');
		});
	}

	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	};

	sentenceCase(str) {
		if ((str===null) || (str==='')){
			return false;
		} else {
			str = str.toString();
		}
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}

	render() {
		return <div>
			{(() => {
				if (this.props.station_data) {
					let line_chart_data = [];
					let unit = '';
					let graph_colors = [
						'#43429a',
						'#07adb1',
						'#a44a9c',
						'#f4801f',
						'#c14040',
						'#6fccdd',
						'#61c3ab',
						'#56bc7b',
						'#e2da3e',
						'#41ce00',
						'#aa4728',
						'#b3d23c',
						'#a0632a',
						'#7156a3',
						'#3d577f',
						'#ee3352',
						'#43429a',
						'#07adb1',
						'#a44a9c',
						'#f4801f',
						'#c14040',
						'#6fccdd',
						'#61c3ab',
						'#56bc7b'
					];
					let graph_fill_color = [
						'rgba(67, 66, 154, 0.05)',
						'rgba(7, 173, 177, 0.05)',
						'rgba(164, 74, 156, 0.05)',
						'rgba(244, 128, 31, 0.05)',
						'rgba(193, 64, 64, 0.05)',
						'rgba(111, 204, 221, 0.05)',
						'rgba(97, 195, 171, 0.05)',
						'rgba(86, 188, 123, 0.05)',
						'rgba(226, 218, 62, 0.05)',
						'rgba(65, 206, 0, 0.05)',
						'rgba(170, 71, 40, 0.05)',
						'rgba(179, 210, 60, 0.05)',
						'rgba(160, 99, 42, 0.05)',
						'rgba(113, 86, 163, 0.05)',
						'rgba(61, 87, 127, 0.05)',
						'rgba(238, 51, 82, 0.05)',
						'rgba(67, 66, 154, 0.05)',
						'rgba(7, 173, 177, 0.05)',
						'rgba(164, 74, 156, 0.05)',
						'rgba(244, 128, 31, 0.05)',
						'rgba(193, 64, 64, 0.05)',
						'rgba(111, 204, 221, 0.05)',
						'rgba(97, 195, 171, 0.05)',
						'rgba(86, 188, 123, 0.05)'
					];
					let i;

					if (this.props.station_data && this.props.current_param && this.props.station_data.param_values != null && this.props.station_data.param_values[this.props.current_param]) {
						this.props.station_data.param_values[this.props.current_param].map((value, index) => {
							line_chart_data.push([
								this.props.station_data.time_stamps[index] * 1000,
								parseFloat(value)
							]);
						});

						this.props.station_data.params.map((parameter, index) => {
							if (this.props.current_param === parameter.key) {
								unit = this.props.station_data.param_units[index];
								//set current param unit
								/**
								 * This is the current parameter's unit.
								 * @type {String}
								 */
								this.current_param_unit = unit;
								i = index;
							}
						});

						/**
						 * This is a Object to plot Line chart graph data.
						 * @type {Object}
						 * @property {Object} chart This sets chart properties.
						 * @property {Object} title This sets the title for the graph.
						 * @property {Object} subtitle This sets the subtitle for the graph.
						 * @property {Object} xAxis This defines what to show in xAxis.
						 * @property {Object} yAxis This defines what to show in yAxis.
						 * @property {Object} legend This hides the printing options from graph.
						 * @property {Object} tooltip This sets what to show in tooltip.
						 * @property {Object} plotOptions This customize the graph plot settings/options.
						 * @property {Array} series This contains data for ploting graph.
						 */
						
						this.line_chart_config = {
							chart: {
								zoomType: 'x',
								height: (window.innerHeight - 400)
							},
							title: {
								text: 'Current value: ' + (this.props.current_param == 'rain' ? this.props.station_data.param_values[this.props.current_param][this.props.station_data.param_values[this.props.current_param].length - 1] + ' ' + unit + ' / ' + (((this.props.station_data.param_values[this.props.current_param][this.props.station_data.param_values[this.props.current_param].length - 1] * 25.4).toFixed(2)) + ' mm') : this.props.station_data.param_values[this.props.current_param][this.props.station_data.param_values[this.props.current_param].length - 1] + ' ' + unit)
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								type: 'datetime',
								text: 'Time'
							},
							yAxis: {
								title: {
									text: unit
								}
							},
							legend: {
								enabled: false
							},
							tooltip: {
								pointFormat: (this.props.current_param == 'rain' ? '<span style="color:{point.color}">' + ('Conc. ') + '<b>{point.y}</b> ' + unit + ' / ' + '<b>{point.y}</b> ' + 'mm' + '<br/>' : '<span style="color:{point.color}">' + (this.props.current_param === 'temperature' || this.props.current_param === 'humidity' || this.props.current_param === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>')
							},
							plotOptions: {
								area: {
									marker: {
										radius: 0
									},
									lineWidth: 1,
									states: {
										hover: {
											lineWidth: 1
										}
									},
									threshold: null
								}
							},

							series: [{
								type: 'area',
								color: graph_colors[i],
								fillColor: graph_fill_color[i],
								data: line_chart_data
							}]
						};
					}

					ReactHighcharts.Highcharts.setOptions({
						global: {
							useUTC: false
						}
					});

					let individual_graph_data = [];
					let current_time = moment().unix();
					let previous_time = current_time - 3600;
					let categories = [];
					var breakpoint_table = {
						'pm2.5': [ 
							[0, 30],
							[31, 60],
							[61, 90],
							[91, 120],
							[121, 250],
							[251, 380]
						],
						'pm10': [
							[0, 50],
							[51, 100],
							[101, 250],
							[251, 350],
							[351, 430],
							[431, 510]
						],
						'no2': [
							[0, 40],
							[41, 80],
							[81, 180],
							[181, 280],
							[281, 400],
							[401, 500]
						],
						'o3': [
							[0, 50],
							[51, 100],
							[101, 168],
							[169, 208],
							[209, 748],
							[749, 800]
						],
						'co': [
							[0, 1.0],
							[1.1, 2.0],
							[2.1, 10],
							[10.1, 17],
							[17.1, 34],
							[34.1, 50]
						],
						'so2': [
							[0, 40],
							[41, 80],
							[81, 380],
							[381, 800],
							[801, 1600],
							[1601, 2000]
						],
						'nh3': [
							[0, 200],
							[201, 400],
							[401, 800],
							[801, 1200],
							[1201, 1800],
							[1801, 2000]
						],
						'pb': [
							[0, 0.5],
							[0.6, 1.0],
							[1.1, 2.0],
							[2.1, 3.0],
							[3.1, 3.5],
							[3.6, 4.0]
						]
					};

					if (this.props.station_data && this.props.station_data.param_values != null) {
						this.props.station_data.hourly_param_concs[this.props.current_param].map((hourly_conc, i) => {
							if (hourly_conc === null) {
								hourly_conc = 0;
							}

							let color = window.color_code.color_1;
							if (breakpoint_table[this.props.current_param] !== undefined) {
								let factor_usepa_to_naaqi;
								let decimal_upto;
								switch(this.props.current_param) {
									case 'no2':
										factor_usepa_to_naaqi = 46.0055/24.45;
										break;
									case 'o3':
										factor_usepa_to_naaqi = 48/24.45;
										break;
									case 'co':
										factor_usepa_to_naaqi = 28.01/24.45;
										break;
									case 'so2':
										factor_usepa_to_naaqi = 64.066/24.45;
										break;
									default:
										factor_usepa_to_naaqi = 1;
										break;
								}

								if (this.props.current_param == 'co' || this.props.current_param == 'pb') {
									decimal_upto = 1;
								} else {
									decimal_upto = 0
								}

								if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.props.current_param][5][0]) {
									color = window.color_code.color_6;
								} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.props.current_param][4][0]) {
									color = window.color_code.color_5;
								} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.props.current_param][3][0]) {
									color = window.color_code.color_4;
								} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.props.current_param][2][0]) {
									color = window.color_code.color_3;
								} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.props.current_param][1][0]) {
									color = window.color_code.color_2;
								} else if ((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.props.current_param][0][0]) {
									color = window.color_code.color_1;
								}
							}

							individual_graph_data.push({
								y: parseFloat(hourly_conc.toFixed(2)),
								mm: parseFloat((hourly_conc*25.4).toFixed(2)),
								color: color
							});

							categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));		
						});
						categories.reverse();
					}
					individual_graph_data.reverse();

					/**
					 * This is a Object to plot Individual graph data.
					 * @type {Object}
					 * @property {Object} chart This sets chart properties.
					 * @property {Object} plotOptions This customize the graph plot settings/options.
					 * @property {Object} title This sets the title for the graph.
					 * @property {Object} subtitle This sets the subtitle for the graph.
					 * @property {Object} xAxis This defines what to show in xAxis.
					 * @property {Object} yAxis This defines what to show in yAxis.
					 * @property {Object} legend This hides the printing options from graph.
					 * @property {Object} tooltip This sets what to show in tooltip.
					 * @property {Array} series This contains data for ploting graph.
					 */
					this.individual_graph_config = {
						chart: {
							type: 'column',
							height: (window.innerHeight >= 800) ? 200 : 175
						},
						plotOptions: {
							series: {
								pointPadding: 0,
								groupPadding: 0
							}
						},
						title: {
							text: this.props.current_param == 'rain' ? 'Value: ' + this.props.station_data.param_values['rain'][this.props.station_data.param_values['rain'].length - 1] + ' ' + unit + ' / ' + (((this.props.station_data.param_values['rain'][this.props.station_data.param_values['rain'].length - 1] * 25.4).toFixed(2)) + ' mm') : '1 Hour Average Report',
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							title: {
								enabled: true,
								text: 'Time',
								style: {
									fontWeight: 'normal'
								}
							},
							type: '',
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							categories: categories
						},
						yAxis: {
							title: {
								text: this.props.current_param === 'temperature' || this.props.current_param === 'humidity' || this.props.current_param === 'noise' ? 'Value ' : 'Conc. '
							},
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							gridLineColor: 'transparent'
						},
						plotOptions: {
							column: {
								pointPadding: 0,
								pointWidth: 30
							}
						},
						legend: {
							enabled: false
						},
						tooltip: {
							pointFormat: ((this.props.current_param == 'rain') ? '<span style="color:{point.color}">' + 'Conc. ' + '<b>{point.y}</b> ' + unit + ' / ' + '<b>{point.mm}</b> ' + 'mm' + '<br/>' : '<span style="color:{point.color}">' + (this.props.current_param === 'temperature' || this.props.current_param === 'humidity' || this.props.current_param === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>')
						},
						series: [{
							data: individual_graph_data
						}]
					};
				}

				if (this.props.station_data && this.props.station_data.param_values && Object.keys(this.props.station_data.param_values).length && this.props.station_data.time_stamps && this.props.station_data.time_stamps.length) {
					let allowed_parameters_in_order = ['pm1', 'pm2.5', 'pm10', 'so2', 'no', 'no2', 'co', 'o3', 'co2', 'o2', 'temperature', 'humidity', 'rain', 'uv', 'light', 'noise', 'voc', 'lint', 'nox', 'ch4', 'lead', 'wspeed', 'press', 'no3', 'ph', 'do', 'nh4', 'orp', 'us_mb', 'cur', 'nh3', 'us_hc04', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'];
					let current_params = {}, tab_content;
					this.props.station_data.params.map((param) => {
						current_params[param.key] = param;
					});
					// console.log('current_params', current_params);
					let parameters = allowed_parameters_in_order.map((param, i) => {
						// console.log('param', param);
						if(Object.keys(current_params).indexOf(param) >= 0) {
							tab_content = <span dangerouslySetInnerHTML={{__html: current_params[param].name}} />
							return(
								<TabPane tab={tab_content} key={current_params[param].key} dangerouslySetInnerHTML={{__html: current_params[param].name}} />
								// <div className={'parameter-btn hellip' + ((param === this.state.current_param)? ' active': '')} dangerouslySetInnerHTML={{__html: current_params[param].name}} onClick={() => {this.change_param(param);}} key={i} />
							);
						}
					});
					return <Content className="contains">
						<div className="contain mar-top-35">
							<Row>
								<Tabs activeKey={this.props.current_param} onChange={(activeKey) => {this.props.change_param(activeKey);}} type="card">
									{parameters}
								</Tabs>
							</Row>
							{(() => {
								if (this.props.current_param != 'rain') {
									return <Row className="gas-graph">
										<ReactHighcharts config={this.line_chart_config} ref="real_time_chart"></ReactHighcharts>
									</Row>;
								}
							})()}
							<Row className="gas-graph">
								<ReactHighcharts config={this.individual_graph_config} ref="hourly_data"></ReactHighcharts>
							</Row>
						</div>
					</Content>;
				}
			})()}
		</div>;
	}
}