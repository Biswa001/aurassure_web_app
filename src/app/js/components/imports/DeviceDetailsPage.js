import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Switch, Modal, Drawer, Card, notification } from 'antd';
// import Head from './imports/Head';
// import Side from './imports/Side';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import _ from 'lodash';
// import Link from 'react-router/Link';
import io from 'socket.io-client';
import moment from 'moment-timezone';
import { Link } from 'react-router-dom';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const confirm = Modal.confirm;

export default class DeviceDetailsPage extends React.Component {
	/**
	 * This is the Constructor for Dashboard page used to set the default task while page loads.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		this.state = {
			is_admin: document.getElementById('city_id') ? (document.getElementById('city_id').value == 5 ? true : false) : false,
			device_id: props.device_id,
			device_auth_token: null,
			location_name: null,
			location_type: null,
			all_cities: null,
			city_id: null,
			lat: null,
			long: null,
			device_mac_id: null,
			station_full_address: null,
			device_firmware_versions: ['1.0.0'],
			device_qr_code: null,
			device_firmware_version: null,
			iot_board_versions: ['1.0.0'],
			iot_board_qr_code: null,
			iot_board_version: null,
			shield_versions: ['1.0.0'],
			shield_qr_code: null,
			shield_version: null,
			sensor_board_versions: ['1.0.0'],
			sensor_board_qr_code: null,
			sensor_board_version: null,
			sensor_no: null,
			sim_mobile_no: null,
			sim_sl_no: null,
			device_archive_status: false,
			connectivity_gprs: false,
			connectivity_wifi: false,
			connectivity_ethernet: false,
			publicly_accessible: null,
			enable_data_push: null,
			data_loading: 1,
			saving_status: null,
			status: null,
			all_locations: {
				1 : 'Infra',
				2 : 'Aqua',
				3 : 'Weather'
			},
			group_placeholder: 'Please select a group',
		};
	}

	/**
	 * This function calls on success of the clipboard button and generate the success tooltip.
	 */
	/*onSuccess() {
		console.info('successfully coppied');
		let that = this,
			tooltip = findDOMNode(that.refs.copy);
		tooltip.setAttribute("data-tip", 'Copied!');
		// tooltip.setAttribute("data-tip-disable", false);
		ReactTooltip.hide(tooltip);
		ReactTooltip.rebuild();
		ReactTooltip.show(tooltip);
	}*/

	/**
	 * This function calls onMouseLeave and used for tooltip regeneration.
	 */
	/*removeTooltip() {
		findDOMNode(this.refs.copy).setAttribute("data-tip", 'Click to copy to clipboard.');
		// findDOMNode(this.refs.copy).setAttribute("data-tip-disable", true);
		ReactTooltip.rebuild();
	}*/

	/**
	 * This function uses for getting text for clipboard.
	 */
	getText() {
		return this.state.device_auth_token;
	}

	/**
	 * This enables the QR code fields accept key names.
	 * @param  {string} key This is the key of the qr input field.
	 */
	enableQrCodeAcception(key) {
		this.accept_scanned_qr_code = true;
		this.insert_qr_code_into = key;
		console.log('Focus In', key);
	}

	/**
	 * This disables the QR code fields and sets qr_code to null.
	 */
	disableQrCodeAcception() {
		this.accept_scanned_qr_code = false;
		this.insert_qr_code_into = null;
		console.log('Focus Out');
	}

	/*getQrCode() {
		this.socket.emit('receive_qr_code_from_scanner', JSON.stringify({
			qr_code: 'a1'
		}));
	}*/

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 */
	/*componentDidUpdate() {
		ReactTooltip.rebuild();
	}*/

	getGroupIds(values) {
		let that = this;
		console.log('values stations', values);
		
		this.setState({
			groups_selected: values
		});
		console.log('groups_select', that.state.groups_select);
		console.log('groups_selected', that.state.groups_selected);
	}

	setGroupList(){
		let that = this,
			groups_selected = that.state.groups_select,
			all_group_options = this.state.all_groups;
		console.log('station_lists', all_group_options);
		that.setState({
			all_group_options: all_group_options,
			groups_selected: groups_selected
		}, () => {
			that.resetSelectedStations();
		});
	}

	resetSelectedStations() {
		let that = this,
			selected = [];
		if (that.state.groups_selected) {
			that.state.groups_selected.map((id) => {
				Object.keys(that.state.all_groups).map((group_id) => {
					if(group_id == id && selected.indexOf(id) === -1) {
						selected.push(id);
					}
				});
			});
		}
		console.log('selected', selected);
		that.setState({
			groups_selected: selected
		});
	}

	/**
	 * This function used for saving details of the device.
	 */
	saveDeviceDetails() {
		console.log('save_device_details', this.state);
		this.socket.emit('save_device_details', JSON.stringify({
			device_id: this.state.device_id,
			device_auth_token: this.state.device_auth_token,
			location_id: this.state.location_id,
			location_name: this.state.location_name,
			location_type: this.state.location_type,
			selected_groups: this.state.groups_selected,
			city_id: this.state.city_id,
			lat: this.state.lat,
			long: this.state.long,
			device_mac_id: this.state.device_mac_id,
			station_full_address: this.state.station_full_address,
			device_qr_code: this.state.device_qr_code,
			device_firmware_version: this.state.device_firmware_version,
			iot_board_qr_code: this.state.iot_board_qr_code,
			iot_board_version: this.state.iot_board_version,
			shield_qr_code: this.state.shield_qr_code,
			shield_version: this.state.shield_version,
			sensor_board_qr_code: this.state.sensor_board_qr_code,
			sensor_board_version: this.state.sensor_board_version,
			sensor_no: this.state.sensor_no,
			sim_operator: this.state.sim_operator,
			sim_sl_no: this.state.sim_sl_no,
			sim_mobile_no: this.state.sim_mobile_no,
			sim_type: this.state.sim_type,
			device_archive_status: this.state.device_archive_status,
			connectivity_gprs: this.state.connectivity_gprs,
			connectivity_wifi: this.state.connectivity_wifi,
			connectivity_ethernet: this.state.connectivity_ethernet,
			publicly_accessible: this.state.publicly_accessible,
			enable_data_push: this.state.enable_data_push
		}));
		this.setState({saving_status: 1});
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Device Details - Chennai Smart City';

		/**
		 * Socket is used to sync the data between client and server in real time.
		 */
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
			query: {
				s_token: this.readCookie('PHPSESSID')
				// s_token: 'qd3gdp5gnqgi6svqj63c3ro1p6'
			}
		});
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket');
			console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			this.socket.emit('get_device_details', JSON.stringify({
				device_id: this.state.device_id
			}));
		});

		let device_details = {
		  "all_cities": {
		    "1": "Demo",
		    "2": "Rourkela",
		    "3": "Trinity",
		    "4": "Knowledge Lens",
		    "5": "Admin",
		    "6": "L&T Tech Services",
		    "7": "Vadodara",
		    "8": "Jindal",
		    "9": "iRam",
		    "10": "Raipur",
		    "11": "Rajkot",
		    "12": "AgartalaPOC",
		    "13": "BelagaviPOC",
		    "14": "Belagavi",
		    "15": "Aqua",
		    "16": "Siliguri",
		    "17": "Aurangabad",
		    "18": "Ecotech",
		    "19": "Coimbatore",
		    "20": "Udaipur",
		    "21": "Ranchi",
		    "22": "Iclei",
		    "23": "GoaPOC1",
		    "24": "GoaPOC2",
		    "25": "Convexicon",
		    "26": "Bhubaneswar",
		    "27": "Taru",
		    "28": "L & T",
		    "29": "Custom",
		    "30": "MangalorePOC",
		    "31": "Clean Air Asia",
		    "32": "Chennai",
		    "33": "ELPROENERGY",
		    "34": "TirupatiPOC"
		  },
		  "device_id": "53",
		  "device_mac_id": "2384723",
		  "device_qr_code": "",
		  "device_auth_token": "hzrAvsbk4Rjkl9sa9xcggsTmX6t45ojYQgkDvYvfa0DHbrHDniEjPYIhtp4UUghq",
		  "device_firmware_version": "1.0.0",
		  "location_name": "Smart City India Expo - Jaipur - May be demo device",
		  "location_type": null,
		  "station_full_address": "device",
		  "city_id": 5,
		  "lat": 26.9158,
		  "long": 75.7714103,
		  "publicly_accessible": false,
		  "enable_data_push": false,
		  "status": "offline",
		  "iot_board_qr_code": "",
		  "iot_board_version": "1.0.0",
		  "shield_qr_code": "",
		  "shield_version": "1.0.0",
		  "sensor_board_qr_code": "",
		  "sensor_board_version": "1.0.0",
		  "sensor_no": "",
		  "sim_operator": "",
		  "sim_sl_no": "",
		  "sim_mobile_no": "",
		  "sim_type": "",
		  "device_archive_status": false,
		  "connectivity_gprs": false,
		  "connectivity_wifi": false,
		  "connectivity_ethernet": false
		};
		console.log('update_device_details', device_details);
		if (device_details.device_id == this.state.device_id) {
			this.setState({
				device_id: device_details.device_id,
				device_auth_token: device_details.device_auth_token,
				location_id: device_details.location_id,
				location_name: device_details.location_name,
				location_type: device_details.location_type,
				all_cities: device_details.all_cities,
				all_groups: device_details.all_groups,
				groups_select: device_details.selected_groups,
				city_id: device_details.city_id,
				lat: device_details.lat,
				long: device_details.long,
				device_mac_id: device_details.device_mac_id,
				station_full_address: device_details.station_full_address,
				device_qr_code: device_details.device_qr_code,
				device_firmware_version: device_details.device_firmware_version === '' ? this.state.device_firmware_versions[0] : device_details.device_firmware_version,
				iot_board_qr_code: device_details.iot_board_qr_code,
				iot_board_version: device_details.iot_board_version === '' ? this.state.iot_board_versions[0] : device_details.iot_board_version,
				shield_qr_code: device_details.shield_qr_code,
				shield_version: device_details.shield_version === '' ? this.state.shield_versions[0] : device_details.shield_version,
				sensor_board_qr_code: device_details.sensor_board_qr_code,
				sensor_board_version: device_details.sensor_board_version === '' ? this.state.sensor_board_versions[0] : device_details.sensor_board_version,
				sensor_no: device_details.sensor_no,
				sim_operators: ['Airtel','BSNL','Vodafone','Custom'],
				sim_operator: device_details.sim_operator,
				sim_types: ['Prepaid','Postpaid'],
				sim_type: device_details.sim_type,
				sim_mobile_no: device_details.sim_mobile_no,
				sim_sl_no: device_details.sim_sl_no,
				device_archive_status: device_details.device_archive_status,
				connectivity_gprs: device_details.connectivity_gprs,
				connectivity_wifi: device_details.connectivity_wifi,
				connectivity_ethernet: device_details.connectivity_ethernet,
				publicly_accessible: device_details.publicly_accessible,
				enable_data_push: device_details.enable_data_push,
				data_loading: null,
				status: device_details.status
			}, () => {this.setGroupList()});
		}

		this.socket.on('update_device_details', (payload) => {
			let device_details = JSON.parse(payload);
			console.log('update_device_details', device_details);
			if (device_details.device_id == this.state.device_id) {
				this.setState({
					device_id: device_details.device_id,
					device_auth_token: device_details.device_auth_token,
					location_id: device_details.location_id,
					location_name: device_details.location_name,
					location_type: device_details.location_type,
					all_cities: device_details.all_cities,
					all_groups: device_details.all_groups,
					groups_select: device_details.selected_groups,
					city_id: device_details.city_id,
					lat: device_details.lat,
					long: device_details.long,
					device_mac_id: device_details.device_mac_id,
					station_full_address: device_details.station_full_address,
					device_qr_code: device_details.device_qr_code,
					device_firmware_version: device_details.device_firmware_version === '' ? this.state.device_firmware_versions[0] : device_details.device_firmware_version,
					iot_board_qr_code: device_details.iot_board_qr_code,
					iot_board_version: device_details.iot_board_version === '' ? this.state.iot_board_versions[0] : device_details.iot_board_version,
					shield_qr_code: device_details.shield_qr_code,
					shield_version: device_details.shield_version === '' ? this.state.shield_versions[0] : device_details.shield_version,
					sensor_board_qr_code: device_details.sensor_board_qr_code,
					sensor_board_version: device_details.sensor_board_version === '' ? this.state.sensor_board_versions[0] : device_details.sensor_board_version,
					sensor_no: device_details.sensor_no,
					sim_operators: ['Airtel','BSNL','Vodafone','Custom'],
					sim_operator: device_details.sim_operator,
					sim_types: ['Prepaid','Postpaid'],
					sim_type: device_details.sim_type,
					sim_mobile_no: device_details.sim_mobile_no,
					sim_sl_no: device_details.sim_sl_no,
					device_archive_status: device_details.device_archive_status,
					connectivity_gprs: device_details.connectivity_gprs,
					connectivity_wifi: device_details.connectivity_wifi,
					connectivity_ethernet: device_details.connectivity_ethernet,
					publicly_accessible: device_details.publicly_accessible,
					enable_data_push: device_details.enable_data_push,
					data_loading: null,
					status: device_details.status
				}, () => {this.setGroupList()});
			}
		});

		this.socket.on('update_qr_code', (payload) => {
			console.log('update_qr_code', JSON.parse(payload));
			let qr_code = JSON.parse(payload).qr_code;
			if (this.accept_scanned_qr_code) {
				let state_object = {};
				state_object[this.insert_qr_code_into] = qr_code;
				this.setState(state_object);
			}
		});

		this.socket.on('device_details_saved', (payload) => {
			console.log('device_details_saved', JSON.parse(payload));
			if (JSON.parse(payload).device_id == this.state.device_id) {
				this.setState({saving_status: null});
				this.openNotification('success', 'Details saved successfully!');
			}
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			// console.log('update_location_details_in_dashboard', JSON.parse(payload));
			if (JSON.parse(payload).device_id == this.state.device_id) {
				this.setState(JSON.parse(payload));
			}
		});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {
		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	};

	changeValues(key, value) {
		if (key == 'st_name') {
			this.setState({
				location_name: value
			});
		} else if (key == 'lat') {
			this.setState({
				lat: value
			});
		} else if (key == 'long') {
			this.setState({
				long: value
			});
		} else if (key == 'addr') {
			this.setState({
				station_full_address: value
			});
		}
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	render () {
		if(this.state.data_loading) {
			return <div className="panel-body details-list text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
		} else {
			return <div>
				<Form layout="vertical" hideRequiredMark>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Station Name">
								<Input placeholder="Please enter station name" value={this.state.location_name} onChange={(e) => this.changeValues('st_name', e.target.value)}/>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Device Latitude">
								<Input placeholder="Please enter device latitude" value={this.state.lat} onChange={(e) => this.changeValues('lat', e.target.value)}/>
							</Form.Item>
						</Col>
						<Col span={12} className="wid-100">
							<Form.Item label="Device Lontitude">
								<Input placeholder="Please enter device longitude" value={this.state.long} onChange={(e) => this.changeValues('long', e.target.value)}/>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Location Full Address">
								<Input.TextArea rows={4} placeholder="Please enter location full address" autosize={false} value={this.state.station_full_address} onChange={(e) => this.changeValues('addr', e.target.value)}/>
							</Form.Item>
						</Col>
					</Row>
				</Form>
				<div className="butn-contain">
					<Button type="primary" onClick={() => this.saveDeviceDetails()}>Save</Button>
				</div>
			</div>;
		}
	}
}