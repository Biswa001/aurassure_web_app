import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Switch, Modal, Drawer, Card, notification } from 'antd';
// import Head from './imports/Head';
// import Side from './imports/Side';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import _ from 'lodash';
// import Link from 'react-router/Link';
import io from 'socket.io-client';
import moment from 'moment-timezone';
import { Link } from 'react-router-dom';
import { findDOMNode } from 'react-dom';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const confirm = Modal.confirm;

function showDelete() {
	confirm({
		title: 'Do you want to delete ?',
		content: '',
		onOk() {},
		onCancel() {},
	});
}

const action = (
	<Menu>
		<Menu.Item key="action-1">Edit</Menu.Item>
		<Menu.Item key="action-2" onClick={showDelete}>Delete</Menu.Item>
	</Menu>
);

const AddForm = Form.create()(
	class extends React.Component {
		constructor(props) {
			super(props);
			this.state = {
				calib_field: [],
				index_param: (this.props.last_index)
			};

			/**
			 * This is used for City name.
			 * @type {string}
			 */
			this.calibration = [];
			this.calibration.push(<Col span={4} className="wid-100">
				<Form.Item label="WEzero">
					{/*{getFieldDecorator('email', {
						rules: [{
							type: 'number', message: 'The input is not valid value!',
						}],
					})(
					)}*/}
					<Input placeholder="WEzero" defaultValue='' value={this.props.param_calibs && this.props.param_calibs[this.props.last_index] ? this.props.param_calibs[this.props.last_index].we_zero : ''} onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'we_zero', e.target)} />
				</Form.Item>
			</Col>);
			this.calibration.push(<Col span={4} className="wid-100">
				<Form.Item label="AUXzero">
					<Input placeholder="AUXzero" defaultValue='' value={this.props.param_calibs && this.props.param_calibs[this.props.last_index] ? this.props.param_calibs[this.props.last_index].aux_zero : ''} onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'aux_zero', e.target)} />
				</Form.Item>
			</Col>);
			this.calibration.push(<Col span={4} className="wid-100">
				<Form.Item label="Sensitivity">
					<Input placeholder="Sensitivity" defaultValue='' value={this.props.param_calibs && this.props.param_calibs[this.props.last_index] ? this.props.param_calibs[this.props.last_index].sensitivity : ''} onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'sensitivity', e.target)} />
				</Form.Item>
			</Col>);
			this.state = {
				calib_field: this.calibration,
			};
			console.log('last_index', this.props.last_index);
		}

		changeCalibrationType(param) {
			console.log('changed', param);
			console.log(param);
			console.log('param_calibs', this.props.param_calibs.length);
			console.log('params', this.props.params.length);
			console.log('index_param', this.props.last_index);
			this.calibration = [];
			if (this.props.templates.template1.indexOf(param) > -1) {
				this.calibration = [];
				this.calibration.push(<Col span={4} className="wid-100">
					<Form.Item label="WEzero">
						{/*{getFieldDecorator('email', {
							rules: [{
								type: 'number', message: 'The input is not valid value!',
							}],
						})(
						)}*/}
						<Input placeholder="WEzero" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'we_zero', e.target)} />
					</Form.Item>
				</Col>);
				this.calibration.push(<Col span={4} className="wid-100">
					<Form.Item label="AUXzero">
						<Input placeholder="AUXzero" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'aux_zero', e.target)} />
					</Form.Item>
				</Col>);
				this.calibration.push(<Col span={4} className="wid-100">
					<Form.Item label="Sensitivity">
						<Input placeholder="Sensitivity" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'sensitivity', e.target)} />
					</Form.Item>
				</Col>);
				this.setState({calib_field: this.calibration});
			} else if (this.props.templates.template3.indexOf(param) > -1) {
				this.calibration = [];
				this.calibration.push(<Col span={4} className="wid-100">
					<Form.Item label="O2 V1">
						<Input placeholder="O2 V1" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'o2_v1', e.target)} />
					</Form.Item>
				</Col>);
				this.setState({calib_field: this.calibration});
			} else if (this.props.templates.template4.indexOf(param) > -1) {
				this.calibration = [];
				this.calibration.push(<Col span={4} className="wid-100">
					<Form.Item label="UV Adcmin">
						<Input placeholder="UV Adcmin" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'uv_adcmin', e.target)} />
					</Form.Item>
				</Col>);
				this.setState({calib_field: this.calibration});
			} else if (this.props.templates.template5.indexOf(param) > -1) {
				this.calibration = [];
				this.calibration.push(<Col span={4} className="wid-100">
					<Form.Item label="NH3 V1">
						<Input placeholder="NH3 V1" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'nh3_v1', e.target)} />
					</Form.Item>
				</Col>);
				this.setState({calib_field: this.calibration});
			} else if (this.props.templates.template6.indexOf(param) > -1) {
				this.calibration = [];
				this.calibration.push(<Col span={4} className="wid-100">
					<Form.Item label="zeroPoint">
						<Input placeholder="zeroPoint" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'zeroPoint', e.target)} />
					</Form.Item>
				</Col>);
				this.setState({calib_field: this.calibration});
			} else {
				this.calibration = [];
				this.setState({calib_field: this.calibration});
			}
		}

		render() {
			console.log('current_calib', this.props.current_calib);
			console.log('current_param', this.props.current_param);
			console.log('current_index', this.props.current_index);
			const { visible, onCancel, onCreate } = this.props;
			const { getFieldDecorator } = this.props.form;
			
			return (
				<div id="create_form">
					<Drawer
						title={(this.props.pathName.search('/edit/') > -1) ? 'Edit Calibration' : 'New Calibration'}
						width={1000}
						placement="right"
						visible={visible}
						onClose={onCancel}
						maskClosable={false}
						style={{
							height: 'calc(100% - 55px)',
							overflow: 'auto',
							paddingBottom: 53,
						}}
					>
						<Form layout="vertical" hideRequiredMark>
							<Row gutter={16}>
								<Col span={6} className="wid-100">
								{(() => {
									if (this.props.pathName.search('/edit/') > -1 || this.props.pathName.search('/edit') > -1) {
										return <Form.Item label={'Calibration Of' + ' ' + this.props.parameters[this.props.current_param]}>
										</Form.Item>;
									} else {
										return <Form.Item label="Calibration Type">
											<Select showSearch defaultValue={0} placeholder="Select Calibration Type" onChange={(e) => {this.props.changeCalibrationType(e, this.props.last_index)}}>
												<Option value={0} disabled>Calibration Type</Option>
												{(() => {
													let parameters = this.props.parameters;

													if (!this.props.is_admin) {
														let delete_param = this.props.templates.template2.map((del_param, id) => {
															delete parameters[del_param];
														});
													}

													let params = Object.keys(parameters).map((key, ind) => {
														return <Option value={key} key={ind}>{this.props.parameters[key]}</Option>;
													});
													return params;
												})()}
											</Select>
										</Form.Item>;
									}
								})()}
								</Col>
							</Row>
							<Row gutter={16}>
								{(() => {
									if (this.props.pathName.search('/edit/') > -1 || this.props.pathName.search('/edit') > -1) {
										let calibration = [];
										if (this.props.templates.template1.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="WEzero">
													{/*{getFieldDecorator('email', {
														rules: [{
															type: 'number', message: 'The input is not valid value!',
														}],
													})(
													)}*/}
													<Input placeholder="WEzero" value={this.props.current_calib.we_zero} onChange={(e) => this.props.changeCalibarationValues(this.props.current_index, 'we_zero', e.target)} />
												</Form.Item>
											</Col>);
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="AUXzero">
													<Input placeholder="AUXzero" value={this.props.current_calib.aux_zero} onChange={(e) => this.props.changeCalibarationValues(this.props.current_index, 'aux_zero', e.target)} />
												</Form.Item>
											</Col>);
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="Sensitivity">
													<Input placeholder="Sensitivity" value={this.props.current_calib.sensitivity} onChange={(e) => this.props.changeCalibarationValues(this.props.current_index, 'sensitivity', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template2.indexOf(this.props.current_param) > -1) {
											return calibration;
										} else if (this.props.templates.template3.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="O2 V1">
													<Input placeholder="O2 V1" value={this.props.current_calib.o2_v1} onChange={(e) => this.props.changeCalibarationValues(this.props.current_index, 'o2_v1', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template4.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="UV Adcmin">
													<Input placeholder="UV Adcmin" value={this.props.current_calib.uv_adcmin} onChange={(e) => this.props.changeCalibarationValues(this.props.current_index, 'uv_adcmin', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template5.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="NH3 V1">
													<Input placeholder="NH3 V1" value={this.props.current_calib.nh3_v1} onChange={(e) => this.props.changeCalibarationValues(this.props.current_index, 'nh3_v1', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template6.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="zeroPoint">
													<Input placeholder="zeroPoint" value={this.props.current_calib.zeroPoint} onChange={(e) => this.props.changeCalibarationValues(this.props.current_index, 'zeroPoint', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										}
									} else {
										let calibration = [];
										if (this.props.templates.template1.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="WEzero">
													{/*{getFieldDecorator('email', {
														rules: [{
															type: 'number', message: 'The input is not valid value!',
														}],
													})(
													)}*/}
													<Input placeholder="WEzero" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'we_zero', e.target)} />
												</Form.Item>
											</Col>);
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="AUXzero">
													<Input placeholder="AUXzero" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'aux_zero', e.target)} />
												</Form.Item>
											</Col>);
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="Sensitivity">
													<Input placeholder="Sensitivity" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'sensitivity', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template3.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="O2 V1">
													<Input placeholder="O2 V1" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'o2_v1', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template4.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="UV Adcmin">
													<Input placeholder="UV Adcmin" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'uv_adcmin', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template5.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="NH3 V1">
													<Input placeholder="NH3 V1" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'nh3_v1', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.props.templates.template6.indexOf(this.props.current_param) > -1) {
											calibration.push(<Col span={4} className="wid-100">
												<Form.Item label="zeroPoint">
													<Input placeholder="zeroPoint" defaultValue='' onChange={(e) => this.props.changeCalibarationValues(this.props.last_index, 'zeroPoint', e.target)} />
												</Form.Item>
											</Col>);
											return calibration;
										} else {
											calibration.push(<Col span={6} className="wid-100">
												<Form.Item label="Please Select a Parameter">
												</Form.Item>
											</Col>);
											return calibration;
										}
									}
								})()}
							</Row>
						</Form>
						<div
							style={{
								position: 'absolute',
								bottom: 0,
								width: '100%',
								borderTop: '1px solid #e8e8e8',
								padding: '10px 16px',
								textAlign: 'right',
								left: 0,
								background: '#fff',
								borderRadius: '0 0 4px 4px',
							}}
						>
							<Button
								style={{
									marginRight: 8,
								}}
								onClick={onCancel}
							>
								Cancel
							</Button>
							{(() => {
								if (this.props.pathName.search('/edit/') > -1) {
									return <Button onClick={() => this.props.saveDeviceCalibration()} type="primary">Save</Button>;
								} else {
									return <Button onClick={() => this.props.saveDeviceCalibration()} type="primary">Add</Button>;
								}
							})()}
						</div>
					</Drawer>
				</div>
			);
		}
	}
);

export default class DeviceCalibrationPage extends React.Component {
	/**
	 * This is the Constructor for Dashboard page used to set the default task while page loads.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		this.state = {
			is_admin: document.getElementById('city_id') ? (document.getElementById('city_id').value == 5 ? true : false) : false,
			device_id: props.device_id ? props.device_id : 1,
			allow_calibrations_success_msg: false,
			calibrations: null,
			data_sending_configs: null,
			device_qr_code: null,
			status: null,
			params: null,
			param_calibs: null,
			enable_save: true,
			param_data_sending_configs: null,
			sen_id: {
				'temperature': '1',
				'humidity': '2',
				'so2': '3',
				'no2': '4',
				'o3': '5',
				'no': '6',
				'co': '7',
				'co2': '8',
				'o2': '9',
				'noise': '10',
				'uv': '11',
				'press': '12',
				'pm1': '13',
				'pm2.5': '14',
				'pm10': '15',
				'wspeed': '16',
				'wdir': '17',
				'rain': '18',
				'us_mb': '19',
				'cur': '20',
				'nh3': '21',
				'us_hc04': '22'
			},
			parameters: {
				'pm1': 'PM1',
				'pm2.5': 'PM2.5',
				'pm10': 'PM10',
				'no': 'NO',
				'no2': 'NO2',
				'nox': 'NOx',
				'so2': 'SO2',
				'o2': 'O2 ( Winsen Ze-03 O2 )',
				'o3': 'O3',
				'ch4': 'CH4',
				'lead': 'Lead',
				'co': 'CO',
				'co2': 'CO2',
				'voc': 'VOC',
				'nh3': 'NH3',
				'temperature': 'Temperature',
				'humidity': 'Humidity',
				'noise': 'Noise',
				'rain': 'Rainfall',
				'uv': 'UV ( ML8511 )',
				'lint': 'Light',
				'wspeed': 'Wind Speed',
				'wdir': 'Wind Direction',
				'press': 'Atmospheric Pressure',
				'no3': 'Nitrate',
				'ph': 'pH',
				'do': 'Dissolved Oxygen',
				'nh4': 'Amonium',
				'us_mb': 'Ultrasonic ( Maxbotix )',
				'cur': 'Current Sensor',
				'nh3': 'Ammonia',
				'us_hc04': 'Ultrasonic ( HC04 )'
			},
			templates: {
				template1: ['no2','so2','co','o3','no'], //All 5 parameters - Alpha Sense
				template2: ['pm1','pm2.5','pm10','nox','co2','noise','lint','rain','wspeed','wdir','temperature','humidity','voc','lead','ch4','press','no3','ph','do','nh4','orp','cur'], //2 parameters
				template3: ['o2'], //3 parameters
				template4: ['uv'], //3 parameters
				template5: ['nh3'],
				template6: ['us_mb','us_hc04']
			},
			current_param: null
		};

		/**
		 * This is used for City name.
		 * @type {string}
		 */
		this.city_name = window.location.host.split('.')[0];
	}

	/**
	 * This function is used to check the parameter type and how many fields it'll have.
	 * @param  {string} param This is the selected parameter.
	 * @param  {number} index This is the index of selected parameters array.
	 */
	changeCalibrationType(param, index) {
		console.log(param,index);
		let params = this.state.params;
		params[index] = param;
		this.setState({current_param : param});

		let param_calibs = this.state.param_calibs ? this.state.param_calibs : [];

		if (this.state.templates.template1.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				we_zero: '0',
				aux_zero: '0',
				sensitivity: '0'
			};
		} else if (this.state.templates.template2.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
			};
		} else if (this.state.templates.template3.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				o2_v1: '1.0'
			};
		} else if (this.state.templates.template4.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				uv_adcmin: '0'
			};
		} else if (this.state.templates.template5.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				nh3_v1: '1.0'
			};
		} else if (this.state.templates.template6.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				zeroPoint: '0'
			};
		}

		let param_data_sending_configs = this.state.param_data_sending_configs ? this.state.param_data_sending_configs : [];
		param_data_sending_configs[index] = 'on';

		this.setState({
			params: params,
			param_calibs: param_calibs,
			param_data_sending_configs: param_data_sending_configs
		});
	}

	/**
	 * This function called after changing the values of the parameter and set that to state.
	 * @param  {number} index  This is the index of selected parameter.
	 * @param  {string} param  This is the key of the parameter which holds the value.
	 * @param  {number} target This is the value of the parameter.
	 */
	changeCalibarationValues(index, param, target) {
		console.log('index', index);
		console.log('param', param);
		console.log('target', target);
		let param_calibs = this.state.param_calibs;
		param_calibs[index][param] = target.value;
		this.setState({param_calibs: param_calibs});

		let value_check = document.getElementsByClassName('form-check'),
			check_fields = [];
		for (var i = 0; i < value_check.length; i++) {
			let valueCheck = value_check[i].value;
			// console.log(valueCheck.split('.')[1]);
			if (
				!isNaN(valueCheck.trim()) && 
				(valueCheck.trim() != '') && 
				!(
					(valueCheck.indexOf('.') > -1) && ((valueCheck.split('.')[0] === '') || (valueCheck.split('.')[1] === ''))
				)
			) {
				check_fields[i] = true;
				value_check[i].parentElement.className = 'form-config';
			} else {
				check_fields[i] = false;
				value_check[i].parentElement.className = 'form-config error';
			}
		}
		this.setState({enable_save: (check_fields.indexOf(false) > -1) ? false : true});
		// this.setState({});
	}

	showAddDrawer() {
		this.setState({ drawAddVisible: true });
	}

	handleCancel() {
		this.setState({ drawAddVisible: false });
	}

	/**
	 * This is a toggle that used to send data or not.
	 * @param  {number} index This is the index of selected parameter.
	 * @param {boolean} value This sats the toggle on or off.
	 */
	changeCalibarationDataSending(index, value) {
		let param_data_sending_configs = this.state.param_data_sending_configs;
		param_data_sending_configs[index] = value;
		this.setState({param_data_sending_configs: param_data_sending_configs});
	}

	/**
	 * This function is used to add a new row of parameter.
	 */
	addNewCalibarationRow() {
		let parameters = this.state.parameters;

		if (!this.state.is_admin) {
			let delete_param = this.state.templates.template2.map((del_param, id) => {
				delete parameters[del_param];
			});
		}
		let param = Object.keys(this.state.parameters)[0];

		let params = this.state.params ? this.state.params : [];
		params.push(param);

		let param_calibs = this.state.param_calibs ? this.state.param_calibs : [];
		// param_calibs.push({sen_id: this.state.sen_id[param]});
		if (this.state.templates.template1.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				we_zero: '0',
				aux_zero: '0',
				sensitivity: '0'
			});
		} else if (this.state.templates.template2.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1'
			});
		} else if (this.state.templates.template3.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				o2_v1: '1.0'
			});
		} else if (this.state.templates.template4.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				uv_adcmin: '0'
			});
		} else if (this.state.templates.template5.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				nh3_v1: '1.0'
			});
		} else if (this.state.templates.template6.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				zeroPoint: '0'
			});
		}

		let param_data_sending_configs = this.state.param_data_sending_configs ? this.state.param_data_sending_configs : [];
		param_data_sending_configs.push('on');

		this.setState({
			params: params,
			param_calibs: param_calibs,
			param_data_sending_configs: param_data_sending_configs
		});
	}

	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	};

	/**
	 * This function is used to remove a parameter row.
	 * @param  {number} index This is the parameter index.
	 */
	removeCalibarationRow(index) {
		let params = this.state.params;
		params.splice(index, 1);

		let param_calibs = this.state.param_calibs;
		param_calibs.splice(index, 1);

		let param_data_sending_configs = this.state.param_data_sending_configs;
		param_data_sending_configs.splice(index, 1);

		this.setState({
			params: params,
			param_calibs: param_calibs,
			param_data_sending_configs: param_data_sending_configs
		});
		this.saveDeviceCalibration();
		console.log('params', params);
		console.log('params calib', param_calibs);
		console.log('params data sending', param_data_sending_configs);
	}

	/**
	 * This function is to save Caliberations for device.
	 */
	saveDeviceCalibration() {
		let calibrations = {};
		calibrations['device_id'] = this.state.device_id;
		calibrations['calibrations'] = {};
		calibrations['data_sending_configs'] = {};
		this.state.params.map((param, index) => {
			calibrations['calibrations'][param] = this.state.param_calibs[index];
			calibrations['data_sending_configs'][param] = (this.state.param_data_sending_configs[index] == true) ? 'on' : 'off';
			console.log('Saving Calibrations', calibrations);
		});
		this.closeDrawerForm();
		console.log('Saved Calibrations', JSON.stringify(calibrations));
		this.socket.emit('save_device_calibrations', JSON.stringify(calibrations));
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		if (this.props.location.pathname.search('/edit/') > -1 || this.props.location.pathname.search('/edit') > -1) {
			this.props.history.push('/devices/' + this.state.device_id +'/calibration/');
			document.title = 'Device Calibration - Chennai Smart City';
		} else if (this.props.location.pathname.search('/add/') > -1 || this.props.location.pathname.search('/add') > -1) {
			this.props.history.push('/devices/' + this.state.device_id +'/calibration/');
			document.title = 'Device Calibration - Chennai Smart City';
		}

		document.title = 'Device Calibration - Chennai Smart City';

		/**
		 * Socket is used to sync the data between client and server in real time.
		 */
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
			query: {
				s_token: this.readCookie('PHPSESSID')
				// s_token: 'qd3gdp5gnqgi6svqj63c3ro1p6'
			}
		});
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket');
			console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			this.socket.emit('get_device_calibrations', JSON.stringify({
				device_id: this.state.device_id
			}));
		});

		this.socket.on('update_device_calibrations', (payload) => {
			console.log('update_device_calibrations', JSON.parse(payload));
			if (this.state.device_id == JSON.parse(payload).device_id) {
				let calib = JSON.parse(payload),
					saved_calib = JSON.parse(payload),
					params = [],
					param_calibs = [],
					param_data_sending_configs = [],
					saved_params = [],
					saved_param_calibs = [],
					saved_param_data_sending_configs = [];
				if (calib.calibrations) {
					Object.keys(calib.calibrations).map((param, index) => {
						params.push(param);
						param_calibs.push(calib.calibrations[param]);
						param_data_sending_configs.push((calib.data_sending_configs[param] == 'on') ? true : false);
						saved_params.push(param);
						saved_param_calibs.push(saved_calib.calibrations[param]);
						saved_param_data_sending_configs.push((saved_calib.data_sending_configs[param] == 'on') ? true : false);
					});
				}
				let saved_calibrations = {
					params: saved_params,
					param_calibs: saved_param_calibs,
					param_data_sending_configs: saved_param_data_sending_configs
				};
				this.setState({
					params: params,
					param_calibs: param_calibs,
					param_data_sending_configs: param_data_sending_configs,
					last_index: param_calibs ? param_calibs.length : 0,
					status: calib.status,
					device_qr_code: calib.device_qr_code,
					saved_calibrations: saved_calibrations
				});
				console.log('call to previous state of caliberation',saved_calibrations);
				if (this.state.allow_calibrations_success_msg) {
					this.openNotification('success', 'Calibration saved successfully!');
					// showPopup('success', 'Configurations saved successfully!');
				} else {
					this.setState({
						allow_calibrations_success_msg: true
					});
				}
			}
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			// console.log('update_location_details_in_dashboard', JSON.parse(payload));
			if (JSON.parse(payload).device_id == this.state.device_id) {
				this.setState(JSON.parse(payload));
			}
		});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {

		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 * @param  {object} prevProps This is the Previous saved props.
	 * @param  {object} prevState This is the Previous saved state.
	 */
	componentDidUpdate(prevProps, prevState) {
		// if (this.state.saved_calibrations) {
		// 	/*console.log('param calib', this.state.saved_calibrations.param_calibs);
		// 	console.log('data sending', this.state.saved_calibrations.param_data_sending_configs);*/
		// 	if (
		// 		(_.isEqual(this.state.saved_calibrations.param_calibs, this.state.param_calibs) && 
		// 		_.isEqual(this.state.saved_calibrations.param_data_sending_configs, this.state.param_data_sending_configs) && 
		// 		_.isEqual(this.state.saved_calibrations.params, this.state.params)) || (this.state.enable_save == false)
		// 	) {
		// 		findDOMNode(this.refs.saveCalib).setAttribute("disabled", true);
		// 	} else {
		// 		findDOMNode(this.refs.saveCalib).removeAttribute("disabled");
		// 	}
		// }
		// ReactTooltip.rebuild();
	}

	editCalibration(value, index, param) {
		console.log('edit_value', value);
		console.log('index', index);
		console.log('calib_check', this.state.param_calibs[index]);
		this.setState({
			current_calib: this.state.param_calibs[index],
			calib_index: index,
			current_param: param
		});
		this.props.history.push('/devices/' + this.state.device_id +'/calibration/edit/');
		this.showAddDrawer();
		document.title = 'Device Calibration - Modify Calibrtaion - Chennai Smart City';
	}

	closeDrawerForm() {
		this.setState({
			current_calib: null,
			calib_index: null,
			current_param: null,
			// param_calibs: this.state.saved_calibrations.param_calibs,
			// params: this.state.saved_calibrations.params,
			// param_data_sending_configs: this.state.saved_calibrations.param_data_sending_configs
		});
		this.handleCancel();
		this.props.history.push('/devices/' + this.state.device_id +'/calibration/');
		document.title = 'Device Calibration - Chennai Smart City';
	}

	openDrawerForm() {
		this.setState({
			current_calib: null,
			calib_index: null,
			current_param: null
		});
		this.props.history.push('/devices/' + this.state.device_id +'/calibration/add/');
		this.showAddDrawer();
		document.title = 'Device Calibration - Add New Calibrtaion - Chennai Smart City';
	}

	showDeleteConfirm(value, index) {
		console.log('delete_value', value);
		console.log('index', index);
		document.title = 'Device Calibration - Delete Calibrtaion - Chennai Smart City';
		let that = this;
		confirm({
			title: 'Do you want to delete ?',
			content: '',
			onOk() {
				that.removeCalibarationRow(index);
				document.title = 'Device Calibration - Chennai Smart City';
			},
			onCancel() {
				// that.closeAddEditModal();
				document.title = 'Device Calibration - Chennai Smart City';
			}
		});
	}

	render () {
		if(this.state.data_loading) {
			return<div className="text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
		} else {
			return <div>
				<div className="btn-contain">
					<Button className="add-btn" type="primary" icon="plus" onClick={() =>this.openDrawerForm()}>Add New</Button>
				</div>
				<Form layout="vertical" className="calibration-form" hideRequiredMark>
					{(() => {
						if (this.state.saved_calibrations && this.state.saved_calibrations.params && this.state.saved_calibrations.params.length) {
							let parameters = this.state.parameters;

							if (1) {
								let delete_param = this.state.templates.template2.map((del_param, id) => {
									delete parameters[del_param];
								});
							}

							let calibrations = this.state.saved_calibrations.params.map((param, index) => {
								return <Row gutter={16} className={'calib' + (!this.state.is_admin ? (this.state.templates.template2.includes(param) ? ' hide' : '') : '')} key={index}>
									<Col span={5}>
										<Form.Item label="Calibration Type">
											<div className="input-local-class-out">
												<div className="input-local-class-in">{this.state.parameters[param]}</div>
											</div>
											{/*<Select showSearch defaultValue="no2" placeholder="Please select calibration type">
												<Option value="no2">NO2</Option>
												<Option value="co2">CO2</Option>
												<Option value="co">CO</Option>
												<Option value="pm2.5">PM2.5</Option>
												<Option value="pm10">PM10</Option>
												<Option value="noise">Noise</Option>
											</Select>*/}
										</Form.Item>
									</Col>
									{(() => {
										let calibration = [];
										if (this.state.templates.template1.indexOf(param) > -1) {
											calibration.push(<Col span={3}>
												<Form.Item label="WEzero">
													<div className="input-local-class-out">
														<div className="input-local-class-in">{this.state.saved_calibrations.param_calibs[index].we_zero}</div>
													</div>
													{/*<Input placeholder="Please enter value" defaultValue="290" />*/}
												</Form.Item>
											</Col>);
											calibration.push(<Col span={3}>
												<Form.Item label="AUXzero">
													<div className="input-local-class-out">
														<div className="input-local-class-in">{this.state.saved_calibrations.param_calibs[index].aux_zero}</div>
													</div>
													{/*<Input placeholder="Please enter value" defaultValue="290" />*/}
												</Form.Item>
											</Col>);
											calibration.push(<Col span={3}>
												<Form.Item label="Sensitivity">
													<div className="input-local-class-out">
														<div className="input-local-class-in">{this.state.saved_calibrations.param_calibs[index].sensitivity}</div>
													</div>
													{/*<Input placeholder="Please enter value" defaultValue="290" />*/}
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.state.templates.template2.indexOf(param) > -1) {
											return null;
										} else if (this.state.templates.template3.indexOf(param) > -1) {
											calibration.push(<Col span={3}>
												<Form.Item label="O2 V1">
													<div className="input-local-class-out">
														<div className="input-local-class-in">{this.state.saved_calibrations.param_calibs[index].o2_v1}</div>
													</div>
													{/*<Input placeholder="Please enter value" defaultValue="290" />*/}
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.state.templates.template4.indexOf(param) > -1) {
											calibration.push(<Col span={3}>
												<Form.Item label="UV Adcmin">
													<div className="input-local-class-out">
														<div className="input-local-class-in">{this.state.saved_calibrations.param_calibs[index].uv_adcmin}</div>
													</div>
													{/*<Input placeholder="Please enter value" defaultValue="290" />*/}
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.state.templates.template5.indexOf(param) > -1) {
											calibration.push(<Col span={3}>
												<Form.Item label="NH3 V1">
													<div className="input-local-class-out">
														<div className="input-local-class-in">{this.state.saved_calibrations.param_calibs[index].nh3_v1}</div>
													</div>
													{/*<Input placeholder="Please enter value" defaultValue="290" />*/}
												</Form.Item>
											</Col>);
											return calibration;
										} else if (this.state.templates.template6.indexOf(param) > -1) {
											calibration.push(<Col span={3}>
												<Form.Item label="zeroPoint">
													<div className="input-local-class-out">
														<div className="input-local-class-in">{this.state.saved_calibrations.param_calibs[index].zeroPoint}</div>
													</div>
													{/*<Input placeholder="Please enter value" defaultValue="290" />*/}
												</Form.Item>
											</Col>);
											return calibration;
										}
									})()}
									<Col span={2} className=" right">
										<Form.Item label=" ">
											<Dropdown overlay={
												<Menu>
													<Menu.Item key="action-1" onClick={(e) => this.editCalibration(e, index, param)}>Edit</Menu.Item>
													<Menu.Item key="action-2" onClick={(e) => this.showDeleteConfirm(e, index, param)}>Delete</Menu.Item>
												</Menu>
												} trigger={['click']} placement="bottomLeft">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 512" className="action-img"><path d="M204 102c28 0 51-23 51-51S232 0 204 0s-51 23-51 51 23 51 51 51zm0 51c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51zm0 153c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51z"/></svg>
											</Dropdown>
										</Form.Item>
									</Col>
								</Row>;
							});
							return calibrations;
						} else if (this.state.params && (this.state.params.length == 0)) {
							return <center className="panel-body details-list text-center">No Calibration settings found for device.</center>;
						} else {
							return <div className="panel-body details-list text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
						}
					})()}
					{/*<Row gutter={16} className="calib">
						<Col span={5}>
							<Form.Item label="Calibration Type">
								<div className="input-local-class-out">
									<div className="input-local-class-in">NO2</div>
								</div>
							</Form.Item>
						</Col>
						<Col span={3}>
							<Form.Item label="Sensitivity">
								<div className="input-local-class-out">
									<div className="input-local-class-in">0.267</div>
								</div>
							</Form.Item>
						</Col>
						<Col span={3}>
							<Form.Item label="Offset">
								<div className="input-local-class-out">
									<div className="input-local-class-in">0</div>
								</div>
							</Form.Item>
						</Col>
						<Col span={3}>
							<Form.Item label="Multiplier">
								<div className="input-local-class-out">
									<div className="input-local-class-in">1</div>
								</div>
							</Form.Item>
						</Col>
						<Col span={2} className=" right">
							<Form.Item label=" ">
								<Dropdown overlay={action} trigger={['click']} placement="bottomLeft">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 512" className="action-img"><path d="M204 102c28 0 51-23 51-51S232 0 204 0s-51 23-51 51 23 51 51 51zm0 51c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51zm0 153c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51z"/></svg>
								</Dropdown>
							</Form.Item>
						</Col>
					</Row>*/}
				</Form>
				{(() => {
					if (this.state.last_index >= 0) {
						return <AddForm
							current_calib={this.state.current_calib}
							current_param={this.state.current_param}
							current_index={this.state.calib_index}
							parameters={this.state.parameters}
							templates={this.state.templates}
							params={this.state.params}
							param_calibs={this.state.param_calibs}
							last_index={this.state.last_index}
							pathName={this.props.location.pathname}
							saved_calibrations={this.state.saved_calibrations}
							visible={this.state.drawAddVisible}
							saveDeviceCalibration={() => this.saveDeviceCalibration()}
							changeCalibrationType={(a,b,c) => this.changeCalibrationType(a,b,c)}
							changeCalibarationValues={(a,b,c) => this.changeCalibarationValues(a,b,c)}
							onCancel={() =>this.closeDrawerForm()}
							onCreate={() =>this.handleCancel()}
						/>;
					}
				})()}
			</div>;
		}
	}
}