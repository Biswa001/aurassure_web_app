import React from 'react';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import { Layout, Row, Col, Button, Select, Divider, Icon, Tabs, Input, Anchor, notification } from 'antd';
import moment from 'moment-timezone';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

export default class WindParameterGadget extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	componentDidMount() {
		this.data_update_interval_handle = setInterval(() => this.update_station_data(), 15000);
	}

	componentWillUnmount() {
		if(this.data_update_interval_handle) {
			clearInterval(this.data_update_interval_handle);
		}
	}

	update_station_data() {
		var that = this;
		var parameters = [];
		if (that.props.station_data.params && that.props.station_data.params.length) {
			that.props.station_data.params.map((param) => {
				parameters.push(param.key);
			});
		}

		fetch('https://api.aurassure.com/smart_city_platform/get_updated_data_of_station_for_parameter.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: this.props.station_id,
				parameters: parameters,
				last_data_update_time: this.props.station_data.time_stamps[this.props.station_data.time_stamps.length - 1],
				last_hourly_data_update_time: this.props.station_data.last_data_update_time
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			// console.log('Refresh Data:',json);
			if (json.status === 'success') {
				if (that.props.station_data && json.time_stamps && json.time_stamps != null && json.param_values && json.param_values != null) {
					var data = that.props.station_data;
					// console.log('direction',that.refs.direction.getChart());
					if (json.param_values.wspeed) {
						let speed = that.refs.speed.getChart();
						var point = speed.series[0].points[0];
						let newVal;
						if (json.param_values.wspeed && json.param_values.wspeed.length) {
							newVal = parseFloat(json.param_values.wspeed[json.param_values.wspeed.length - 1]);
						}
						point.update(newVal);
					}
					if (json.param_values.wdir) {
						let direction = that.refs.direction.getChart();
						let point = direction.series[0].points[0];
						let newVal;
						if (json.param_values.wdir && json.param_values.wdir.length) {
							newVal = parseFloat(json.param_values.wdir[json.param_values.wdir.length - 1]);
						}
						console.log(newVal);
						point.update(newVal);
						let point2 = direction.series[1].points[0];
						point2.update(newVal);
					}
					if (json.param_values.press) {
						let pressure = that.refs.pressure.getChart();
						var point = pressure.series[0].points[0];
						let newVal;
						if (json.param_values.press && json.param_values.press.length) {
							newVal = (parseFloat(json.param_values.press[json.param_values.press.length - 1]) * 0.750062);
						}
						point.update(newVal);
					}
				}
		
			} else {
				// showPopup('danger',json.message);
				that.openNotification('error', json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			// showPopup('danger','Unable to load data!');
			that.openNotification('error', 'Unable to load data!');
		});
	}

	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	};

	render() {
		return <div>
			{(() => {
				if (this.props.station_data) {
					this.speed_index;
					this.wind_index;
					this.pressure_index;
					if (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.wdir) {
						this.wind_index = this.props.station_data.params.findIndex(x => x.key=="wdir");
						this.wind_direction = {
							chart: {
								type: 'gauge',
								height: 120,
								// width: 120
							},
							exporting: {
								enabled: false
							},
							credits: {
								enabled: false
							},

							title: {
								text: ''
							},
							tooltip: {
								enabled: false,
								backgroundColor: '#FCFFC5',
								borderColor: '#fff',
								pointFormat: '{series.name}: <b>{point.y}</b><br/>',
								valueSuffix: ' °'
							},
							 plotOptions: {
								series: {
									dataLabels: {
										enabled: false
									}
								}
							},

							pane: {
								startAngle: 0,
								endAngle: 360,
								background: [{
									borderWidth: 1,
									outerRadius: '50%'
								}, {
									backgroundColor: '#fff',
									borderWidth: 4,
									outerRadius: '10%'
								}, {
									backgroundColor: '#fff',
									borderWidth: 4,
									outerRadius: '10%'
								}, {
									// default background
								}, {
									backgroundColor: '#f1f1f1',
									borderWidth: 0,
									outerRadius: '105%',
									innerRadius: '0%'
								}]
							},
							// the value axis
							yAxis: [{
								title: {
									text: '',
								},
								min: 0,
								max: 360,
								lineColor: '#333',
								offset: -10,
								tickInterval: 0,
								tickWidth: 2,
								tickPosition: 'outside',
								tickLength: 10,
								tickColor: '#333',
								minorTickInterval: 5,
								minorTickWidth: 1,
								minorTickLength: 10,
								minorTickPosition: 'outside',
								minorTickColor: '#666',
								labels: {
									step: 1,
									distance: -12,
									rotation: 'auto'
								},
							}, {
								title: {
									text: '',
								},
								type: 'category',
								categories: ['N', '' , 'NE', '', 'E', '', 'SE', '', 'S', '', 'SW', '' , 'W', '', 'NW', '', 'N'],
								min: 0,
								max: 16,
								lineColor: '#ddd',
								offset: -20,
								tickInterval: 1,
								tickWidth: 1,
								tickPosition: 'outside',
								tickLength: 20, // =50-10
								tickColor: '#ddd',
								minorTickInterval: 1,
								minorTickWidth: 0,
								minorTickLength: 50,
								minorTickPosition: 'inside',
								minorTickColor: '#0f0',
								labels: {
									step: 1,
									distance: 10,
									rotation: 'auto'
								},
								endOnTick: true,
							}, {
								type: 'number',
								title: {
									text: '',
								},
								labels: {
									enabled: false,
								},
								min: 0,
								max: 12,
								lineColor: '#ddd',
								offset: -50,
								tickInterval: 10,
								tickWidth: 0,
								tickPosition: 'inside',
								tickLength: 45,
								tickColor: '#ddd',
								minorTickWidth: 0,
								endOnTick: false,
							}],

							series: [{
								name: 'East',
								data: [parseFloat(this.props.station_data.latest_param_value.wdir)],
								dial: {
									radius: '60%',
									baseWidth: 10,
									baseLength: '0%',
									rearLength: 0,
									borderWidth: 1,
									borderColor: '#9A0000',
									backgroundColor: '#CC0000',
								}
							}, {
								name: 'West',
								data: [parseFloat(this.props.station_data.latest_param_value.wdir)],
								dial: {
									radius: '-60%',
									baseWidth: 10,
									baseLength: '0%',
									rearLength: 0,
									borderWidth: 1,
									borderColor: '#1B4684',
									backgroundColor: '#3465A4',
								}
							}]
						};
					}

					if (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.wspeed) {
						this.speed_index = this.props.station_data.params.findIndex(x => x.key=="wspeed");
						this.wind_speed = {
							chart: {
								type: 'gauge',
								height: 120,
								// width: 120
							},
							title: {
								text: ''
							},
							pane: {
								startAngle: -150,
								endAngle: 150,
								background: [{
									backgroundColor: '#fff',
									borderWidth: 0,
									outerRadius: '109%'
								}, {
									backgroundColor: '#fff',
									borderWidth: 1,
									outerRadius: '107%'
								}, {
									// default background
								}, {
									backgroundColor: '#f1f1f1',
									borderWidth: 0,
									outerRadius: '105%',
									innerRadius: '0%'
								}]
							},
							// the value axis
							yAxis: {
								min: 0,
								max: 60,
								minorTickInterval: 'auto',
								minorTickWidth: 1,
								minorTickLength: 10,
								minorTickPosition: 'inside',
								minorTickColor: '#666',
								tickPixelInterval: 30,
								tickWidth: 2,
								tickPosition: 'inside',
								tickLength: 10,
								tickColor: '#666',
								labels: {
									step: 2,
									distance: -18,
									rotation: 'auto',
									style: {
										fontSize: '8px'
									}
								},
								title: {
									text: ''
								},
								plotBands: [{
									from: 0,
									to: 20,
									color: '#55BF3B' // green
								}, {
									from: 20,
									to: 40,
									color: '#DDDF0D' // yellow
								}, {
									from: 40,
									to: 60,
									color: '#DF5353' // red
								}]
							},
							plotOptions: {
								series: {
									dataLabels: {
										enabled: false
									}
								}
							},
							exporting: {
								enabled: false
							},
							credits: {
								enabled: false
							},
							tooltip: {
								enabled: false
							},
							series: [{
								data: [parseFloat(this.props.station_data.latest_param_value.wspeed)]
							}]
						};
					}

					if (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.press) {
						this.pressure_index = this.props.station_data.params.findIndex(x => x.key=="press");
						this.pressure = {
							chart: {
								type: 'gauge',
								alignTicks: false,
								plotBackgroundColor: null,
								plotBackgroundImage: null,
								plotBorderWidth: 0,
								plotShadow: false,
								height: 120,
								// width: 120
							},
							title: {
								text: ''
							},
							pane: {
								startAngle: -150,
								endAngle: 150,
								background: [{
									borderWidth: 1,
									outerRadius: '50%'
								}, {
									backgroundColor: '#fff',
									borderWidth: 4,
									outerRadius: '10%'
								}, {
									backgroundColor: '#fff',
									borderWidth: 4,
									outerRadius: '10%'
								}, {
									// default background
								}, {
									backgroundColor: '#f1f1f1',
									borderWidth: 0,
									outerRadius: '105%',
									innerRadius: '0%'
								}]
							},
							yAxis: [{
								min: 960,
								max: 1160,
								tickPosition: 'outside',
								lineColor: '#933',
								lineWidth: 2,
								minorTickPosition: 'outside',
								tickColor: '#933',
								minorTickColor: '#933',
								tickLength: 5,
								minorTickLength: 5,
								labels: {
									enabled: true,
									distance: 6,
									rotation: 'auto',
									 style: {
										 fontSize: '8px'
									 }
								},
								offset: -12,
								endOnTick: false
							}],
							exporting: {
								enabled: false
							},
							credits: {
								enabled: false
							},
							tooltip: {
								enabled: false
							},
							series: [{
								data: [(parseFloat(this.props.station_data.latest_param_value.press) * 0.750062)],
								dataLabels: {
									enabled: "true",
									formatter: function () {
										let mmHg = this.y.toFixed(2),
											mBar = Math.round(mmHg / 0.750062).toFixed(2);
										// return '<span style="color:#339">' + mBar + ' ' + that.props.station_data.param_units[this.pressure_index]+ '</span><br/>' +'<span style="color:#933">' + mmHg + ' mmHg</span>';
									},
									backgroundColor: '#fff'
								}
							}]
						};
					}
				}

				if (this.props.station_data && this.props.station_data.latest_param_value) {
					return <Row type="flex" justify="space-around" className="pad-bot-40">
						{(() => {
							if (this.props.station_data.latest_param_value.wspeed) {
								return <Col className="center mar-top-20" span={7}>
									<ReactHighcharts config={this.wind_speed} ref="speed"></ReactHighcharts>
									<div className="para-value">
										<div id="wind_speed_data">{this.props.station_data.latest_param_value.wspeed + ' ' + this.props.station_data.param_units[this.speed_index]}</div>
										<div className="bold">Wind Speed</div>
									</div>
								</Col>;
							}
						})()}
						{(() => {
							if (this.props.station_data.latest_param_value.wdir) {
								return <Col className="center mar-top-20" span={7}>
									<ReactHighcharts config={this.wind_direction} ref="direction"></ReactHighcharts>
									<div className="para-value">
										<div id="wind_direction_data">{parseInt(this.props.station_data.latest_param_value.wdir) + this.props.station_data.param_units[this.wind_index]}</div>
										<div className="bold">Wind Direction</div>
									</div>
								</Col>;
							}
						})()}
						{(() => {
							if (this.props.station_data.latest_param_value.press) {
								return <Col className="center mar-top-20" span={7}>
									<ReactHighcharts config={this.pressure} ref="pressure"></ReactHighcharts>
									<div className="para-value">
										<div id="pressure_data">{this.props.station_data.latest_param_value.press + ' ' + this.props.station_data.param_units[this.pressure_index]}</div>
										<div className="bold">Pressure</div>
									</div>
								</Col>;
							}
						})()}
					</Row>;
				}
			})()}
		</div>;
	}
}