import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Switch, Modal, Drawer, Card, notification } from 'antd';
// import Head from './imports/Head';
// import Side from './imports/Side';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import _ from 'lodash';
// import Link from 'react-router/Link';
import io from 'socket.io-client';
import moment from 'moment-timezone';
import { Link } from 'react-router-dom';
import { findDOMNode } from 'react-dom';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const confirm = Modal.confirm;

export default class DeviceConfigurationPage extends React.Component {
	/**
	 * This is the Constructor for Dashboard page used to set the default task while page loads.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		this.state = {
			is_admin: document.getElementById('city_id') ? (document.getElementById('city_id').value == 5 ? true : false) : false,
			device_id: props.device_id ? props.device_id : 1,
			data_loading: 1,
			status: null,
			device_qr_code: null,
			data_sampling_interval: null,
			data_transmission_interval: null,
			wifi_pass: null,
			wifi_ssid: null,
			modem_type: 0,
			fs_ip: null,
			fs_port: null,
			ss_ip: null,
			ss_port: null,
			device_ip: null,
			subnet_mask: null,
			default_gateway: null,
			dns_server: null,
			enable_save_transmission: true,
			enable_save_sampling: true,
			allow_configurations_success_msg: false
		};
	}

	/**
	 * This function saves the user edited configurations.
	 */
	saveDeviceConfiguration() {
		this.socket.emit('save_device_configurations', JSON.stringify({
			device_qr_code: this.state.device_qr_code,
			status: this.state.status,
			device_id: this.state.device_id,
			configurations: {
				data_transmission_interval: this.state.data_transmission_interval,
				data_sampling_interval: this.state.data_sampling_interval,
				modem_type: this.state.modem_type,
				wifi_ssid: this.state.wifi_ssid,
				wifi_pass: this.state.wifi_pass,
				fs_ip: this.state.fs_ip,
				fs_port: this.state.fs_port,
				ss_ip: this.state.ss_ip,
				ss_port: this.state.ss_port,
				device_ip: this.state.device_ip,
				subnet_mask: this.state.subnet_mask,
				default_gateway: this.state.default_gateway,
				dns_server: this.state.dns_server
			}
		}));
	}

	/**
	 * This funtion used for validating the fields (number or not).
	 * @param  {number} target value of the field.
	 * @param  {string} key    name of the field from api side is used as key.
	 */
	dataValidationCheckConfig(target,key) {
		switch(key) {
			case 'data_transmission_interval':
				this.setState({data_transmission_interval: target.value});
				if (target.value >= 0 && !isNaN(target.value)) {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12';
					this.setState({enable_save_transmission: true});
				} else {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 error';
					this.setState({enable_save_transmission: false});
				}
				break;
			case 'data_sampling_interval':
				this.setState({data_sampling_interval: target.value});
				if (target.value >= 0 && !isNaN(target.value)) {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12';
					this.setState({enable_save_sampling: true});
				} else {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 error';
					this.setState({enable_save_sampling: false});
				}
				break;
		}
		// console.log('Target', target);
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
			document.title = 'Device Configuration - Chennai Smart City';

			/**
			 * Socket is used to sync the data between client and server in real time.
			 */
			this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
				query: {
					s_token: this.readCookie('PHPSESSID')
					// s_token: 'qd3gdp5gnqgi6svqj63c3ro1p6'
				}
			});
			this.socket.on('connect', () => {
				this.socket.emit('connect_dashboard_to_socket');
				console.log('connected to socket');
			});

			this.socket.on('dashboard_successfully_connected', () => {
				this.socket.emit('get_device_configurations', JSON.stringify({
					device_id: this.state.device_id
				}));
			});
			let device_configuration = {
			  "configurations": {},
			  "device_id": "53",
			  "device_qr_code": "",
			  "status": "offline"
			};

			this.socket.on('update_device_configurations', (payload) => {
				let device_configuration = JSON.parse(payload);
				console.log('update_device_configurations', device_configuration);
				if (this.state.device_id == device_configuration.device_id) {
					let saved_configurations = {
						data_sampling_interval: device_configuration.configurations ? device_configuration.configurations.data_sampling_interval : this.state.data_sampling_interval,
						data_transmission_interval: device_configuration.configurations ? device_configuration.configurations.data_transmission_interval : this.state.data_transmission_interval,
						modem_type: device_configuration.configurations ? device_configuration.configurations.modem_type : this.state.modem_type,
						wifi_pass: device_configuration.configurations ? device_configuration.configurations.wifi_pass : this.state.wifi_pass,
						wifi_ssid: device_configuration.configurations ? device_configuration.configurations.wifi_ssid : this.state.wifi_ssid,
						fs_ip: device_configuration.configurations ? device_configuration.configurations.fs_ip : this.state.fs_ip,
						fs_port: device_configuration.configurations ? device_configuration.configurations.fs_port : this.state.fs_port,
						ss_ip: device_configuration.configurations ? device_configuration.configurations.ss_ip : this.state.ss_ip,
						ss_port: device_configuration.configurations ? device_configuration.configurations.ss_port : this.state.ss_port,
						device_ip: device_configuration.configurations ? device_configuration.configurations.device_ip : this.state.device_ip,
						subnet_mask: device_configuration.configurations ? device_configuration.configurations.subnet_mask : this.state.subnet_mask,
						default_gateway: device_configuration.configurations ? device_configuration.configurations.default_gateway : this.state.default_gateway,
						dns_server: device_configuration.configurations ? device_configuration.configurations.dns_server : this.state.dns_server
					};
					this.setState({
						device_qr_code: device_configuration.device_qr_code ? device_configuration.device_qr_code : this.state.device_qr_code,
						status: device_configuration.status ? device_configuration.status : this.state.status,
						device_id: device_configuration.device_id,
						data_sampling_interval: device_configuration.configurations ? device_configuration.configurations.data_sampling_interval : this.state.data_sampling_interval,
						data_transmission_interval: device_configuration.configurations ? device_configuration.configurations.data_transmission_interval : this.state.data_transmission_interval,
						modem_type: device_configuration.configurations ? device_configuration.configurations.modem_type : this.state.modem_type,
						wifi_pass: device_configuration.configurations ? device_configuration.configurations.wifi_pass : this.state.wifi_pass,
						wifi_ssid: device_configuration.configurations ? device_configuration.configurations.wifi_ssid : this.state.wifi_ssid,
						fs_ip: device_configuration.configurations ? device_configuration.configurations.fs_ip : this.state.fs_ip,
						fs_port: device_configuration.configurations ? device_configuration.configurations.fs_port : this.state.fs_port,
						ss_ip: device_configuration.configurations ? device_configuration.configurations.ss_ip : this.state.ss_ip,
						ss_port: device_configuration.configurations ? device_configuration.configurations.ss_port : this.state.ss_port,
						device_ip: device_configuration.configurations ? device_configuration.configurations.device_ip : this.state.device_ip,
						subnet_mask: device_configuration.configurations ? device_configuration.configurations.subnet_mask : this.state.subnet_mask,
						default_gateway: device_configuration.configurations ? device_configuration.configurations.default_gateway : this.state.default_gateway,
						dns_server: device_configuration.configurations ? device_configuration.configurations.dns_server : this.state.dns_server,
						modem_types: {
							1: 'GPRS',
							2: 'WIFI',
							3: 'Ethernet'
						},
						data_loading: null,
						saved_configurations: saved_configurations
					});
					if (this.state.allow_configurations_success_msg) {
						this.openNotification('success', 'Configurations saved successfully!');
					} else {
						this.setState({
							allow_configurations_success_msg: true
						});
					}
				}
			});

			this.socket.on('update_location_details_in_dashboard', (payload) => {
				// console.log('update_location_details_in_dashboard', JSON.parse(payload));
				if (JSON.parse(payload).device_id == this.state.device_id) {
					this.setState(JSON.parse(payload));
				}
			});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {

		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	};

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 * @param  {object} prevProps This is the Previous saved props.
	 * @param  {object} prevState This is the Previous saved state.
	 */
	componentDidUpdate(prevProps, prevState) {
		// console.log(this.state.saved_configurations);
		if (this.state.saved_configurations) {
			if (
				((this.state.saved_configurations.data_sampling_interval == this.state.data_sampling_interval) && 
				(this.state.saved_configurations.data_transmission_interval == this.state.data_transmission_interval) && (this.state.saved_configurations.modem_type == this.state.modem_type) &&
				(this.state.saved_configurations.wifi_ssid == this.state.wifi_ssid) && 
				(this.state.saved_configurations.wifi_pass == this.state.wifi_pass) && 
				(this.state.saved_configurations.fs_ip == this.state.fs_ip) && 
				(this.state.saved_configurations.fs_port == this.state.fs_port) &&
				(this.state.saved_configurations.ss_ip == this.state.ss_ip) && 
				(this.state.saved_configurations.ss_port == this.state.ss_port) &&
				(this.state.saved_configurations.device_ip == this.state.device_ip) &&
				(this.state.saved_configurations.subnet_mask == this.state.subnet_mask) &&
				(this.state.saved_configurations.default_gateway == this.state.default_gateway) &&
				(this.state.saved_configurations.dns_server == this.state.dns_server)) ||
				(!this.state.enable_save_transmission) ||
				(!this.state.enable_save_sampling)
			) {
				findDOMNode(this.refs.saveConfig).setAttribute("disabled", true);
			} else {
				findDOMNode(this.refs.saveConfig).removeAttribute("disabled");
			}
		}
		// ReactTooltip.rebuild();
	}

	render () {
		if(this.state.data_loading) {
			return<div className="text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
		} else {
			return <div>
				<Form layout="vertical" hideRequiredMark>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Modem Type">
								<Select showSearch placeholder="Please select operator" value={this.state.modem_type} onChange={(e) => this.setState({modem_type: e})}>
									<Option value={0} selected={((!this.state.modem_type || this.state.modem_type == 0) ? true : false)} disabled>Select Operator</Option>
									{(() => {
										if (this.state.modem_types) {
											let options = Object.keys(this.state.modem_types).map((index) => {
												return <Option value={index}>{this.state.modem_types[index]}</Option>;
											});
											return options;
										}
									})()}
								</Select>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Data Transmission Interval">
								<Input placeholder="Please enter data transmission interval" value={this.state.data_transmission_interval} onChange={(e) => this.dataValidationCheckConfig(e.target, 'data_transmission_interval')}/>
							</Form.Item>
						</Col>
						<Col span={12} className="wid-100">
							<Form.Item label="Data Sampling Interval">
								<Input placeholder="Please enter data sampling interval" value={this.state.data_sampling_interval} onChange={(e) => this.dataValidationCheckConfig(e.target, 'data_sampling_interval')}/>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="WiFi SSID">
								<Input placeholder="Please enter wifi ssid" value={this.state.wifi_ssid} onChange={(e) =>this.setState({wifi_ssid: e.target.value})}/>
							</Form.Item>
						</Col>
						<Col span={12} className="wid-100">
							<Form.Item label="WiFi Password">
								<Input placeholder="Please enter wifi password" value={this.state.wifi_pass} onChange={(e) => this.setState({wifi_pass: e.target.value})}/>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="First Server IP">
								<Input placeholder="Please enter first server ip"  value={this.state.fs_ip} onChange={(e) =>this.setState({fs_ip: e.target.value})}/>
							</Form.Item>
						</Col>
						<Col span={12} className="wid-100">
							<Form.Item label="First Server Port">
								<Input placeholder="Please enter first server port" value={this.state.fs_port} onChange={(e) => this.setState({fs_port: e.target.value})}/>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Secondary Server IP">
								<Input placeholder="Please enter secondary server ip" value={this.state.ss_ip} onChange={(e) =>this.setState({ss_ip: e.target.value})}/>
							</Form.Item>
						</Col>
						<Col span={12} className="wid-100">
							<Form.Item label="Secondary Server Port">
								<Input placeholder="Please enter secondary server port" value={this.state.ss_port} onChange={(e) => this.setState({ss_port: e.target.value})}/>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Device IP">
								<Input placeholder="Please enter device ip" value={this.state.device_ip} onChange={(e) => this.setState({device_ip: e.target.value})}/>
							</Form.Item>
						</Col>
						<Col span={12} className="wid-100">
							<Form.Item label="Subnet Mask">
								<Input placeholder="Please enter subnet mask" value={this.state.subnet_mask} onChange={(e) => this.setState({subnet_mask: e.target.value})}/>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={50}>
						<Col span={12} className="wid-100">
							<Form.Item label="Default Gateway">
								<Input placeholder="Please enter default gateway" value={this.state.default_gateway} onChange={(e) => this.setState({default_gateway: e.target.value})}/>
							</Form.Item>
						</Col>
						<Col span={12} className="wid-100">
							<Form.Item label="DNS Server">
								<Input placeholder="Please enter dns server" value={this.state.dns_server} onChange={(e) => this.setState({dns_server: e.target.value})}/>
							</Form.Item>
						</Col>
					</Row>
				</Form>
				<div className="butn-contain">
					{(() => {
						if (!this.state.data_loading) {
							return <Button type="primary" ref="saveConfig" onClick={() => this.saveDeviceConfiguration()}>Save</Button>;
						} else {
							return <Button type="primary" disabled="true" ref="saveConfig" onClick={() => this.saveDeviceConfiguration()}>Save</Button>;
						}
					})()}
				</div>
			</div>;
		}
	}
}