import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Modal, Drawer, Card, notification } from 'antd';
import Head from './imports/Head.js';
import Side from './imports/Side.js';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import Loading from './imports/Loading';
import moment from 'moment-timezone';
import _ from 'lodash';
import Tour from 'reactour';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];
const confirm = Modal.confirm;

const steps = [
	{
		selector: '[data-tut="compare_menu"]',
		content: 'This section lets you compare your city and stations among others.',
		position: 'right',
	},
	{
		selector: '[data-tut="add_new"]',
		content: ' Click to add a new city to the list.',
		position: 'left',
	},
	{
		selector: '[data-tut="city_compare"]',
		content: 'Here you can see all your added cities for comparison.',
		position: 'left',
	},
	{
		selector: '[data-tut="close_city"]',
		content: 'Click the cross icon to remove a city from the list.',
		position: 'right',
	},
	{
		selector: '[data-tut="city_select"]',
		content: ' Select a city from the dropdown menu to compare stations deployed in the city.',
		position: 'bottom',
	},
	{
		selector: '[data-tut="station_select"]',
		content: 'Select a station and click “add” button from the drop-down list to add the station for comparison.',
		position: 'bottom',
	},
	{
		selector: '[data-tut="station_compare"]',
		content: 'Here you can see all your added stations for comparison.',
		position: 'bottom',
	},
	{
		selector: '[data-tut="close_station"]',
		content: 'Click the cross icon to remove a station from the list.',
		position: 'right',
	},
]

const config = {
	chart: {
		type: 'column',
		height: 80,
		width: 200,
		backgroundColor: 'transparent',
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: 'Past 24 hr AQI Trend'
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

const config2 = {
	chart: {
		type: 'column',
		height: 55,
		width: 180,
		backgroundColor: 'transparent',
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: ' '
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

function showDelete() {
	confirm({
		title: 'Do you want to delete ?',
		content: '',
		onOk() {},
		onCancel() {},
	});
}

const action = (
	<Menu>
		<Menu.Item key="action-1">Edit</Menu.Item>
		<Menu.Item key="action-2" onClick={showDelete}>Delete</Menu.Item>
	</Menu>
);

const city = [];
for (let i = 1; i < 12; i++) {
	city.push(<Option key={'City - ' + i}>{'City - ' + i}</Option>);
}

const station = [];
for (let i = 1; i < 12; i++) {
	station.push(<Option key={'Sation - ' + i}>{'Station - ' + i}</Option>);
}

class Compare extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleModal: false,
			show_search_list: null,
			search_term: '',
			station_id: 1,
			stations: null,
			all_stations: null,
			index: null,
			items: [],
			isTourOpen: false
		};
		this.fetch_compare_station_data();
		this.getCPCBAllCity();
	}

	/**
	 * Predefined function of ReactJS. Called after the component is mounted.
	 * @return {void} 
	 */

	componentDidMount() {
		document.title = 'Compare - Chennai Smart City';
		// console.log('in mount');
		
		// this.retriveData();
	}

	/**
	 * This function sets the delete confirmation popup.
	 * @param  {Object} role Role data.
	 */	
	showDeleteConfirmStation(station) {
		
		let that = this;
		// that.props.history.push('/settings/users/' + station.id + '/delete/' + that.props.history.location.search);
		console.log('showDeleteConfirmStation', station);
		confirm({
			title: 'Do you want to delete ?',
			content: station.name,
			onOk() {
				that.remove_station(station.id);
			},
			onCancel() {
				// that.closeAddEditModal();
				// that.props.history.push('/settings/users/' + that.props.history.location.search);
			}
		});
	}

		/**
	 * This function sets the delete confirmation popup.
	 * @param  {Object} role Role data.
	 */	
	showDeleteConfirmCPCB(station) {
		
		let that = this;
		// that.props.history.push('/settings/users/' + station.id + '/delete/' + that.props.history.location.search);
		console.log('showDeleteConfirmStation', station);
		confirm({
			title: 'Do you want to delete ?',
			content: station.name,
			onOk() {
				that.remove_cpcb(station.id);
			},
			onCancel() {
				// that.closeAddEditModal();
				// that.props.history.push('/settings/users/' + that.props.history.location.search);
			}
		});
	}

	/**
	 * This function used to fetch compared station details to user.
	 */
	fetch_compare_station_data() {
		var that = this;
		that.setState({
			stations: null,
			station_loading: true
		});
		fetch('https://api.aurassure.com/smart_city_platform/' + 'get_all_data_of_compared_stations_of_user.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Stations', json);
			if (json.status === 'success') {
				let stations = [];
				let station_array = [];
				json.stations.map((station, i) => {
					stations.push(station);
					station_array.push(station.id);
				});
				that.setState({
					stations: stations,
					station_array: station_array,
					station_loading: false
				});
				// that.creatingGraphConfig();
				that.retrive_data();
				that.getAllCity();
			} else {
				that.openNotification('error',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			that.openNotification('error','Unable to load data!');
		});
	}

	/**
	 * This function is used to configure graph for compared stations.
	 */

	openTour() {
    	this.setState({ 
    		isTourOpen: true,
    		tour_done: false 
    	});
  	}

	closeTour() {
    	this.setState({
    		isTourOpen: false,
    		tour_done: true
    	});
  	}

	creatingGraphConfig() {
		var that = this;
		if (that.state.stations) {
			let current_time = moment().unix();
			let previous_time = current_time - 3600;
			let graph_data = [];
			let categories = [];
			let config = [];
			let items = [];
			let plot_graph = [];
			let param_data = [];

			that.state.stations.map((station, ind) => {
				graph_data = [];
				categories = [];
				plot_graph[ind] = false;
				station.hourly_aqis.map((hourly_aqi, i) => {
					let color = window.color_code.color_1;
					if(hourly_aqi.aqi >= 401) {
						color = window.color_code.color_6;
					} else if(hourly_aqi.aqi >= 301) {
						color = window.color_code.color_5;
					} else if(hourly_aqi.aqi >= 201) {
						color = window.color_code.color_4;
					} else if(hourly_aqi.aqi >= 101) {
						color = window.color_code.color_3;
					} else if(hourly_aqi.aqi >= 51) {
						color = window.color_code.color_2;
					}
					graph_data.push({
						y: parseInt(hourly_aqi.aqi),
						param: ((hourly_aqi.param == "" || hourly_aqi.param == null) ? '-' : (hourly_aqi.param.toUpperCase())),
						color: color
					});

					if(hourly_aqi.aqi) {plot_graph[ind] = true;}

					categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));
				});
				categories.reverse();
				graph_data.reverse();

				let param_plot_graph = [];
				let param_graph_data = [];
				let param_categories = [];
				let param_aqis_config = [];
				Object.keys(station.hourly_param_aqis).map((param, i) => {
					param_plot_graph[param] = false;
					param_categories = [];
					param_graph_data = [];
					station.hourly_param_aqis[param].map((aqi, a) => {
						let color = window.color_code.color_1;
						if(aqi >= 401) {
							color = window.color_code.color_6;
						} else if(aqi >= 301) {
							color = window.color_code.color_5;
						} else if(aqi >= 201) {
							color = window.color_code.color_4;
						} else if(aqi >= 101) {
							color = window.color_code.color_3;
						} else if(aqi >= 51) {
							color = window.color_code.color_2;
						}
						param_graph_data.push({
							y: parseInt(aqi),
							color: color
						});

						if(aqi) {param_plot_graph[param] = true;}

						param_categories.push(moment.unix(previous_time - 3600 * a).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * a).tz('Asia/Kolkata').format('HH'));
					});
					param_categories.reverse();
					param_graph_data.reverse();
					param_aqis_config[param] = {
						chart: {
							type: 'column',
							height: 55,
							width: 180,
							backgroundColor: 'transparent',
						},
						plotOptions: {
							series: {
								pointPadding: 0,
								groupPadding: 0
							}
						},
						title: {
							text: ''
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							title: {
								enabled: false,
								text: '',
								style: {
									fontWeight: 'normal'
								}
							},
							type: '',
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							categories: param_categories

						},
						yAxis: {
							title: {
								text: ''
							},
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							gridLineColor: 'transparent'

						},
						legend: {
							enabled: false
						},
						tooltip: {
							pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b><br/>'
						},
						series: [{
							data: param_graph_data
						}]
					};
				});

				items.push({
					id: station.id,
					name: station.name,
					time: station.last_data_update_time,
					city_id: station.city_id,
					aqi: station.aqi,
					aqi_range: station.aqi_range,
					aqi_status: station.aqi_status,
					connection_status: station.connection_status,
					responsible_param: station.hourly_aqis[0].param,
					source: 'aurassure',
					plot_graph: plot_graph[ind],
					type: station.type,
					latest_param_value: station.latest_param_value,
					last_data_receive_time: station.last_data_receive_time,
					data_receive_connection_status: station.data_receive_connection_status,
					params: station.params,
					parameters_value: {
						'so2': 1.2,
						'no': 11111,
						'PM': 2.1,
						'pm<sub>2.5</sub>': 1.1,
						'spm': 1.2
					},
					parameters_unit: {
						'so2': 'Mg/nm<sub>3</sub>',
						'no': 'ppm',
						'PM': 'Mg/nm<sub>3</sub>',
						'pm<sub>2.5</sub>': 'm<sup>3</sup>',
						'spm': 'Mg/nm<sub>3</sub>'
					},
					hourly_aqi_config: {
						chart: {
							type: 'column',
							height: 80,
							width: 200,
							backgroundColor: 'transparent',
						},
						plotOptions: {
							series: {
								pointPadding: 0,
								groupPadding: 0
							}
						},
						title: {
							text: ''
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							title: {
								enabled: true,
								text: 'Past 24 hr Trend',
								style: {
									fontWeight: 'normal'
								}
							},
							type: '',
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							categories: categories

						},
						yAxis: {
							title: {
								text: ''
							},
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							gridLineColor: 'transparent'

						},
						legend: {
							enabled: false
						},
						tooltip: {
							pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
						},
						series: [{
							data: graph_data
						}]
					},
					param_aqis_config: param_aqis_config,
					param_plot_graph: param_plot_graph,
					hourly_param_aqis: station.hourly_param_aqis,
					remove_station: function() {that.remove_station(station.id);}
				});
			});
			that.setState({items: items});
			let index = [];
			that.state.items.map((item, i) => {
				index.push(item.id);
			});
			that.setState({index: index});
		}
	}

	/**
	 * This function is used to fetch all stations data.
	 */
	retrive_data() {
		var that = this;
		that.setState({
			station_loading: true
		});
		fetch('https://api.aurassure.com/smart_city_platform/' + 'get_all_stations.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				let stations_to_compare = json.stations.slice(0);
				let station_option = [];
				if (that.state.stations && Object.keys(that.state.stations).length) {
					that.state.stations.map((c_station) => {
						if (stations_to_compare && stations_to_compare.length) {
							stations_to_compare.map((station, j) => {
								if (c_station.id === station.id) {
									console.info('Removed ['+j+']:',station.id);
									if (!c_station.city_id) {
										c_station.city_id = station.city_id;
									} else {
										c_station.city_id = station.city_id;
									}
									// stations_to_compare.splice(stations_to_compare[j], 1);
									if (_.findIndex(station_option, { 'id': c_station.id}) == -1) {
										station_option.push({
											id: station.id,
											city_id: station.city_id,
											name: station.name,
											disabled: true
										});
									}
									delete stations_to_compare[j];
								}/* else {
									if (_.findIndex(station_option, { 'id': c_station.id}) == -1) {
										station_option.push({
											id: station.id,
											city_id: station.city_id,
											name: station.name,
											disabled: false
										});
									}
								}*/
							});
						}
					});
				}
				let temp_all_stations = json.stations.slice(0);
				_.remove(temp_all_stations, (st) => {
					if (st) {
						console.log('st');
						return _.findIndex(station_option, { id: st.id }) !== -1 ? true : false;
					}
				});
				let station_option2 = [];
				if (temp_all_stations && temp_all_stations.length) {
					temp_all_stations.map((station) => {
						station_option2.push({
							id: station.id,
							city_id: station.city_id,
							name: station.name,
							disabled: false
						});
					});
				}
				let station_option_final = _.concat(station_option, station_option2);
				console.log('abc', temp_all_stations);
				that.setState({
					all_stations: json.stations,
					stations_to_compare: stations_to_compare,
					station_option: station_option_final,
					station_loading: false
				}, () => {
					that.creatingGraphConfig();
					if (!that.state.tour_done) {
						that.openTour();
					}
				});
			} else {
				that.openNotification('error',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			that.openNotification('error','Unable to load data!');
		});
	}

	getCPCBAllCity() {
		var that = this;
		fetch('https://api.aurassure.com/smart_city_platform/' + 'get_all_cpcb_stations.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				let cpcb_stations_name = {};
				if (json.cpcb_stations && json.cpcb_stations.length) {
					json.cpcb_stations.map((city) => {
						if (!cpcb_stations_name[city.id]) {
							cpcb_stations_name[city.id] = city.name;
						} else {
							cpcb_stations_name[city.id] = city.name;
						}
					});
				}
				that.setState({
					cpcb_stations: json.cpcb_stations,
					cpcb_stations_name: cpcb_stations_name
				}, () => that.getCPCBCompareData());
			} else {
				that.openNotification('error',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			that.openNotification('error','Unable to load data!');
		});
	}

	getCPCBCompareData() {
		var that = this;
		that.setState({
			city_loading: true
		});
		fetch('https://api.aurassure.com/smart_city_platform/' + 'get_all_data_of_compared_cpcb_stations_of_user.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				var stations_to_compare = json.cpcb_stations;
				let city_option_cpcb = [];
				if (that.state.cpcb_stations && Object.keys(that.state.cpcb_stations).length) {
					that.state.cpcb_stations.map((c_station) => {
						if (stations_to_compare && stations_to_compare.length) {
							stations_to_compare.map((station, j) => {
								if (c_station.id === station.id) {
									console.log('Removed ['+j+']:',station.id);
									if (!c_station.city_id) {
										c_station.city_id = station.city_id;
									} else {
										c_station.city_id = station.city_id;
									}
									// stations_to_compare.splice(stations_to_compare[j], 1);
									if (_.findIndex(city_option_cpcb, { 'id': c_station.id}) == -1) {
										city_option_cpcb.push({
											id: station.id,
											name: station.name,
											disabled: true
										});
									}
									// delete stations_to_compare[j];
								}/* else {
									if (_.findIndex(city_option_cpcb, { 'id': c_station.id}) == -1) {
										city_option_cpcb.push({
											id: station.id,
											city_id: station.city_id,
											name: station.name,
											disabled: false
										});
									}
								}*/
							});
						}
					});
				}
				let cpcb_array = [];
				if (json.cpcb_stations && json.cpcb_stations.length) {
					json.cpcb_stations.map((cpcb) => {
						cpcb_array.push(cpcb.id);
					});
				}
				let temp_all_stations = that.state.cpcb_stations.slice(0);
				_.remove(temp_all_stations, (st) => {
					if (st) {
						console.log('st');
						return _.findIndex(city_option_cpcb, { id: st.id }) !== -1 ? true : false;
					}
				});
				let city_option_cpcb2 = [];
				if (temp_all_stations && temp_all_stations.length) {
					temp_all_stations.map((station) => {
						city_option_cpcb2.push({
							id: station.id,
							name: station.name,
							disabled: false
						});
					});
				}
				let city_option_cpcb_final = _.concat(city_option_cpcb, city_option_cpcb2);
				console.log('abccc', temp_all_stations);
				that.setState({
					cpcb_array: cpcb_array,
					cpcb_array2: cpcb_array,
					cpcb_stations_cpcb: json.cpcb_stations,
					city_option_cpcb: city_option_cpcb_final,
					city_loading: false
				}, () => that.creatingGraphConfig());
			} else {
				that.openNotification('error',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			that.openNotification('error','Unable to load data!');
		});
	}

	getAllCity() {
		var that = this;
		fetch('https://api.aurassure.com/smart_city_platform/' + 'get_all_cities_for_compare.php', {
			method: 'GET',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				let city_option = [];
				if (json.cities && json.cities.length) {
					json.cities.map((city) => {
						city_option.push(<Option value={city.id} title={city.name} key={city.id}>{city.name}</Option>);
					});
				}
				that.setState({
					cities: json.cities,
					city_option: city_option
				});
			} else {
				that.openNotification('error',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			that.openNotification('error','Unable to load data!');
		});
	}

	/**
	 * This function used for add station to compared list.
	 * @param {number} id This is the station id of selected station for compare.
	 */
	add_station(id) {
		console.log('Add Station:',id);
		if (id) {
			var that = this;
			var station_array = that.state.station_array.slice(0);
			if (station_array.indexOf(id) === -1) {
				// station_array.splice(station_array.indexOf(id), 1);
				station_array.push(parseInt(id));
			}
			console.log('Added Station:',station_array);
			fetch('https://api.aurassure.com/smart_city_platform/' + 'add_station_to_compare_list_of_user.php', {
				credentials: 'include',
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					station_array: station_array.map(Number)
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('Add Response:',json);
				if (json.status === 'success') {
					that.setState({
						station_array: station_array
					});
					that.fetch_compare_station_data();
				} else {
					that.openNotification('error',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.openNotification('error','Unable to load data!');
			});
		} else {
			that.openNotification('error','Invalid Station!');
		}
	}

	add_cpcb_data() {
		if (this.state.cpcb_array2) {
			this.add_cpcb(true);
		}
	}

	/**
	 * This function used for add station to compared list.
	 * @param {number} id This is the station id of selected station for compare.
	 */
	add_cpcb(id) {
		console.log('Add Station:',id);
		if (id) {
			var that = this;
			var cpcb_array = that.state.cpcb_array2.slice(0);
			var cpcb_array3 = [];
			if (cpcb_array && cpcb_array.length) {
				cpcb_array.map((id) => {
					cpcb_array3.push(parseInt(id));
				});
			}
			cpcb_array = cpcb_array3;
			console.log('Added Station:',cpcb_array);
			fetch('https://api.aurassure.com/smart_city_platform/' + 'add_cpcb_station_to_compare_list_of_user.php', {
				credentials: 'include',
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					station_array: cpcb_array
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('Add Response:',json);
				if (json.status === 'success') {
					that.setState({
						cpcb_array: cpcb_array,
						cpcb_array2: cpcb_array,
						visibleModal: false
					});
					that.getCPCBCompareData();
				} else {
					that.openNotification('error',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.openNotification('error','Unable to load data!');
			});
		} else {
			that.openNotification('error','Invalid Station!');
		}
	}

	/**
	 * This function used for remove station from compare list.
	 * @param  {number} id This is the station id of selected station.
	 */
	remove_station(id) {
		console.log('Remove Station:',id);
		if (id) {
			let that = this;
			let station_array = that.state.station_array.slice(0);
			if (station_array.indexOf(id) > -1) {
				station_array.splice(station_array.indexOf(id), 1);
			}
			console.log('Removed Station:',station_array);
			fetch('https://api.aurassure.com/smart_city_platform/' + 'delete_station_from_compare_list_of_user.php', {
				credentials: 'include',
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					station_array: station_array.map(Number)
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('Remove Response:',json);
				if (json.status === 'success') {
					that.setState({
						station_array: station_array
					});
					that.fetch_compare_station_data();
				} else {
					that.openNotification('error',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.openNotification('error','Unable to load data!');
			});
		} else {
			that.openNotification('error','Invalid Station!');
		}
	}

	/**
	 * This function used for remove station from compare list.
	 * @param  {number} id This is the station id of selected station.
	 */
	remove_cpcb(id) {
		console.log('Remove Station:',id);
		if (id) {
			let that = this;
			let cpcb_array = that.state.cpcb_array.slice(0);
			if (cpcb_array.indexOf(id) > -1) {
				cpcb_array.splice(cpcb_array.indexOf(id), 1);
			}
			console.log('Removed Station:',cpcb_array);
			fetch('https://api.aurassure.com/smart_city_platform/' + 'delete_cpcb_station_from_compare_list_of_user.php', {
				credentials: 'include',
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					station_array: cpcb_array
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('Remove Response:',json);
				if (json.status === 'success') {
					that.setState({
						cpcb_array: cpcb_array,
						cpcb_array2: cpcb_array,
					});
					that.getCPCBCompareData();
				} else {
					that.openNotification('error',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.openNotification('error','Unable to load data!');
			});
		} else {
			that.openNotification('error','Invalid Station!');
		}
	}

	/**
	 * This function opens the notification in render.
	 */
	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	};

	showModal () {
		this.setState({
			visibleModal: true,
		});
	}

	handleCancel(e) {
		this.setState({
			visibleModal: false,
			cpcb_array2: this.state.cpcb_array
		});
	}

	handleCityChange(e) {
		console.log('eeeeee', e);
		this.setState({
			city_selected_filter: e
		}, () => this.filterStationOpt());
	}

	filterStationOpt() {
		let that = this;
		let stations_to_compare = [];
		let station_option = [];
		if (that.state.city_selected_filter && that.state.city_selected_filter != null) {
			if (that.state.all_stations && that.state.all_stations.length) {
				that.state.all_stations.map((st) => {
					if (that.state.city_selected_filter == st.city_id) {
						stations_to_compare.push(st);
					}
				});
			}
		} else {
			stations_to_compare = that.state.all_stations.slice(0);
		}
		if (that.state.stations && Object.keys(that.state.stations).length) {
			that.state.stations.map((c_station) => {
				if (stations_to_compare && stations_to_compare.length) {
					stations_to_compare.map((station, j) => {
						if (c_station.id === station.id) {
							console.info('Removed ['+j+']:',station.id);
							if (!c_station.city_id) {
								c_station.city_id = station.city_id;
							} else {
								c_station.city_id = station.city_id;
							}
							// stations_to_compare.splice(stations_to_compare[j], 1);
							if (_.findIndex(station_option, { 'id': c_station.id}) == -1) {
								station_option.push({
									id: station.id,
									city_id: station.city_id,
									name: station.name,
									disabled: true
								});
							}
							delete stations_to_compare[j];
						}/* else {
							if (_.findIndex(station_option, { 'id': c_station.id}) == -1) {
								station_option.push({
									id: station.id,
									city_id: station.city_id,
									name: station.name,
									disabled: false
								});
							}
						}*/
					});
				}
			});
		}
		let temp_all_stations = stations_to_compare;
		_.remove(temp_all_stations, (st) => {
			if (st) {
				console.log('st');
				return _.findIndex(station_option, { id: st.id }) !== -1 ? true : false;
			}
		});
		let station_option2 = [];
		if (temp_all_stations && temp_all_stations.length) {
			temp_all_stations.map((station) => {
				station_option2.push({
					id: station.id,
					city_id: station.city_id,
					name: station.name,
					disabled: false
				});
			});
		}
		let station_option_final = _.concat(station_option, station_option2);
		console.log('abc', temp_all_stations);
		that.setState({
			station_option: station_option_final
		});
	}

	handleCPCB(e) {
		console.log('eeeeee', e);
		this.setState({
			cpcb_array2: e
		});
	}

	render () {

		return (
			<div id="compare" className="mar-top-70">
				<Tour
					steps={steps}
					isOpen={this.state.isTourOpen}
					startAt={0}
					onRequestClose={()=> this.closeTour()}
					disableKeyboardNavigation={['esc']}
					lastStepNextButton={<Button type="primary" onClick={()=> this.closeTour()}>Done !</Button>}
					onAfterOpen={target => (document.body.style.overflowY = 'hidden')}
					onBeforeClose={target => (document.body.style.overflowY = 'auto')}
				/>
				<Side active_link="compare" />
				<Head openTour={() => this.openTour()}/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="page-header-txt">
							City Comparision
						</div>
						<div className="btn-contain">
							<div data-tut="add_new" style={{display: "inline-block"}}>
								<Button className="add-btn" type="primary" icon="plus" onClick={() => this.showModal()}>Add New</Button>
							</div>
						</div>
						<div className="card-container" data-tut="city_compare">
							{(() => {
								if (!this.state.cpcb_stations || !this.state.cpcb_stations_cpcb || this.state.city_loading) {
									return <Loading is_inline={true} />;
								} else if (this.state.cpcb_stations_cpcb && this.state.cpcb_stations_cpcb.length == 0) {
									return(
										<div className="hourly-aqi-na-error text-center compare-search">No city selected to be compared. Please select a city!</div>
									);
								} else {
									return <div className="card-contain">
										{(() => {
											if (this.state.cpcb_stations_cpcb && this.state.cpcb_stations_cpcb.length) {
												let c = 0;
												let cpcb_city = this.state.cpcb_stations_cpcb.map((cpcb) => {
													c++;
													return <Card extra={<Icon type="close" data-tut="close_city" className="close-btn" theme="outlined" onClick={() => this.showDeleteConfirmCPCB(cpcb)} />} className="card" title={cpcb.name}>
														<Tooltip title="Last data received Time">
															<div className="city-time">
																{/*<span className="dot"></span>*/}
																{ (cpcb.last_data_update_time ? moment.unix(cpcb.last_data_update_time).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY') : 'NA')}
															</div>
														</Tooltip>
														<Row>
															<span className="aqi">AQI</span>
															<span className="aqi-value-txt">{cpcb.aqi}</span>
															<span className={'aqi-'+cpcb.aqi_range}>{cpcb.aqi_status}</span>
														</Row>
													</Card>;
												}).filter(Boolean);
												if (c > 0) {
													return cpcb_city;
												} else {
													return <div className="hourly-aqi-na-error text-center compare-search">No city selected!</div>;
												}
											}
										})()}
									</div>
								}
							})()}
							
						</div>

						<Modal
								title="Add city to compare"
								visible={this.state.visibleModal}
								onOk={() => this.add_cpcb_data()}
								onCancel={() => this.handleCancel()}
							>
							<Form layout="vertical" hideRequiredMark>
								<Row>
									<Col span={24} className="wid-100">
										<Form.Item label="Select City">
											<Select showSearch mode="multiple" value={this.state.cpcb_array2} onChange={(e) => this.handleCPCB(e)} placeholder="Please select city">
												{(() => {
													if (this.state.city_option_cpcb && this.state.city_option_cpcb.length) {
														return this.state.city_option_cpcb.map((cpcb) => {
															return <Option disabled={cpcb.disabled} value={cpcb.id} title={cpcb.name} key={cpcb.id}>{cpcb.name}</Option>
														});
													}
												})()}
											</Select>
										</Form.Item>
									</Col>
								</Row>
							</Form>
						</Modal>

					</Content>

					<Content className="contains mar-top-50 compare-container">
						<div className="page-header-txt">
							Station Comparision
						</div>
						<div className="select-menu">
							<div data-tut="city_select">
								<Select className="select-icon" allowClear onDeselect={() => this.setState({city_selected_filter: false})} optionFilterProp="title" onChange={(e) => this.handleCityChange(e)} value={this.state.city_selected_filter} showSearch placeholder="Please select city">
								{this.state.city_option}
								</Select>
							</div>
						</div>
						<div className="select-menu pad-left">
							<div data-tut="station_select">
								<Select className="select-icon" optionFilterProp="title" showSearch value={null} placeholder="Please select station">
									{(() => {
										if (this.state.station_option && this.state.station_option.length) {
											return this.state.station_option.map((st) => {
												return <Option value={st.id} title={st.name} key={st.id}><div className="station-drop-contain"><span className="station-name hellip">{st.name}</span><Button key="submit" type="primary" className="station-add-btn" disabled={st.disabled} loading={this.props.getting_report} onClick={() => this.add_station(st.id)}>Add</Button></div></Option>
											});
										}
									})()}
								</Select>
							</div>
						</div>
						<div className="card-container" data-tut="station_compare">
							{(() => {
								if (!this.state.stations || !this.state.all_stations || this.state.station_loading) {
										return <Loading is_inline={true} />;
									} else if (this.state.all_stations && this.state.all_stations.length == 0) {
										return(
											<div className="hourly-aqi-na-error text-center compare-search">Sorry, No station was found in your city!</div>
										);
									} else {
										if (this.state.items && this.state.items.length) {
											return <div className="compare-contain">
												{(() => {
													let c= 0;
													let station_display = this.state.items.map((station) => {
														if (this.state.city_selected_filter == station.city_id || !this.state.city_selected_filter) {
															c++;
															return <Card extra={<Icon type="close" data-tut="close_station" className="close-btn" theme="outlined" onClick={() => this.showDeleteConfirmStation(station)} />} className="card" title={station.name}>
																<div className="time">
																	<Tooltip title="Last data received Time">
																		<span className={'dot' + (station.connection_status == 'online' ? ' success' : ' danger')}></span>
																		{(parseInt(station.time)) ? moment.unix(station.time).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY') : 'NA'}
																	</Tooltip>
																</div>
																<Row gutter="6" type="flex" justify="space-around" align="middle">
																	<Col span="4">
																		<img src="https://dev.datoms.io/aurassure-webapp/smart_city_platform/images/aurassure_logo.svg"/>
																	</Col>
																	<Col span="4" className="center">
																		<div className="value-txt">{(parseInt(station.time)) ? station.aqi : 'NA'}</div>
																		<div>AQI</div>
																	</Col>
																	<Col span="12">
																		<div className={'aqi-'+station.aqi_range}>{(parseInt(station.time)) ? station.aqi_status : ''}</div>
																		{(() => {
																			if(parseInt(station.time) && station.params && Object.keys(station.params).length) {
																				let aqi_name = station.params.map((param) => {
																					if (param.key == station.responsible_param) {
																						return <div className="polutant">Pollutant - <span className="polutant-name" dangerouslySetInnerHTML={{__html: param.name}}></span></div>;
																					}
																				});
																				return aqi_name;
																			}
																		})()}
																		
																	</Col>
																</Row>
																{(() => {
																	if (station.plot_graph) {
																		let hourly_aqis = [];
																		station.hourly_aqi_config.series[0].data.map((hourly_aqi) => {
																			if(hourly_aqi && !isNaN(hourly_aqi.y)) {
																				hourly_aqis.push(parseInt(hourly_aqi.y));
																			}
																		});

																		return <Row className="hr24-graph">
																			<Col span={17}>
																				<ReactHighcharts config={station.hourly_aqi_config} />
																			</Col>
																			<Col span={6}>
																				<span className="value">
																					<div className="graph-value">{Math.min.apply(null, hourly_aqis)}</div>
																					<div>Min</div>
																				</span>
																				<span className="value">
																					<div className="graph-value">{Math.max.apply(null, hourly_aqis)}</div>
																					<div>Max</div>
																				</span>
																			</Col>
																		</Row>;
																	} else {
																		return <Row className="hr24-graph">
																			<div className="danger center">No data received in last 24 hours.</div>
																		</Row>;
																	}
																})()}
																{(() => {
																	if (station.plot_graph) {
																		return <div className="aqi-scale"><img src="https://dev.datoms.io/aurassure-webapp/smart_city_platform/images/aqi_scale.svg" /></div>;
																	}
																})()}
																<div className="head">Last 24 Hrs Data</div>
																{/*<div className="24hr-data graphs">*/}
																	{(() => {
																		let graphs_24hrs = [];
																		Object.keys(station.param_plot_graph).map((param, i) => {
																			if (station.param_plot_graph[param] && station.param_aqis_config[param]) {
																				graphs_24hrs.push(<Row className="hr24-graph" align="bottom" key={i}>
																					<div className="graph-name">
																						{(() => {
																							if(parseInt(station.time) && station.params && Object.keys(station.params).length) {
																								let aqi_name = station.params.map((parameter) => {
																									if (parameter.key == param) {
																										return <span className="pollutant-name" dangerouslySetInnerHTML={{__html: parameter.name}}></span>;
																									}
																								});
																								return aqi_name;
																							}
																						})()}
																						{(() => {
																							let color = 1;
																							if(station.hourly_param_aqis[param].filter(function(val){if(val)return val})[0] >= 401) {
																								color = 6;
																							} else if(station.hourly_param_aqis[param].filter(function(val){if(val)return val})[0] >= 301) {
																								color = 5;
																							} else if(station.hourly_param_aqis[param].filter(function(val){if(val)return val})[0] >= 201) {
																								color = 4;
																							} else if(station.hourly_param_aqis[param].filter(function(val){if(val)return val})[0] >= 101) {
																								color = 3;
																							} else if(station.hourly_param_aqis[param].filter(function(val){if(val)return val})[0] >= 51) {
																								color = 2;
																							}
																							return <span className="graph-aqi"><span className={'graph-aqi-value aqi-' + color}>{station.hourly_param_aqis[param].filter(function(val){if(val)return val})[0]}</span>AQI</span>;
																						})()}
																						
																					</div>
																					<Col span={17}>
																						<ReactHighcharts config={station.param_aqis_config[param]} />
																					</Col>
																					<Col span={5}>
																						<span className="value">
																							<div className="graph-value">{Math.min.apply(null, station.hourly_param_aqis[param].filter((value) => value ? true : false))}</div>
																							<div>Min</div>
																						</span>
																						<span className="value">
																							<div className="graph-value">{Math.max.apply(null, station.hourly_param_aqis[param].filter((value) => value ? true : false))}</div>
																							<div>Max</div>
																						</span>
																					</Col>
																				</Row>);

																				
																			}
																		});
																		if (graphs_24hrs.length) {
																			return(<div className="24hr-data graphs">{graphs_24hrs}</div>);
																		} else {
																			return(<div className="24hr-data graphs"><div className="danger center no-data-txt">No data received in last 24 hours.</div></div>);
																		}
																	})()}
																	{/*<Row className="hr24-graph" align="bottom">
																			<div className="graph-name">
																				<span className="pollutant-name">PM <sub>2.5</sub></span>
																				<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
																			</div>
																			<Col span={17}>
																				<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
																			</Col>
																			<Col span={5}>
																				<span className="value">
																					<div className="graph-value">67</div>
																					<div>Min</div>
																				</span>
																				<span className="value">
																					<div className="graph-value">99</div>
																					<div>Max</div>
																				</span>
																			</Col>
																		</Row>
																		<Row className="hr24-graph" align="bottom">
																			<div className="graph-name">
																				<span className="pollutant-name">CO <sub></sub></span>
																				<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
																			</div>
																			<Col span={17}>
																				<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
																			</Col>
																			<Col span={5}>
																				<span className="value">
																					<div className="graph-value">67</div>
																					<div>Min</div>
																				</span>
																				<span className="value">
																					<div className="graph-value">99</div>
																					<div>Max</div>
																				</span>
																			</Col>
																		</Row>
																		<Row className="hr24-graph" align="bottom">
																			<div className="graph-name">
																				<span className="pollutant-name">O <sub>3</sub></span>
																				<span className="graph-aqi"><span className="graph-aqi-value success">50</span>AQI</span>
																			</div>
																			<Col span={17}>
																				<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
																			</Col>
																			<Col span={5}>
																				<span className="value">
																					<div className="graph-value">67</div>
																					<div>Min</div>
																				</span>
																				<span className="value">
																					<div className="graph-value">99</div>
																					<div>Max</div>
																				</span>
																			</Col>
																		</Row>
																		<Row className="hr24-graph" align="bottom">
																			<div className="graph-name">
																				<span className="pollutant-name">SO <sub>2</sub></span>
																				<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
																			</div>
																			<Col span={17}>
																				<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
																			</Col>
																			<Col span={5}>
																				<span className="value">
																					<div className="graph-value">67</div>
																					<div>Min</div>
																				</span>
																				<span className="value">
																					<div className="graph-value">99</div>
																					<div>Max</div>
																				</span>
																			</Col>
																		</Row>
																		<Row className="hr24-graph" align="bottom">
																			<div className="graph-name">
																				<span className="pollutant-name">CO <sub>2</sub></span>
																				<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
																			</div>
																			<Col span={17}>
																				<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
																			</Col>
																			<Col span={5}>
																				<span className="value">
																					<div className="graph-value">67</div>
																					<div>Min</div>
																				</span>
																				<span className="value">
																					<div className="graph-value">99</div>
																					<div>Max</div>
																				</span>
																			</Col>
																		</Row>
																		<Row className="hr24-graph" align="bottom">
																			<div className="graph-name">
																				<span className="pollutant-name">PM <sub>10</sub></span>
																				<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
																			</div>
																			<Col span={17}>
																				<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
																			</Col>
																			<Col span={5}>
																				<span className="value">
																					<div className="graph-value">67</div>
																					<div>Min</div>
																				</span>
																				<span className="value">
																					<div className="graph-value">99</div>
																					<div>Max</div>
																				</span>
																			</Col>
																		</Row>*/}
																{/*</div>*/}
															</Card>;
														}
													}).filter(Boolean);
													if (c > 0) {
														return station_display;
													} else {
														return <div className="hourly-aqi-na-error text-center compare-search">No Station in the selected city!</div>;
													}
												})()}
											</div>;
										} else {
											return(
												<div className="hourly-aqi-na-error text-center compare-search">No Station is selected to be compared. Please select a station!</div>
											);
										}
									}
							})()}
						</div>
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Compare;