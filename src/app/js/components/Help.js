import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';;
import Loading from './imports/Loading';
import Head from './imports/Head';
import Side from './imports/Side';
import Tour from 'reactour';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Table, Modal, notification, Alert, Spin, Card, AutoComplete, Input } from 'antd';
import $ from 'jquery';
import Magnifier from "react-magnifier";

const { Content, Footer } = Layout;

const InputGroup = Input.Group;

export default class Help extends React.Component {
	
	constructor(props) {
		super(props);
		
		this.state = {};
	}

	componentDidMount() {
		let that = this;
		$(function() {
			var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;
				var links = this.el.find('.link');
				links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
			}

			Accordion.prototype.dropdown = function(e) {
				var _el = e.data.el,
					_this = $(this),
					_next = _this.next();

				_next.slideToggle();
				_this.parent().toggleClass('open');

				if (!e.data.multiple) {
					_el.find('.submenu').not(_next).slideUp().parent().removeClass('open');
				};
			}	

			var accordion = new Accordion($('#accordion'), false);
		});

		$(document).on('click', '#menu1', function () {
			const tabScroll = $("#dashboard_content").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu2', function () {
			const tabScroll = $("#view_real_time").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu3', function () {
			const tabScroll = $("#view_sation_details").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu4', function () {
			const tabScroll = $("#generate_report").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu5', function () {
			const tabScroll = $("#compare_content").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu6', function () {
			const tabScroll = $("#add_city_compare").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu7', function () {
			const tabScroll = $("#user_content").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu8', function () {
			const tabScroll = $("#add_new_user").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu9', function () {
			const tabScroll = $("#user_group_content").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu10', function () {
			const tabScroll = $("#add_user_group").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu11', function () {
			const tabScroll = $("#role_content").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});

		$(document).on('click', '#menu12', function () {
			const tabScroll = $("#station_content").offset();
			window.scrollTo({
				'behavior': 'smooth',
				'left': 0,
				'top': tabScroll.top - 60
			});
		});



		$(document).on('click', '#mob_menu1', function () {
			that.moveSection ('mob_menu1');
		});

		$(document).on('click', '#mob_menu2', function () {
			that.moveSection ('mob_menu2');
		});

		$(document).on('click', '#mob_menu3', function () {
			that.moveSection ('mob_menu3');
		});

		$(document).on('click', '#mob_menu4', function () {
			that.moveSection ('mob_menu4');
		});

		$(document).on('click', '#mob_menu5', function () {
			that.moveSection ('mob_menu5');
		});

		$(document).on('click', '#mob_menu6', function () {
			that.moveSection ('mob_menu6');
		});

		$(document).on('click', '#mob_menu7', function () {
			that.moveSection ('mob_menu7');
		});

		$(document).on('click', '#mob_menu8', function () {
			that.moveSection ('mob_menu8');
		});

		$(document).on('click', '#mob_menu9', function () {
			that.moveSection ('mob_menu9');
		});

		$(document).on('click', '#menu-tab', function () {
			$('#menu-nav').slideToggle('slow');
			$('.topBar1').toggleClass('rotate-topbar');
			$('.btmBar1').toggleClass('rotate-btmbar');
			if (!$('#menu-back').hasClass('open1')) {
				$('#menu-back').addClass('open1');
			}
			else {
				$('#menu-back').removeClass('open1');
			}
		});

		$(document).on('click', '#menu-back', function () {
			if ($('#menu-back').hasClass('open1')) {
				$('#menu-nav').slideToggle('slow');
				$('#menu-back').removeClass('open1');
				$('.topBar1').removeClass('rotate-topbar');
				$('.btmBar1').removeClass('rotate-btmbar');
			}
		});

		$(function() {
			var link,
				toggleScrollToTopLink = function(){
					if($("body").scrollTop() > 100 || $("html").scrollTop() > 100){
						link.fadeIn(500);   
					}else{
						link.fadeOut(500);   
					}
					(window.scrollY > window.innerHeight) ? $("#hidden").addClass("active") : $("#hidden").removeClass("active");
				};

			link = $("#back_to_top");
			$(window).scroll(toggleScrollToTopLink);
			toggleScrollToTopLink();
			link.on("click", function(){
				$("body").animate({scrollTop: 0}, 1000);
				$("html").animate({scrollTop: 0});
			});
		});

		this.setArray();
	}

	closeMenu () {
		$('.topBar1').toggleClass('rotate-topbar');
		$('.btmBar1').toggleClass('rotate-btmbar');
		$('#menu-nav').slideToggle('slow');
		$('#menu-back').removeClass('open1');
	}

	setArray() {
		let element_array = Array.prototype.slice.call( document.getElementsByClassName("search-option"));
		console.log('element_array', element_array);
		this.setState({
			// element_array: element_array,
			element_array_copy: element_array.slice(0),
		});
	}
	
	componentDidUpdate(prevProps, prevState) {}

	setSearchText(text) {
		console.log('setSearchText text', text);
		let element_array = [];
		let text_array = [];
		if (text && text != '') {
			text_array = text.split(' ');
		}

		if (text_array.length) {
			text_array.map((text_map) => {
				if (this.state.element_array_copy && this.state.element_array_copy.length) {
					this.state.element_array_copy.map((st) => {
						if (st.innerText.split(' ').length) {
							st.innerText.split(' ').map((st_text) => {
								if (text_map && text_map != '' && st_text.toLowerCase() ==  text_map/*st.innerText.toLowerCase().includes(decodeURI(encodeURI(text_map.toLowerCase())))*/ && !element_array.includes(st.innerText)) {
									element_array.push(st.innerText);
								};
							});
						}

						if (text && text != '' && st.innerText.toLowerCase() == decodeURI(encodeURI(text.toLowerCase()))) {
							console.log('done dana done done', st.getAttribute("ID"));
							this.moveSection(st.getAttribute("ID"));
						}
					});
				}
			});
		}
		this.setState({
			search_text: decodeURI(encodeURI(text)),
			element_array: element_array
		});
	}

	emitEmpty () {
		this.search_bar.focus();
		this.setSearchText('');
	}

	moveSection (id) {
		console.log('moveSection id', id);
		let tabScroll;
		switch(id) {
			case 'mob_menu1':
				tabScroll = $("#dashboard_content").offset();
				this.closeMenu();
				$(".menu-hed").text("Dashboard");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu2':
				tabScroll = $("#view_real_time").offset();
				this.closeMenu();
				$(".menu-hed").text("Real-Time");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu3':
				tabScroll = $("#view_sation_details").offset();
				this.closeMenu();
				$(".menu-hed").text("Real-Time");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu4':
				tabScroll = $("#generate_report").offset();
				this.closeMenu();
				$(".menu-hed").text("Reports");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu5':
				tabScroll = $("#add_city_compare").offset();
				this.closeMenu();
				$(".menu-hed").text("Compare");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu6':
				tabScroll = $("#add_new_user").offset();
				this.closeMenu();
				$(".menu-hed").text("User");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu7':
				tabScroll = $("#add_user_group").offset();
				this.closeMenu();
				$(".menu-hed").text("User Group");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu8':
				tabScroll = $("#role_content").offset();
				this.closeMenu();
				$(".menu-hed").text("Role");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;

			case 'mob_menu9':
				tabScroll = $("#station_content").offset();
				this.closeMenu();
				$(".menu-hed").text("Station");
				window.scrollTo({
					'behavior': 'smooth',
					'left': 0,
					'top': tabScroll.top - 100
				});
				break;
		}
	}

	render() {
		const suffix = this.state.search_text ? <Icon type="close-circle" onClick={() => this.emitEmpty()} /> : null;
		return(
			<div id="help" className="full-page-container archive-container">
				<Head/>
				<div>
					<div id="menu-back" />
					<div id="menu-wrapper">
						<div id="menu-bar" />
						<div className="menu-hed">Dashboard</div>
						<div id="menu-nav">
							<ul>
								<li />
								<li id="mob_menu1" className="search-option"><span>How to Start Using “Aurassure Environment Monitoring Dashboard”?</span></li>
								<li id="mob_menu2" className="search-option"><span>How to view the real-time information/data of all your sensors deployed at different stations?</span></li>
								<li id="mob_menu3" className="search-option"><span>How to view the detailed information of a station?</span></li>
								<li id="mob_menu4" className="search-option"><span>How to generate and download reports?</span></li>
								<li id="mob_menu5" className="search-option"><span>How to add cities and stations to compare their data?</span></li>
								<li id="mob_menu6" className="search-option"><span>How to add a new user?</span></li>
								<li id="mob_menu7" className="search-option"><span>How to add new user group?</span></li>
								<li id="mob_menu8" className="search-option"><span>Role</span></li>
								<li id="mob_menu9" className="search-option"><span>Station</span></li>
							</ul>
					  </div>
					  <div id="menu-tab" className="burger-container1 closed">
							<div id="burger1">
								<div className="bar1 topBar1" />
								<div className="bar1 btmBar1" />
							</div>
						</div>
					</div>

					<div id="hed1" className="container-fluid">
						<h1 className="page-header">Help &amp; Features</h1>
					</div>
					<div className="main-container">
						<div className="menu-side-container">
							<div className="menu-side">
								<ul id="accordion" className="accordion">
									<li>
										<div id="drop-link" className="link">Dashboard<i className="fa fa-chevron-down" /></div>
										<ul className="submenu">
											<li id="menu1"><span>How to Start Using “Aurassure Environment Monitoring Dashboard”?</span></li>
										</ul>
									</li>
									<li>
										<div id="drop-link" className="link">Real-time<i className="fa fa-chevron-down" /></div>
										<ul className="submenu">
											<li id="menu2"><span>How to view the real-time information/data of all your sensors deployed at different stations?</span></li>
											<li id="menu3"><span>How to view the detailed information of a station?</span></li>
										</ul>
									</li>
									<li>
										<div className="link">Reports<i className="fa fa-chevron-down" /></div>
										<ul className="submenu">
											<li id="menu4"><span>How to generate and download reports?</span></li>
										</ul>
									</li>
									<li>
										<div className="link">Compare<i className="fa fa-chevron-down" /></div>
										<ul className="submenu">
											<li id="menu5"><span>Overview</span></li>
											<li id="menu6"><span>How to add cities and stations to compare their data?</span></li>
										</ul>
									</li>
									<li>
										<div className="link">User<i className="fa fa-chevron-down" /></div>
										<ul className="submenu">
											<li id="menu7"><span>Overview</span></li>
											<li id="menu8"><span>How to add a new user?</span></li>
										</ul>
									</li>
									<li>
										<div className="link">User Group<i className="fa fa-chevron-down" /></div>
										<ul className="submenu">
											<li id="menu9"><span>Overview</span></li>
											<li id="menu10"><span>How to add new user group?</span></li>
										</ul>
									</li>
									<li>
										<div id="menu11" className="link">Role</div>
									</li>
									<li>
										<div id="menu12" className="link">Station</div>
									</li>
								</ul>
							</div>
						</div>
						<div className="content-container">
							<div className="container">
								<InputGroup className="search-box-container">
									<AutoComplete
										dataSource={this.state.element_array}
										style={{ width: '100%' }}
										onChange={(e) => this.setSearchText(e)}
										placeholder="Search"
										allowClear={true}
									/>
								</InputGroup>
								<h3 className="heading">AURASSURE ENVIRONMENT MONITORING PORTAL</h3>
								<div className="underline" />
								<div className="content">
									<div className="row">
										<h3 className="content-title">DASHBOARD</h3>
										<ul className="sec-container">
											<li id="dashboard_content">
												<h4 className="content-title">
													1. How to Start Using “Aurassure Environment Monitoring Dashboard”?
												</h4>
												<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<div className="full-width align-center">
														<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/dashboard_structure-01.png"} />
														{/*<img className="img_dashboard widt-100" src="https://dev.datoms.io/aurassure-website/imgs/help/dashboard_structure-01.png" alt="Dashboard Structure" />*/}
													</div>
												</div>
												<ol>
													<li><strong>Admin/User</strong> are the only authorized and registered users who have the access to Aurassure portal. By clicking on the avatar, you can change your password or logout from your session.</li>
													<li>The <strong>Sidebar</strong> is a launching point for all your key actions. This is where you can switch between different sections of the dashboard to check the status of your devices, compare stations, generate reports and manage users, stations etc.</li>
													<li>The <strong>REAL-TIME</strong> section of the portal gives you a comprehensive overview of all your environment sensors deployed at different locations along with their changing trends over time.</li>
													<li><strong>DEVICES</strong> ???</li>
													<li>The <strong>REPORTS</strong> section gives you the flexibility to generate eye catching reports based on your inputs of different stations and parameters for sharing or compliance purposes.</li>
													<li><strong>COMPARE</strong> page allows you to easily compare the AQI value of your city and pollution concentration of different parameters in real time.</li>
													<li><strong>SETTINGS</strong> page allows you to add new users, groups, assign user roles, set threshold values for stations to receive alerts in case of violation etc.</li>
												</ol>
											</li>
										</ul>
									</div>
									<div className="row">
										<h3 className="content-title">REAL-TIME</h3>
										<ul className="sec-container">
											<li id="view_real_time">
												<h4 className="content-title">
												1. How to view the real-time information/data of all your sensors deployed at different stations?
												</h4>
												<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/real_time-02.png"} />
													{/*<img className="img_dashboard widt-100" src="https://dev.datoms.io/aurassure-website/imgs/help/real_time-02.png" alt="Real-time Structure"/>*/}
												</div>
												</div>
												<ol>
													<li><strong>Choose Sensors Category to View</strong>: On clicking the search bar, a dropdown list shows up; here you can choose the desired category of sensors by selecting the checkboxes.</li>
													<li><strong>Search by Station Name</strong>: You can search for a station by typing in the station name.</li>
													<li>You can also hover/click on any map marker to get the station details.</li>
													<li>You can click on the &gt; mark to get a real time overview of every sensors deployed at that station.</li>
													<li>The color of the icon shows the active status of the station. Different colors have been used to show the current status of that station.</li>
												</ol>
											</li>
											<li id="view_sation_details">
												<h4 className="content-title">
												2. How to view the detailed information of a station?
												</h4>
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/more_details-03.png"} />
													{/*<img className="img_dashboard widt-100" src="https://dev.datoms.io/aurassure-website/imgs/help/more_details-03.png" alt="Real-time Structure with more details" />*/}
												</div>
												<ol>
													<li>This section shows an overview of different sensors deployed at the selected station.</li>
													<li>Click on the (x) icon to close the station details.</li>
													<li>
														To get a complete detailed information of the station click on ‘More Details’. You will then be taken to the following section: -
														<div className="full-width align-center mar-top-10">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/more_details-04.png"} />
															{/*<img className="img_dashboard widt-100" src="https://dev.datoms.io/aurassure-website/imgs/help/more_details-04.png" alt="Real-time Structure with more details"/>*/}
														</div>
													</li>
													<li>Select a time interval for which you want to view the information of station using the calendar.</li>
													<li>This section shows the 24-hour AQI (Air Quality Index) trends of different parameters. You can hover your mouse pointer over each bar to view the index of every hour.</li>
													<li>This section shows an overview of different weather parameters over the last 24 hours.</li>
													<li>This section shows the continuous trend of each individual parameter through an informative graph in real-time.</li>
													<li>You can zoom into a section of graph by dragging your mouse pointer over a selected area.</li>
													<li>You can reset back the graph by clicking on the ‘Reset Button’.</li>
													<li>You can toggle between different parameters by clicking on each individual tab button.</li>
													<li>This section shows the 1-hour average report of each individual parameter. You can hover your pointer over each bar to view the time and concentration of the parameters.</li>
												</ol>
											</li>
										</ul>
									</div>
									<div className="row">
										<h3 className="content-title">REPORTS</h3>
										<ul className="sec-container">
											<li id="generate_report">
												<h4 className="content-title">
												1. How to generate and download reports?
												</h4>
												<ol className="type-a">
													<li className="font-bold mar-top-10">
														Select Sub-category
														<ul className="font-normal">
															<li>On clicking the text field, a dropdown list containing different category of station shows up, here you can select the desired sub category of station by clicking on the checkboxes.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/reports_select_sub_category-05.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/reports_select_sub_category-05.png" alt="Select sub catagory for report"/>*/}
														</div>
													</li>
													<li className="font-bold mar-top-10">
														Selecting Station
														<ul className="font-normal">
															<li>On clicking the search field, a list of stations shows up in a drop-down menu. You can select a station by clicking on the station name.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/reports_select_stations-06.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/reports_select_stations-06.png" alt="Select station for report" />*/}
														</div>
													</li>
													<li className="font-bold mar-top-10">
														Data type
														<ul className="font-normal">
															<li>You can opt for a data type, by selecting one of the radio buttons.</li>
															<li>For average data type you get the option to choose between (1-hour, 8 hour and 24 hour) data formats.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/data_type-07.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/data_type-07.png" alt="Select data type for report" />*/}
														</div>
													</li>
													<li className="font-bold mar-top-10">
														Time Interval
														<ul className="font-normal">
															<li>Here you can select a time frame for which you need to generate the report using the calender.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/reports_calender-08.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/reports_calender-08.png" alt="Select time interval for report" />*/}
														</div>
													</li>
													<li className="font-bold mar-top-10">
														Parameters
														<ul className="font-normal">
															<li>Choose the parameters for which you want to generate the report by clicking on the check boxes.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/reports_parameters-09.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/reports_parameters-09.png" alt="Select parameters for report" />*/}
														</div>
													</li>
													<li className="font-bold mar-top-10">
														Conversion Type
														<ul className="font-normal">
															<li>
																Choose a conversion type by selecting one of the radio buttons.
																<ol className="type-i">
																	<li>USEPA-United States Environmental Protection Agency</li>
																	<li>NAQI-National Air Quality Index</li>
																</ol>
															</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/reports_conversion_type-10.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/reports_conversion_type-10.png" alt="Select conversion type for report" />*/}
														</div>
													</li>
													<li className="font-bold mar-top-10">
														Generate Report 
														<ul className="font-normal">
															<li>
																Finally, you can click on the ‘Generate Report’ button to download/view your report.
															</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/generate_reports-11.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/generate_reports-11.png" alt="Generate report" />*/}
														</div>
													</li>
												</ol>
											</li>
										</ul>
									</div>
									<div id="compare_content" className="row">
										<h3 className="content-title">COMPARE</h3>
										<div className="full-width align-center">
											<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/compare-12.png"} />
											{/*<img className="img_dashboard widt-100" src="https://dev.datoms.io/aurassure-website/imgs/help/compare-12.png" alt="Dashboard Structure" />*/}
										</div>
										<ol>
											<li>This section shows a list of different cities added for comparison. The overall AQI data for each city can be seen and compared to others here.</li>
											<li>This section shows a list of different stations within a city where the sensors are installed. You can compare their AQI values, and last 24 hours trends etc. here.</li>
										</ol>
										<ul className="sec-container">
											<li id="add_city_compare">
												<h4 className="content-title">
												1. How to add cities and stations to compare their data?
												</h4>
												<ol className="type-a">
													<li className="font-bold mar-top-10">
														City Comparison: Add new
														<ul className="font-normal">
															<li>You can click the ‘Add New’ button to add a city to your comparison list.</li>
															<li>Select a city from the drop-down menu that shows up once you click on the text field and press ok.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/add_city_to_compare-13.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/add_city_to_compare-13.png" alt="Add city for compare" />*/}
														</div>
													</li>
													<li className="font-bold mar-top-10">
														Station Comparison
														<ul className="font-normal">
															<li>Select a city from the drop-down menu.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/compare_station_compare-14.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/compare_station_compare-14.png" alt="Select station for compare" />*/}
														</div>
														<ul className="font-normal mar-top-10">
															<li>A list of station names comes up once you click on the text field. You can choose and add a station by clicking on the <strong>Add</strong> button next to the station name. To remove a city or station click on the (x) button.</li>
														</ul>
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/compare_add_station-15.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/compare_add_station-15.png" alt="Add station for compare" />*/}
														</div>
													</li>
												</ol>
											</li>
										</ul>
									</div>
									<div id="user_content" className="row">
										<h3 className="content-title">USER</h3>
										<ul className="sec-container">
											<li>
												This is the user management section of the portal, here you can add/remove users, user groups and assign them a role. The user management section shows list of all users assigned, authorized and using the dashboard.
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/user-16.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/user-16.png" alt="User section" />*/}
												</div>
											</li>
											<li>
												Filter by Status: Click on the field to select users by their active status.
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/user_status-17.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/user_status-17.png" alt="User status" />*/}
												</div>
											</li>
											<li>
												Search Users: Search for a user by typing in a name in the search box
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/user-search-18.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/user-search-18.png" alt="User search" />*/}
												</div>
											</li>
										</ul>
										<ol id="add_new_user" className="type-a">
										<li className="font-bold mar-top-10">
											How to add a new user?
											<ol className="font-normal">
												<li>
													Add New user: To add a new user, click on the Add new user button.
													<div className="full-width align-center">
														<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/add_new_user-19.png"} />
														{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/add_new_user-19.png" alt="Add new user" />*/}
													</div>
												</li>
												<li>
													A drawer box comes in where you can fill in the required details of the user and click on submit after filling in all the details.
													<div className="full-width align-center">
														<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/submit_user-20.png"} />
														{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/submit_user-20.png" alt="Submit user" />*/}
													</div>
												</li>
												<li>
													Action: You can take an action on a user; like edit, delete or deactivate by clicking the (:) icon.
													<div className="full-width align-center">
														<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/take_action-21.png"} />
														{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/take_action-21.png" alt="Take action" />*/}
													</div>
												</li>
											</ol>
										</li>
										</ol>
									</div>
									<div id="user_group_content" className="row">
										<h3 className="content-title">USER GROUP</h3>
										<ul id="user" className="sec-container">
											<li>
												Filter: Filter a user group by their role
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/group_role-22.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/group_role-22.png" alt="User group role" />*/}
												</div>
											</li>
											<li>
												Search User Group: Search for a user group by typing in the group name in the search box.
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/search_user_group-23.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/search_user_group-23.png" alt="Search user group" />*/}
												</div>
											</li>
										</ul>
										<ol id="add_user_group" className="type-a">
											<li className="font-bold mar-top-10">
												How to add new user group?
												<ol className="font-normal">
													<li>
														Create New User Group: To add a new user group, click on the Create New Group button.
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/create_new_group-24.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/create_new_group-24.png" alt="Create new group" />*/}
														</div>
													</li>
													<li>
														Action: You can take an action on a group; like edit or delete by clicking on the (:) icon
														<div className="full-width align-center">
															<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/action_group-25.png"} />
															{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/action_group-25.png" alt="Acion group" />*/}
														</div>
													</li>
												</ol>
											</li>
										</ol>
									</div>
									<div id="role_content" className="row">
										<h3 className="content-title">ROLE</h3>
										<p>This section allows you to assign and manage roles to a user.</p>
										<ol id="role">
											<li>
												Filter Roles by Access: Click on the field and you will be displayed a list of roles defined for a user. You can choose the roles by selecting on the checkboxes and the users will be filtered out based on the selection.
												<div className="full-width align-center mar-top-10">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/roles-26.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/roles-26.png" alt="Role section" />*/}
												</div>
											</li>
											<li>
												Search User Group: Search for a user group by typing in the group name in the search box.
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/search_by_roles-27.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/search_by_roles-27.png" alt="Search role" />*/}
												</div>
											</li>
											<li>
												To create a new role, click on the ‘create new role’ button. On clicking the button, you will be displayed with a list of fields that you need to fill in and then click on the submit button.
												<div className="full-width align-center">
													<Magnifier src={"https://dev.datoms.io/aurassure-website/imgs/help/add_new_role-28.png"} />
													{/*<img className="img_dashboard widt-100 mar-top-10" src="https://dev.datoms.io/aurassure-website/imgs/help/add_new_role-28.png" alt="Add new role" />*/}
												</div>
											</li>
										</ol>
									</div>
									<div id="station_content" className="row">
										<h3 className="content-title">STATION</h3>
										<ul>
											<li>Filter or select stations by their category or sub-category, by selecting the checkboxes once you click on the filter bar.</li>
											<li>Search for a station by its name or sub category by typing the station name in search bar.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="back_to_top">
					 <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><style dangerouslySetInnerHTML={{__html: ".st2{fill:#fff;stroke:#000;stroke-width:2;stroke-miterlimit:10}" }} /><g fill="none" stroke="#000" strokeMiterlimit={10}><circle cx={50} cy={50} r={47} strokeWidth={4} /><path d="M76.57 65.5L51.43 35.13c-.7-.85-2-.85-2.7 0L23.43 65.5" strokeWidth={9} /></g></svg>
				</div>
			</div>
		);
	}
}